# Hacking Developers! - 1


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta CyberPath Bank bünyesinde çalışan developerların sistemlerini test etmeniz istendi. Size bir attacker makine sağlandı.

**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyeti belirleyip sisteme erişip sağlamak.
- Sistemden reverse shell almak.

**Flags**:
- Server'da açık olan port?
    - answer: **8888**
- Web servisinin ismi ve versiyonu?
    - answer: **Tornado httpd 4.5.2**
- Web servisinde çalışan uygulamanın ismi?
    - answer: **jupyter**
- flag.txt?
    - answer: **CyberPath{9snPFr4WRJYMWKvT}**

## **Solution**:

- **Enum**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 921  bytes 53102 (53.1 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 252  bytes 2172160 (2.1 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 346  bytes 2177569 (2.1 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 346  bytes 2177569 (2.1 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV 10.0.0.46 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2021-12-26 08:36 UTC
Nmap scan report for web_0_web_1.web_0_internal_network (10.0.0.46)
Host is up (0.000070s latency).
Not shown: 65534 closed ports
PORT     STATE SERVICE VERSION
8888/tcp open  http    Tornado httpd 4.5.2
| http-robots.txt: 1 disallowed entry 
|_/ 
|_http-server-header: TornadoServer/4.5.2
| http-title: Home
|_Requested resource was /tree?

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 7.72 seconds
```

- **Explotation**:

![1](1.PNG)
![2](2.PNG)
![3](3.PNG)