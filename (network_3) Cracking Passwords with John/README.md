# Cracking Passwords With John


**Flags**:
- Hashin algoritmasını bulun: `7f9a6871b86f40c330132c4fc42cda59` 
  - answer: **md5**
- Hashin algoritmasını bulun: `069fca009882e13e01c6b0559c9b14a4337c4495f83fd720965ec80f0770a699` 
  - answer: **sha256**
- Algoritmasını bulduğunuz hashi kırın: `7f9a6871b86f40c330132c4fc42cda59` 
  - answer: **tinkerbell**
- Algoritmasını bulduğunuz hashi kırın: `069fca009882e13e01c6b0559c9b14a4337c4495f83fd720965ec80f0770a699` 
 - answer: **alejandra**
- NT hashi kırın: `99240555096834225702CA6580817BF2` 
  - answer: **ricardo**
- Single crack modu ile 'joker' kullanıcısının şifresini kırın: `21781ad513911c2cf63fa894bcaf24a8`
  - answer: **j0ker**
- id_rsa ssh keyini kırın!
  - answer: **abc123**
- Metasploit ile server'dan aldığımız unshadow passwd ve shadow dosyalarını kullanarak root şifresini kırın!
  - answer: **hannover23**
- secret.zip'in şifresini kırın!
  - answer: **family**

**0 - John The Ripper'a Giriş:**
- John the Ripper, ücretsiz bir şifre kırma yazılım aracıdır. Orijinal olarak Unix işletim sistemi için geliştirilmiştir. Bir dizi şifre kırıcıyı tek bir pakette birleştirdiği, şifre karma türlerini otomatik olarak algıladığı ve özelleştirilebilir bir kırıcı içerdiği için en sık kullanılan şifre test etme ve kırma programları arasındadır.
- Bu laboratuvarda önce, John the Ripper'ı şifreleri kırmak için nasıl kullanacağımızı öğreniyoruz. Ardından root şifresini ve zip şifresini kırıyoruz!

**1 - Basit hashleri Kıralım(md5, sh256)**

 - Solution:
```bash
root@cyberpath:~# echo 7f9a6871b86f40c330132c4fc42cda59 > hash1.txt
root@cyberpath:~# cat hash1.txt | python2 hash-identifier.py
(gets md5)
root@cyberpath:~# echo 069fca009882e13e01c6b0559c9b14a4337c4495f83fd720965ec80f0770a699 > hash2.txt
root@cyberpath:~# cat hash2.txt | python2 hash-identifier.py
(Gets sha256)
root@cyberpath:~# john --format=raw-md5 --wordlist=rockyou hash1.txt
(Gets tinkerbell)
root@cyberpath:~# john --format=raw-sha256 --wordlist=rockyou hash2.txt
(Gets alejandra)
```

**2 - Windows Hashlerini Kıralım(NTHash / NTLM )**

 - Solution:
```bash
root@cyberpath:~# echo 99240555096834225702CA6580817BF2 > ntlm.txt
root@cyberpath:~# john --format=NT --wordlist=rockyou.txt ntlm.txt
(Gets ricardo)
```

**3 - Single crack Modu**

 - Solution:
 ```bash
root@cyberpath:~# echo 21781ad513911c2cf63fa894bcaf24a8 > singlehash.txt
root@cyberpath:~# cat singlehash.txt | python2 hash-identifier.py
root@cyberpath:~# nano singlehash.txt (change singleash.txt to `joker:21781ad513911c2cf63fa894bcaf24a8`)
root@cyberpath:~# john --single --format=raw-md5 singlehash.txt
(Gets j0ker)
```

**4 - SSH Keylerini Kıralım**

 - Solution:
```bash
root@cyberpath:~# python2 ssh2john.py id_rsa > ssh.hash                                                                
root@cyberpath:~# john --wordlist=rockyou.txt ssh.hash
(gets abc123)
```


**Penetration Testinge Dönme Zamanı!!!**

John The Ripper ile çalışmayı öğrendiğimize göre Pentestimize geri dönebiliriz.

**5 - /etc/shadow'u Kıralım**

 - Solution:
```bash
root@cyberpath:~# unshadow passwd shadow > unshadowed.txt
root@cyberpath:~# john --wordlist=passwords.txt unshadowed.txt 
Using default input encoding: UTF-8
Loaded 2 password hashes with 2 different salts (sha512crypt, crypt(3) $6$ [SHA512 256/256 AVX2 4x])
Cost 1 (iteration count) is 5000 for all loaded hashes
Will run 2 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
eagle            (abbey)
hannover23       (root)
2g 0:00:00:00 DONE (2021-11-02 10:53) 3.278g/s 839.3p/s 1678c/s 1678C/s cachonda..john
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```

**6 - Arşiv Dosyalarını Kıralım**

 - Solution:
```bash
root@cyberpath:~# zip2john secret.zip > secret.hash  
ver 81.9 secret.zip/empty.txt is not encrypted, or stored with non-handled compression  type                                 
root@cyberpath:~# john secret.hash                            
Using default input encoding: UTF-8
Loaded 1 password hash (ZIP, WinZip [PBKDF2-SHA1 256/256 AVX2 8x])
Will run 2 OpenMP threads
Proceeding with single, rules:Single
Press 'q' or Ctrl-C to abort, almost any other key for status
Warning: Only 15 candidates buffered for the current salt, minimum 16 needed for performance.
Warning: Only 9 candidates buffered for the current salt, minimum 16 needed for performance.
Almost done: Processing the remaining buffered candidate passwords, if any.
Warning: Only 12 candidates buffered for the current salt, minimum 16 needed for performance.
Proceeding with wordlist:/usr/share/john/password.lst, rules:Wordlist
family           (secret.zip/empty.txt)
1g 0:00:00:01 DONE 2/3 (2021-11-02 10:56) 0.7092g/s 28391p/s 28391c/s 28391C/s 123456..Peter
Use the "--show" option to display all of the cracked passwords reliably
Session completed
```


**Yapılacaklar:**
- Tüm şifreleri kırdınız ve ağdaki tüm sunuculara eriştiniz! Dwayne Medrano, hızlı süreçten oldukça etkilendi! Bazı şifreleri unuttu ve bu şifreleri sizin kırmanızı istiyor. Ancak bunun için bir kelime listesine ihtiyacınız var çünkü özel şifreler özel wordliste ihtiyaç duyuyor. Wordlist oluşturmayı öğrenmenin zamanı geldi!