# Linux 101

<br>
<br>

## **1 - History, Philosophy(everything is file, etc.) - text-based**
Linux is a family of open-source Unix-like operating systems based on the Linux kernel, an operating System Kernel first released on 1991, by Finnish Student Linux Torvalds.
<br>
Linux is typcially packaged in Linux distribution. Distrubitions include Linux kernel and supporting system software and librearies, many of which are provided by the GNU Project. Populat Linux distrubitons include Debian, Fedora and Ubuntu(Debian-Based). There are commercial distrubitons too, such as Red Hat Enterprise Linux and SUSE Linux Enterprise. Desktop Linux distrubitons include a windowing system, and a desktop environment such as GNOME, KDE, an XFCE. 

<br>

### **Philosophy**:
Linux follows five core principles:
- Everything is a file.
- Small, single-purpose programs.
- Ability to chain programs together to perform complex tasks.
- Avoid captive user interfaces.
- Configuration data stored in a text file.

<br>

### **Linux Components**:
- **Bootloader:** A boot loader, also called a boot manager, is a small program that places the operating system (OS) of a computer into memory.
- **OS Kernel:** The Linux kernel is the main component of a Linux operating system (OS) and is the core interface between a computer's hardware and its processes.
- **Daemons:** A daemon (also known as background processes) is a Linux or UNIX program that runs in the background. 
- **OS Shell:**  Shell or the command language interpreter is the interface between the OS and the user. This interface allows the user to tell the OS what to do. Everything we do through the GUI we can do with the shell. 
- **Graphics server:** The display server communicates with its clients over the display server protocol, like X11. The display server is a key component in any graphical user interface, specifically the windowing system. 
- **Window Manager:** Also known as a graphical user interface (GUI). 
- **Utilities:** Applications or utilities such as Browser, Text Editor, Task Manager.

<br>

### **Linux Architecture**:
The Linux operating system's architecture mainly contains some of the components:
- **Hardware:** Hardware Components such as the system's RAM, CPU, Hard Drive, Printer and others.
- **Linux Kernel:** The core of operating system whose funtion to control computer hardware resources like CPU, RAM, data, and others.
- **Shell:** A command-line interface to run user entered commands.
- **System Utility:** System Utility programs are make avaible to user of Linux System.
 <br>
![kernel](kernel.png)
 <br>

<br>

### **File System**:
Linux file system a tree-like hierarchy and is documented in the Filesystem Hieararchy Standard (FHS) which is a standard maintained by the Linux Foundation. It consists of directories, sub-directories, and data files. 
 <br>
![file_system](file_system.png)
 <br>

- **/**: Root directory which shoud contain only the directories needed at the top level of file structure.
- **/bin**: /bin is where the executable files are located such as vim which is build-in terminal-based text editor.
- **/dev**: /dev is hold device drivers.
- **/etc**: System configuration files.
- **/lib**: Shared libraries required for Operating System.
- **/boot**: Contains files need for booting the System.
- **/home**: Contains the home directory for users.
- **/mnt**: /mnt used to mount other file systems like cdrom.
- **/media**: External removable media devices like USB drives.
- **/tmp**: /tmp hold temporary files used to System.
- **/usr**: Contains executables, libraries, man files, can be used by many users.
- **/sbin**: Contains binary(executable) files for systen administration.
- **/kernel**: Contains Linux Kernel files.

<br>
<br>

## **2 - Introduction to shell(prompt, help, system information commands like whoami, id, ifconfig, etc.) - interactive**
**Introduction to shell:**
<br>
A Shell provides you with an interface to the Unix system. It gathers input from you and executes programs based on that input. When a program finishes executing, it displays that program's output.

Shell is an environment in which we can run our commands, programs, and shell scripts. There are different flavors of a shell, just as there are different flavors of operating systems. Each flavor of shell has its own set of recognized commands and functions.

Terminal emulation is software that emulates the function of a terminal. There are lot of terminal emulators in modern linux systems such as such as GNOME Terminal, XFCE4 Terminal, XTerm, and many others. There are also called command-line interfaces that run in one terminal and thuse are multiplexers. TMUX is one of the most used multiplexers in Linux. You can split terminal in TMUX. TMUX is pretty useful for ssh and other remote sessions which we will learn later.
 <br>
![firefox](tmux.PNG)
 <br>
<br>

The one of the most commonly used shell in Linux is the Bourne-Again Shell (BASH) and is part of the GNU project. We will use BASH in this tutorial.

<br>

**Introduction to Prompt:**
<br>
The BASH prompt, $, which is called the command prompt, is issued by the shell. BASH prompt is easy to understand and default it includes lot of information. The format of the bash:
```bash
<username>@<hostname><current working directory>$
```
In our machine:
```bash
attacker@cyberpath:~$
```
From prompt we can learn username is attacker, hostname is cyberpath and current working directory is **~**(Which is our home directory).
<br>

**Getting help in shell:**
<br>

- Help with man pages:

```bash
attacker@cyberpath:~$ man curl
```

- --help option

```bash
attacker@cyberpath:~$ curl --help | more
Usage: curl [options...] <url>
     --abstract-unix-socket <path> Connect via abstract Unix domain socket
     --alt-svc <file name> Enable alt-svc with this cache file
     --anyauth       Pick any authentication method
    ...
```

**System Information:**
- **whoami**: Display current username:
```bash
attacker@cyberpath:~$ whoami
attacker
```

- **id**: One of the most useful commands in system. Returns user and uid, guid, group permissions:
```bash
attacker@cyberpath:~$ id
uid=1000(attacker) gid=1000(attacker) groups=1000(attacker),27(sudo)
```

- **hostname**: Returns system hostname, this command not useful in bash because we already know hostname. But sometimes we don't have bash in system. If we only have SH shell hostname command would be very useful.
```bash
attacker@cyberpath:~$ hostname
cyberpath
```

- **uname**: Prints basic information about system. I suggest you to use with -a arguman.
```bash
attacker@cyberpath:~$ uname -a
Linux cyberpath 5.10.16.3-microsoft-standard-WSL2 #1 SMP Fri Apr 2 22:23:49 UTC 2021
x86_64 x86_64 x86_64 GNU/Linux
```

- **pwd**: Returns current directory name. Like **hostname** command, this command useful in SH too.
```bash
attacker@cyberpath:~$ pwd
/home/attacker
```

- **ifconfig**: Another most useful command of system. Specially in victim machines. The ifconfig utility design to assign or to view an address to a network interface and/or configure network interface on system. Sometimes ifconfig requires root permission.
```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 172.19.0.2  netmask 255.255.0.0  broadcast 172.19.255.255
        ether 02:42:ac:13:00:02  txqueuelen 0  (Ethernet)
        RX packets 1339  bytes 100865 (100.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 999  bytes 308088 (308.0 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 
```
- **top**: Another most useful command of system. It's a built in terminal based task manager on linux.
```bash
attacker@cyberpath:~$ top
```
```bash
top - 08:55:47 up  1:22,  0 users,  load average: 0.00, 0.00, 0.00
Tasks:   5 total,   1 running,   2 sleeping,   2 stopped,   0 zombie
%Cpu(s):  0.0 us,  0.1 sy,  0.0 ni, 99.9 id,  0.0 wa,  0.0 hi,  0.0 si,  0.0 st
MiB Mem :  12156.9 total,  10468.3 free,    706.7 used,    981.9 buff/cache
MiB Swap:   4096.0 total,   4096.0 free,      0.0 used.  10850.6 avail Mem

  PID USER      PR  NI    VIRT    RES    SHR S  %CPU  %MEM     TIME+ COMMAND
    1 attacker  20   0  999148  25472   7392 S   0.0   0.2   0:01.88 gotty
   92 attacker  20   0    4052   3468   2864 S   0.0   0.0   0:00.05 bash
  117 attacker  20   0   18192   8608   5576 T   0.0   0.1   0:00.01 vim
  120 attacker  20   0   18192   8648   5616 T   0.0   0.1   0:00.00 vim
  129 attacker  20   0    5960   3140   2628 R   0.0   0.0   0:00.00 top



```

You can close top with **q** command.
<br>

<br>
<br>

## **3 - File system commands(ls, cd, cat, touch, mkdir, tree, etc.) - interactive**
### **Create, Move, Copy, and Delete files**:
<br>
Lets work with files! We can create empty file in Linux with **touch** command. For create a empty directory we use **mkdir** command. We use **cp** command to copy file to another location and **mv** command to move file to another location. For delete commands we use **rm** command for files and directories. Lets try this commands in shell.
    - **touch**: We can create any file with any extension with touch.
    ```bash
    attacker@cyberpath:~$ touch new_file.txt
    ```

    - **mkdir**: We can create directories with mkdir command:
    ```bash     
    attacker@cyberpath:~$ mkdir cyberpath
    ```
    - **cp**: Copy file to another directory. In the below example we copy new_file.txt into cyberpath folder.
    ```bash
    attacker@cyberpath:~$ cp new_file.txt cyberpath/
    ```
    - **mv**: Move file to another directory. In the below example we moved new_file.txt to cyberpath2 folder.
    ```bash
    attacker@cyberpath:~$ mv new_file.txt cyberpath2/
    ```
    - **rm**: First lets delete a file with **rm** command.
    ```bash
    attacker@cyberpath:~$ rm new_file
    ```
    How about directories:
    ```bash
    attacker@cyberpath:~$ rm cyberpath
    rm: cannot remove 'cyberpath': Is a directory
    ```
    Our process is fail. Because we need argumant for removing directories. We can use **-r** switch to delete directories and everything in directory. Lets try
    ```bash
    attacker@cyberpath:~$ rm -r cyberpath
    ```
    Success! We delete cyberpath folder with files in it.

<br>

### **File Navigation**:

Before we move through the system, we have to find out in which directory we are.

```bash
attacker@cyberpath:~$ pwd
/home/attacker
```

We can list files in directory with **ls** command.

```bash
attacker@cyberpath:~$ ls --help
Usage: ls [OPTION]... [FILE]...
List information about the FILEs (the current directory by default).
Sort entries alphabetically if none of -cftuvSUX nor --sort is specified.

Mandatory arguments to long options are mandatory for short options too.
  -a, --all                  do not ignore entries starting with .
  -A, --almost-all           do not list implied . and ..
      --author               with -l, print the author of each file
  -b, --escape               print C-style escapes for nongraphic characters
...
```

The following command list all files in current directory.

```bash
attacker@cyberpath:~$ ls -la
total 56
drwxr-xr-x 1 attacker attacker  4096 Nov  6 07:07 .
drwxr-xr-x 1 root     root      4096 Nov  8 07:40 ..
-rw------- 1 attacker attacker  3919 Nov  8 08:43 .bash_history
-rw-r--r-- 1 attacker attacker   220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 root     root      3824 Nov  3 08:29 .bashrc
...
```

To move through the directories, we use the command cd. The follotwing command will change directory to **/tmp**

```bash
attacker@cyberpath:~$ cd /tmp/
attacker@cyberpath:/tmp$
```

We can quickly jump back to the directory we were last in:

```bash
attacker@cyberpath:/tmp$ cd -
/home/attacker
attacker@cyberpath:~$
```

The first entry with a single dot **.** indicates the current directory we are currently in. The second entry with two dots **..** represents the parent directory /home.

```bash
attacker@cyberpath:~$ cd ..
attacker@cyberpath:/home$
```

<br>
<br>

## **4 - Introduction to terminal based text editors(Nano, Vim) - interactive**
At some point in your career, you need to edit a configuration file, write a Bash script, code, from Linux terminal without GUI. When you do, you will turn to one of the popular text editors available to the Linux platform.

- Vim(Almost Every Linux system has Vim or Vi.)
- Nano(Much easier to use)

Let's start with Nano first. Open .bashrc(Our bash config file) with Nano:
```bash
attacker@cyberpath:~$ nano .bashrc
```

![firefox](nano.PNG)

We have shortcut menu from bottom. You can simply edit and save with Write Out(CTRL + O). I suggest you to play with nano. Open file, edit save, save to another location, search words in file and so on.

It's time to learn basic Vim. Vim is a highly configurable text editor built to make creating and changing any kind of text very efficient. It is included as "vi" with most UNIX systems and with Apple OS X. In penetration testing or System Administration most times we don't access GUI. We need to edit files from terminal. We can use Nano but again most times we didn't have Nano text editor on system. There's only Vi or Vim. So at least you able to open file, edit and close with Vim.
<br>
Let's open .bashrc again with Vim.

```bash
attacker@cyberpath:~$ vim .bashrc
```

![firefox](vim.PNG)

We didn't have context menu for this one. Try edit file you probably can't write anything. You need to enter Insert mode for editing. Press **"i"** for insert mode. Now you can edit file. To save file in Vim you need to exit Insert mode. To do this hit Escape(Esc) thats it. Time to send command to vim for save the file. Press **"Ctrl"** + **":"**. Now we are at Vim's command prompt. From Vim's command prompt we can save file with **"w"** command. For save and exit from Vim write **"wq"**. If you don't want to exit Vim before save file you need to use **"q!"** command. w for save, and q for quit. But if you edit file vim get error you just write **"q"** so **"!q"** command get job done. Finally if you want to get back insert mode press **"i"** again.

<br>

Some of the more useful vi commands (to be used when in command mode, and after hitting [Ctrl]+[:]) are:
- h – move cursor one character to left
- j – move cursor one line down
- k – move cursor one line up
- l – move cursor one character to right
- w – move cursor one word to right
- b – move cursor one word to left
- 0 – move cursor to beginning of the current line
- $ – move cursor to end of the current line
- i – insert to left of current cursor position (this places you in insert mode)
- a – append to right of current cursor position (this places you in insert mode)
- dw – delete current word (this places you in insert mode)
- cw – change current word (this places you in insert mode)
- ~ – change case of current character
- dd – delete the current line
- D – delete everything on the line to right of the cursor
- x – delete the current character
- u – undo the last command
- . – repeat the last command
- :w – save the file, but don’t quit vi
- :wq – save the file and quit vi

I highly recomment tou you play with Vim and get comfortable.

<br>
<br>

## **5 - find command - interactive**
The find command is one of the most powerful tools in the Linux system. It searches for files and directories in a directory hierarchy based on a user given expression and can perform user-specified action on each matched file. You can use the find command to search for files and directories based on their permissions, type, date, ownership, size, and more. It can also be combined with other tools such as grep or sed which we learn later.

<br>

General syntax of find:
```bash
find [options] [path...] [expression]
```
- The **options** attribute controls the treatment of the symbolic links, debugging options, and optimization method.
- The **path...** attribute defines the starting directory or directories where find will search the files.
- The **expression** attribute is made up of options, search patterns, and actions separated by operators.

<br>

To search for files in a directory, the user invoking the find command needs to have read permissions on that directory.

<br>

### **Find files by name**:
Finding files by name one of the most common use of find command.To find file by its name, use the -name option followed by the name of the file you searching for.

<br>

For example, to search a file named secret in /home/attacker directory, you would use the following command:
```bash
attacker@cyberpath:~$ find /home/attacker/ -type f -name secret
/home/attacker/.local/share/secret
```
Let's look this command.
- **-type f** switch tells we are looking for file to find. 
- **-name** switch for filename.

To run a case-insensitive search, change the **-name** switch with **-iname**:
```bash
attacker@cyberpath:~$ find /home/attacker/ -type f -iname Secret
/home/attacker/.local/share/secret
```
The command above will match "secret", "Secret", "SECRET" ..etc.

<br>

### **Find files by extension**:
Searching for files by extension is the same as searching for files by name. For example, to find all files ending with .log inside the /var/log/ directory, you would type:

```bash
attacker@cyberpath:~$ find /var/log -type f -name '*.log'
/var/log/apt/history.log
/var/log/apt/term.log
/var/log/bootstrap.log
/var/log/dpkg.log
/var/log/alternatives.log
/var/log/fontconfig.log
/var/log/postgresql/postgresql-13-main.log
find: '/var/log/exim4': Permission denied
```

We can use this for not .log files too with **-not** switch:

```bash
attacker@cyberpath:~$ find /var/log -type f -not -name '*.log'
/var/log/faillog
/var/log/apt/eipp.log.xz
/var/log/wtmp
/var/log/lastlog
/var/log/btmp
/var/log/aptitude
find: '/var/log/exim4': Permission denied
```
<br>

### **Find files by type**:
Sometimes you might need to search for specific file types such as regular files, directories, or symlinks. Remember In Linux, everything is a file.

To search for files based on their type, you can use the "-type" switch with one of the following descriptors to specify the file type:
- **f**: a regular file
- **d**: directory
- **l**: symbolic link
- **c**: character devices
- **b**: block devices
- **p**: named pipe (FIFO)
- **s**: socket

For example if we want find all directories in current directory, we would use:
```bash
attacker@cyberpath:~$ find . -type d
.
./.config
./.config/procps
./.local
./.local/share
./.local/share/nano
```
<br>

### **Find Files by Size**:
To find files based on the file size, you can use **-size** switch with one of the following suffixes to specify the file size:
- **b**: 512-byte blocks (default)
- **c**: bytes
- **w**: two-byte words
- **k**: Kilobytes
- **M**: Megabytes
- **G**: Gigabytes

The following command will find all files of exactly 2 bytes inside the /car directory:
```bash
attacker@cyberpath:~$ find /var/ -type f -size 1c
find: '/var/spool/cron/crontabs': Permission denied
find: '/var/spool/exim4': Permission denied
find: '/var/lib/apt/lists/partial': Permission denied
...
```

The find command also allows you to search for files that are greater or less than a specified size. Which is more useful for finding files by size. 
- **-<size>** less then
- **+<size>** greater then

For example following command find all files in /var directory less then 2 bytes.
```bash
attacker@cyberpath:~$ find /var/ -type f -size -2c
find: '/var/spool/cron/crontabs': Permission denied
find: '/var/spool/exim4': Permission denied
/var/lib/systemd/deb-systemd-helper-enabled/timers.target.wants/fstrim.timer
...
```

We can even search for files within a size range. The following command will find all files in /var directory between 3 and 4 byte:
```bash
attacker@cyberpath:~$ find /var/ -type f -size +3c -size -4c
find: '/var/spool/cron/crontabs': Permission denied
find: '/var/spool/exim4': Permission denied
find: '/var/lib/apt/lists/partial': Permission denied
...
```
<br>

### **Find Files by Modification Date**:
The find command can also search for files based on their last modification, access, or change time with **-mtime <day_number>** switch Which is very useful use case in penetration testing.


Lets find log files in /var directory that ends with .log and has been modified in last 2 days. 
```bash
attacker@cyberpath:~$ find /var -name "*.log" -mtime 2
/var/log/alternatives.log
/var/log/fontconfig.log
/var/log/postgresql/postgresql-13-main.log
...
```

<br>

### **Find Files by Permissions**:
The **-perm** switch allows you to search for files based on the file permissions which is another very useful use case in penetration testing specially Privilege Escalation.

For example, to find all files with permissions of exactly 644 inside the /var/ directory, you would use:
```bash
attacker@cyberpath:~$ find /var -perm 644
/var/lib/pam/seen
/var/lib/pam/session-noninteractive
/var/lib/pam/passwor
```

We can use **-<perm_number>** switch for the file to match, at least the specified bits must be set.
```bash
attacker@cyberpath:~$ find /var -perm -644 | more
find: /var
/var/spool
'/var/spool/cron/crontabs'/var/spool/mail
...
```

<br>

### **Find Files by Owner**:
To find files owned by a particular user or group, use the **-user** and **-group** switchs. The following command will find all files owned by attacker user on system:
```bash
attacker@cyberpath:~$ find / user attacker | more
/home/attacker/.bash_history
/home/attacker/.config
/home/attacker/.config/procps
...
```

<br>
<br>

## **6 - Filter contents(grep, more, less, etc.) - interactive**
A filter is a program that reads standard input, performs an operation upon it and writes the results to standard output. For this reason, it can be used to process information in powerful ways such as restructuring output to generate useful reports, modifying text in files and many other system administration tasks like reading log files.

Let's read /var/log/apt/history.log file with cat.

```bash
attacker@cyberpath:~$ cat /var/log/apt/history.log
...
```

cat immeditaly writes all output to standard output which is pretty ugly and hard to read. To read log files, we do not necessarily have to use cat or editor like nano. Linux system have built-in two tools called more and less, which are very identical. These are fundamental pagers that allow us to scroll through the file in an interactive view. 

### **more**:

```bash
attacker@cyberpath:~$ more /var/log/apt/history.log
...
```

Now you can easly read all log file with more. Press **Enter** to scroll down the Pager, **Space** to scroll all the way down, **q** to close.

If you want to know more about **more** command. Reading man page and help page is would be useful.

```bash
attacker@cyberpath:~$ more --help
...
```

```bash
attacker@cyberpath:~$ man more
```


### **less**:

```bash
attacker@cyberpath:~$ less /var/log/apt/history.log
...
```

**less** command more advanced then **more** command. Like **more** command press **Enter** to scroll down the Pager, **Space** to scroll all the way down, **q** to close. After close you will see output of **less** does not reamin in terminal unlike **more**.

If you want to know more about **more** command. Reading man page and help page is would be useful.

```bash
attacker@cyberpath:~$ less --help
...
```

```bash
attacker@cyberpath:~$ man less
```

### **Grep**:
We read output nicely with **more** and **less** commands. But what about we only interest specific result from log file. **grep** command output lines matching a given argumant. For example we are looking for net-tools log package in log.

```bash
attacker@cyberpath:~$ grep "net-tools" /var/log/dpkg.log
2021-11-03 08:00:39 install net-tools:amd64 <none> 1.60+git20180626.aebd88e-1ubuntu1
2021-11-03 08:00:39 status half-installed net-tools:amd64 1.60+git20180626.aebd88e-1ubuntu1
2021-11-03 08:00:39 status unpacked net-tools:amd64 1.60+git20180626.aebd88e-1ubuntu1
...
```

Sometimes log files much complicated then this one or not write in ascii and for reading it requires special programs. Most system admin combin **grep** with other commands using pipes in Linux. For example we can filter output of **apt-list --installed** like this:

```bash
attacker@cyberpath:~$ apt list --installed | more

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

Listing...
adduser/focal,now 3.118ubuntu2 all [installed]
aircrack-ng/kali-rolling,now 1:1.6+git20210130.91820bc-2 amd64 [installed]
alsa-topology-conf/focal,now 1.2.2-1 all [installed,automatic]
...
--More--
```

Also we can **grep** to filter results:

```bash
attacker@cyberpath:~$ apt list --installed | grep "net-tools"

WARNING: apt does not have a stable CLI interface. Use with caution in scripts.

net-tools/focal,now 1.60+git20180626.aebd88e-1ubuntu1 amd64 [installed]
```

<br>
<br>

## **7 - Introduction to User Management - interactive**
User management is an essential part of Linux administration. Sometimes we need to create new users or add other users to specific groups. Another possibility is to execute commands as a different user like execute as root. After all, it is not too rare that users of only one specific group have the permissions to view or edit specific files or directories. For example, the company employee does not have the right to make changes to the root system.

<br>

### **sudo command**:

Let's try to read /etc/shadow file as user.

```bash
attacker@cyberpath:~$ cat /etc/shadow
cat: /etc/shadow: Permission denied
...
```

Predictably, we don't have permission to read /etc/shadow. But with root permission we can do everything. Even we can delete bootloader. For execute as root you need to use **sudo**.

```bash
attacker@cyberpath:~$ sudo cat /etc/shadow
root:*:18906:0:99999:7:::
daemon:*:18906:0:99999:7:::
bin:*:18906:0:99999:7:::
```

If you are sudo user authorized by root, you can read the document after entering the password. 

```bash
attacker@cyberpath:~$ sudo cat /etc/shadow
[sudo] password for attacker:
root:*:18906:0:99999:7:::
daemon:*:18906:0:99999:7:::
bin:*:18906:0:99999:7:::
```

<br>

### **su command**:

The su utility will change the shell to the other user id after entering the other user password.

```bash
attacker@cyberpath:~$ su root
Password:
su: Authentication failure
```

This didn't work because we didn't know the root's password. If you have sudo permission, you can even switch to a user whose password you do not know.


```bash
attacker@cyberpath:~$ sudo su root
root@cyberpath:/home/attacker#
```

You can switch to old user with **exit** command.


```bash
root@cyberpath:/home/attacker# exit
exit
attacker@cyberpath:~$
```

You can do everything with **sudo** permission on Linux. The Linux to warn you that its action like Windows can harm the system. If you want to delete the bootloader, it will. If you want to delete all files in the system, it will delete it. That's why you should be careful when operating with sudo privileges.

<br>

### **Create user, manage permissions and delete user**:

You can create user on Linux with **useradd** command. Let's create user named neo. Of course we need root permission to create user.

```bash
attacker@cyberpath:~$ sudo useradd -m neo
```

The **-m** switch tells we want home directory for **neo** user. Let's attach password to neo. 

```bash
attacker@cyberpath:~$ sudo passwd neo
New password:
Retype new password:
passwd: password updated successfully
```

We succesfully attach password to neo. Now we can switch to neo with his password.

```bash
attacker@cyberpath:~$ su neo
Password:
$
```

```bash
$ id
uid=1001(neo) gid=1001(neo) groups=1001(neo)
```

Which gives a sh shell because we didn't configure shell for neo user. Let's try to read /etc/shadow file with **sudo** command.

```bash
$ sudo cat /etc/shadow
[sudo] password for neo:
neo is not in the sudoers file.  This incident will be reported.
```

We failed. Because we didn't configure neo user to sudo group. Let's add neo to sudo group. First we need to back our sudo user.

```bash
attacker@cyberpath:~$ sudo usermod -aG sudo neo
```

Now neo user have sudo permissions. If we execute **id** command as neo we can see neo have sudo permission.

```bash
attacker@cyberpath:~$ su neo
Password:
$ id
uid=1001(neo) gid=1001(neo) groups=1001(neo),27(sudo)
```

Let's try again to read /etc/shadow file with **sudo** command as neo.

```bash
$ sudo cat /etc/shadow
[sudo] password for neo:
root:*:18906:0:99999:7:::
daemon:*:18906:0:99999:7:::
bin:*:18906:0:99999:7:::
...
```

Time to delete neo user. We can delete user with **userdel** command.

```bash
$ exit
attacker@cyberpath:~$ sudo userdel neo
```

<br>
<br>

## **10 - Introduction to Package Management - interactive**
A package manager or package-management system is a collection of software tools that automates the process of installing, upgrading, configuring, and removing computer programs for a computer in a consistent manner.

Modern Linux systems have many package-managers, and each Linux distribution has its own main package manager. Debian based distros come with **dpkg** package manager. 

- **dpkg**: dpkg is a tool to install, build, remove and manage Debian packages. The primary and more user-friendly front-end for dpkg is apt and aptitude.

- **apt**: apt is a command-line utility for installing, updating, removing, and otherwise managing deb packages on Debian based Linux distrubitions.

- **aptitude**: aptitude is an alternative to apt and is a high-level interface to the package manager.

- **snap**: snap is a software packaging and deployment system developed by Canonical for operating systems that use the Linux kernel.

- **gem**: package manager for ruby packages.

- **pip**: package manager for python packages.

<br>

### **Advanced Package Manager (APT)**:
On Debian-based distributions, packages are downloaded via the APT package manager. When we refer to packages, we are essentially dealing with archive files that contain multiple deb files that are used by the dpkg utility to install programs.

So why would we need APT? APT, standing for Advanced Package Manage, is responsible for downloading, installing, updating and removing packages from your system. Packages are tied together through package dependencies. For example if you want do download metasploit-framework, it needs hundreds of libraries.

On Linux, software is distributed through software repositories. Software repositories are used in order to aggregate free software provided by the community. Let's look the source repository in our Linux system.

```bash
attacker@cyberpath:~$ cat /etc/apt/sources.list
# See http://help.ubuntu.com/community/UpgradeNotes for how to upgrade to
# newer versions of the distribution.
deb http://archive.ubuntu.com/ubuntu/ focal main restricted
# deb-src http://archive.ubuntu.com/ubuntu/ focal main restricted
...
```

APT uses a database called the APT cache. We can search the APT cache, for example, to find all **net-tools** related packages.

```bash
attacker@cyberpath:~$ apt-cache search net-tools
net-tools - NET-3 networking toolkit
atm-tools - Base programs for ATM in Linux, the net-tools for ATM
ddnet-tools - Tools for DDNet
```

We can then view additional information about a package. 

```bash
attacker@cyberpath:~$ apt-cache show net-tools
Package: net-tools
Version: 1.60+git20181103.0eebece-1
Installed-Size: 991
...
```

We can also list all installed packages.

```bash
attacker@cyberpath:~$ apt list --installed
Listing... Done
adduser/focal,now 3.118ubuntu2 all [installed]
aircrack-ng/kali-rolling,now 1:1.6+git20210130.91820bc-2 amd64 [installed]
...
```

Let's install a package with apt. We will install **sl** which is funny command line application. But first we need to update cache of our system.

```bash
attacker@cyberpath:~$ sudo apt update
Hit:1 http://archive.ubuntu.com/ubuntu focal InRelease
Get:2 http://archive.ubuntu.com/ubuntu focal-updates InRelease [114 kB]
Get:3 http://security.ubuntu.com/ubuntu focal-security InRelease [114 kB]
Get:4 http://archive.ubuntu.com/ubuntu focal-backports InRelease [101 kB]
Get:5 http://archive.ubuntu.com/ubuntu focal-updates/universe amd64 Packages [1090 kB]
Get:7 http://security.ubuntu.com/ubuntu focal-security/universe amd64 Packages [804 kB]
Get:8 http://archive.ubuntu.com/ubuntu focal-updates/main amd64 Packages [1637 kB]
Get:6 https://mirror.karneval.cz/pub/linux/kali kali-rolling InRelease [30.6 kB]
Get:9 https://mirror.karneval.cz/pub/linux/kali kali-rolling/contrib amd64 Packages [110 kB]
Get:10 https://mirror.karneval.cz/pub/linux/kali kali-rolling/non-free amd64 Packages [211 kB]
Get:11 https://mirror.karneval.cz/pub/linux/kali kali-rolling/main amd64 Packages [17.9 MB]
Fetched 22.1 MB in 9s (2475 kB/s)
Reading package lists... Done
Building dependency tree
Reading state information... Done
16 packages can be upgraded. Run 'apt list --upgradable' to see them.
```

Apt package manager tells to ous some of the packages are outdated and we need to upgrade it. Of course you can use tools without software upgrades but for reliable and secure system packages should upgraded.

```bash
attacker@cyberpath:~$ sudo apt list --upgradable
Listing... Done
libfribidi0/focal 1.0.8-2 amd64 [upgradable from: 1.0.8-2]
libgsm1/focal 1.0.18-2 amd64 [upgradable from: 1.0.18-2]
libmemcached11/focal 1.0.18-4.2ubuntu2 amd64 [upgradable from: 1.0.18-4.2]
libmp3lame0/focal 3.100-3 amd64 [upgradable from: 3.100-3]
libshine3/focal 3.1.1-2 amd64 [upgradable from: 3.1.1-2]
libsnappy1v5/focal 1.1.8-1build1 amd64 [upgradable from: 1.1.8-1]
libspeex1/focal 1.2~rc1.2-1.1ubuntu1 amd64 [upgradable from: 1.2~rc1.2-1.1]
libtheora0/focal 1.1.1+dfsg.1-15ubuntu2 amd64 [upgradable from: 1.1.1+dfsg.1-15]
libtwolame0/focal 0.4.0-2 amd64 [upgradable from: 0.4.0-2]
libxfixes3/focal 1:5.0.3-2 amd64 [upgradable from: 1:5.0.3-2]
libxkbfile1/focal 1:1.1.0-1 amd64 [upgradable from: 1:1.1.0-1]
libxpm4/focal 1:3.5.12-1 amd64 [upgradable from: 1:3.5.12-1]
libxrender1/focal 1:0.9.10-1 amd64 [upgradable from: 1:0.9.10-1]
libxshmfence1/focal 1.3-1 amd64 [upgradable from: 1.3-1]
libxslt1.1/focal 1.1.34-4 amd64 [upgradable from: 1.1.34-4]
libxvidcore4/focal 2:1.3.7-1 amd64 [upgradable from: 2:1.3.7-1]
```

Listed packages from apt is outdated. Lets upgrade these packages. 

```bash
attacker@cyberpath:~$ sudo apt upgrade
Reading package lists... Done
Building dependency tree
Reading state information... Done
Calculating upgrade... Done
The following packages will be upgraded:
  libfribidi0 libgsm1 libmemcached11 libmp3lame0 libshine3 libsnappy1v5 libspeex1 libtheora0 libtwolame0 libxfixes3 libxkbfile1 libxpm4 libxrender1 libxshmfence1
  libxslt1.1 libxvidcore4
16 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.
Need to get 1054 kB of archives.
After this operation, 332 kB disk space will be freed.
Do you want to continue? [Y/n]
Get:1 http://archive.ubuntu.com/ubuntu focal/main amd64 libfribidi0 amd64 1.0.8-2 [23.8 kB]
Get:2 http://archive.ubuntu.com/ubuntu focal/universe amd64 libgsm1 amd64 1.0.18-2 [24.4 kB]
Get:3 http://archive.ubuntu.com/ubuntu focal/main amd64 libmemcached11 amd64 1.0.18-4.2ubuntu2 [83.5 kB]
...
```

Time to install application:

```bash
attacker@cyberpath:~$ sudo apt install sl
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following NEW packages will be installed:
  sl
0 upgraded, 1 newly installed, 0 to remove and 16 not upgraded.
Need to get 12.7 kB of archives.
After this operation, 60.4 kB of additional disk space will be used.
Get:1 http://archive.ubuntu.com/ubuntu focal/universe amd64 sl amd64 5.02-1 [12.7 kB]
Fetched 12.7 kB in 0s (27.0 kB/s)
debconf: delaying package configuration, since apt-utils is not installed
Selecting previously unselected package sl.
(Reading database ... 76198 files and directories currently installed.)
Preparing to unpack .../archives/sl_5.02-1_amd64.deb ...
Unpacking sl (5.02-1) ...
Setting up sl (5.02-1) ...
```


With the sl command, a steam locomotive will run across your terminal from right to left. And it looks like this:

```bash
(@@) (  ) (@)  ( )  @@    ()    @     O     @
                     (   )
                 (@@@@)
              (    )
            (@@@)
         ====        ________                ___________
     _D _|  |_______/        \__I_I_____===__|_________|
      |(_)---  |   H\________/ |   |        =|___ ___|      ________________
      /     |  |   H  |  |     |   |         ||_| |_||     _|
     |      |  |   H  |__--------------------| [___] |   =|
     | ________|___H__/__|_____/[][]~\_______|       |   -|
     |/ |   |-----------I_____I [][] []  D   |=======|____|_________________
   __/ =| o |=-O=====O=====O=====O \ ____Y___________|__|___________________
    |/-=|___|=    ||    ||    ||    |_____/~\___/          |_D__D__D_|  |_D_
     \_/      \__/  \__/  \__/  \__/      \_/               \_/   \_/    \_/
```

Pretty cool right?

Now let's remove **sl** packege from our Linux system.

```bash
attacker@cyberpath:~$ sudo apt remove sl
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following packages will be REMOVED:
  sl
0 upgraded, 0 newly installed, 1 to remove and 0 not upgraded.
After this operation, 60.4 kB disk space will be freed.
Do you want to continue? [Y/n]
(Reading database ... 76207 files and directories currently installed.)
Removing sl (5.02-1) ...
```

Now you know most about the APT package manager and can handle packages on Debian systems. Like programming languages, package managers are similar to each other, although they perform different tasks in different syntaxes. After that, you should be able to work with other package managers easily.

<br>
<br>

## **11 - Introduction to Service and Process Management - interactive**

As an administrator or penetration tester it is essential that you correctly manage your services and processes which are running on your server, not only to maintain server integrity so that terminal software doesn’t crash, but also to properly manage security. 

<br>

### **What’s the difference between a service and process?**
In Linux Operating System a service is just another name for a daemon, which is a client/server application that runs in the background. A service is continuously listening for incoming requests and sends a response based on the request given like database. A process is simply an application or a script which can be running in the foreground or the background like cat.

Let's check running currently services on our system:

```bash
attacker@cyberpath:~$ service --status-all
 [ - ]  cron
 [ - ]  exim4
 [ ? ]  hwclock.sh
 [ - ]  postgresql
 [ - ]  procps
 [ - ]  sysstat
 ```

**service** is a command which allows you start, stop or restart services running in the background. 

```bash
attacker@cyberpath:~$ service -h
Usage: service < option > | --status-all | [ service_name [ command | --full-restart ] ]
```

To start postgresql service type:

```bash
attacker@cyberpath:~$ sudo service postgresql start
[sudo] password for attacker:
 * Starting PostgreSQL 13 database server
   ...done.
```

We can check this with **ps**:

```bash
attacker@cyberpath:~$ ps -aux | grep postgresql
postgres   971  0.0  0.2 244636 29532 ?        Ss   09:22   0:00 /usr/lib/postgresql/13/bin/postgres -D /var/lib/postgresql/13/main -c config_file=/etc/postgresql/13/main/postgresql.conf
attacker   997  0.0  0.0   3112   848 pts/0    S+   09:27   0:00 grep --color=auto postgresql
```

To stop postgresql service type:

```bash
attacker@cyberpath:~$ sudo service postgresql stop
 * Stopping PostgreSQL 13 database server
   ...done.
```

<br>

### **Kill a Process**:
A process can be in the following states:
- Running
- Waiting: waiting for an event or system resource
- Stopped
- Zombie: stopped but still has an entry in the process table

In Linux Operating system processes can be controlled using kill, pkill, pgrep, and killall tools. To interact with a process, we must send a signal to it. Type the following command for viewing all signals:

```bash
attacker@cyberpath:~$ kill -l
 1) SIGHUP       2) SIGINT       3) SIGQUIT      4) SIGILL       5) SIGTRAP
 6) SIGABRT      7) SIGBUS       8) SIGFPE       9) SIGKILL     10) SIGUSR1
11) SIGSEGV     12) SIGUSR2     13) SIGPIPE     14) SIGALRM     15) SIGTERM
16) SIGSTKFLT   17) SIGCHLD     18) SIGCONT     19) SIGSTOP     20) SIGTSTP
21) SIGTTIN     22) SIGTTOU     23) SIGURG      24) SIGXCPU     25) SIGXFSZ
26) SIGVTALRM   27) SIGPROF     28) SIGWINCH    29) SIGIO       30) SIGPWR
31) SIGSYS      34) SIGRTMIN    35) SIGRTMIN+1  36) SIGRTMIN+2  37) SIGRTMIN+3
38) SIGRTMIN+4  39) SIGRTMIN+5  40) SIGRTMIN+6  41) SIGRTMIN+7  42) SIGRTMIN+8
43) SIGRTMIN+9  44) SIGRTMIN+10 45) SIGRTMIN+11 46) SIGRTMIN+12 47) SIGRTMIN+13
48) SIGRTMIN+14 49) SIGRTMIN+15 50) SIGRTMAX-14 51) SIGRTMAX-13 52) SIGRTMAX-12
53) SIGRTMAX-11 54) SIGRTMAX-10 55) SIGRTMAX-9  56) SIGRTMAX-8  57) SIGRTMAX-7
58) SIGRTMAX-6  59) SIGRTMAX-5  60) SIGRTMAX-4  61) SIGRTMAX-3  62) SIGRTMAX-2
63) SIGRTMAX-1  64) SIGRTMAX
```

Let's kill a process. First we will create a cat process and then we will kill it by PID.

```bash
attacker@cyberpath:~$ cat &
[1] 1034
attacker@cyberpath:~$ ps
  PID TTY          TIME CMD
  775 pts/0    00:00:00 bash
 1034 pts/0    00:00:00 cat
 1036 pts/0    00:00:00 ps
attacker@cyberpath:~$ kill -9 1034
attacker@cyberpath:~$ ps
  PID TTY          TIME CMD
  775 pts/0    00:00:00 bash
 1037 pts/0    00:00:00 ps
[1]+  Killed                  cat
attacker@cyberpath:~$ ps
  PID TTY          TIME CMD
  775 pts/0    00:00:00 bash
 1038 pts/0    00:00:00 ps
```
<br>
<br>

## **12 - Introduction to Permission Management - interactive**

In Linux Operating System, permissions are assigned to users and groups. Each user can be a member of different groups, and membership in these groups gives the user specific permissions. Remember **neo** user. We can't read /etc/shadow file until we assigned sudo group to **neo**. Each file and directory belongs to a specific user and a specific group. For example **neo** user owns the his home directory. When we create new files or directories, they belong to group we belong to and us.Persmisson system on Linux Operating Systems are three direfferent types of permissions a file or directory can be assigned:
- **r** :  Read
- **w** : Write
- **x** : Execute

Let's look /etc/passwd file.

```bash
attacker@cyberpath:~$ ls -l /etc/passwd
- rw- r-- r--   1 root root 1156    Nov  8 07:55 /etc/passwd
- --- --- ---   |  |    |    |      |                |
|  |   |   |    |  |    |    |      |                |_ File Name
|  |   |   |    |  |    |    |      |___ Date
|  |   |   |    |  |    |    |__________ File Size
|  |   |   |    |  |    |_______________ Group
|  |   |   |    |  |____________________ User
|  |   |   |    |_______________________ Number of hard links
|  |   |   |_ Permission of others (read)
|  |   |_____ Permissions of the group (read, write)
|  |_________ Permissions of the owner (read, write, execute)
|____________ File type (- = File, d = Directory, l = Link, ... )
```

<br>

### **Setting Permissions**:

In Linux Operating System, we can set permissions using the chmod command, permission group references  are:
- **u** : owner
- **g** : Group
- **o**: others
- **a** : All users
and either a[+] or a[-] to add remove the permissions. 

Let's play with **cat** permissions.

```bash
attacker@cyberpath:~$ ls -l /bin/cat
-rwxr-xr-x 1 root root 43416 Sep  5  2019 /bin/cat
```
If we look at output we can realize others have read and execute permissions on cat. Let's remove these permissions.

```bash
attacker@cyberpath:~$ sudo chmod o-rx /bin/cat
attacker@cyberpath:~$ ls -l /bin/cat
-rwxr-x--- 1 root root 43416 Sep  5  2019 /bin/cat
```

Now other can execute and read files with **cat**.

<br>

### **Setting Owner**:
In Linux Operating System to change the owner and/or the group assignments of a file, we use **chown** command.

```bash
attacker@cyberpath:~$ chown --help
Usage: chown [OPTION]... [OWNER][:[GROUP]] FILE...
  or:  chown [OPTION]... --reference=RFILE FILE...
Change the owner and/or group of each FILE to OWNER and/or GROUP.
...
```

Let's play with cat again.

```bash
attacker@cyberpath:~$ ls -la /bin/cat
-rwxr-x--- 1 root root 43416 Sep  5  2019 /bin/cat
```

cat is owned by root. We can change to our user with following command:

```bash
attacker@cyberpath:~$ sudo chown attacker:attacker /bin/cat
attacker@cyberpath:~$ ls -la /bin/cat
-rwxr-x--- 1 attacker attacker 43416 Sep  5  2019 /bin/cat
...
```