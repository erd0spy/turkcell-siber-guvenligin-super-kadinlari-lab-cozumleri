# Make Wordlist with Crunch


**Flags**:
- Şirketin 4 ila 5 karakter arasında temel bir numeric şifre kuralına sahip olduğunu biliyoruz. Crunch ile kelime listesi oluşturmak için cevap için bayt olarak kelime listesi boyutunu gönderin.
   - **73573136**
- Şirketin yalnızca müşterilerin 6 karakterli sayısal şifre oluşturmasına izin verdiğini biliyoruz. Crunch ile wordlist oluşturun ve client'ın bilgisayarından kurtardığımız client password.txt dosyasını kırın.
   - **364266**
- Dwayne Medrano sistemde belirlediği şifreyi hatırlamıyor ve elimizdeki yetkilerle sadece şifrenin hashini görebiliyoruz. Şifrenin 7 haneli olduğunu, büyük harf ve sembol içermediğini, 4, 5 ve 6'nın '202' ve son hanenin sembol olduğunu bilir. Crunch ile wordlist oluşturun ve unutulmuş_password.txt dosyasını kırın.
   - **llg202!**


**0 - Crunch'a Giriş**
- Parolaları kırarken, saldırganın her zaman bir kelime listesine ihtiyacı vardır. Normalde, kelime listesi şifreyi kırmak için saniyede binlerce kelime kullanır. Saldırgan hedef hakkında doğum günleri, çocuk isimleri, evcil hayvan isimleri, favori takım vb. gibi bazı bilgiler toplarsa Saldırgan, şifre kırma için özel kelime listesi oluştururken bilgileri kullanabilir.
- Herhangi bir kuruluşa veya hedef saldırgana saldırırken, saldırmak için kelime listesi oluşturmak için özel bir kelime listesine ihtiyaç duyabilir. Bu yüzden Crunch kullanılır. Crunch, Kali Linux'un yerleşik bir aracıdır. Crunch, özel bir kelime listesi oluşturmaya yardımcı olur. Ve bu kelime listesi John, Aircrack-ng ve daha birçok şifre kırma aracında kullanılabilir.
- Crunch'ın temel sözdizimi: `$ crunch <min> max<max> <characterset> -t <pattern> -o <output filename>`
- Crunch'ın manuel sayfası ile Crunch kullanımı hakkında daha fazla bilgi edinebilirsiniz. Crunch kılavuz sayfasını şu şekilde açabilirsiniz: `$ man crunch`


**1- Crunch ile Basit Wordlist Oluşturmak**

- Solution: 
```bash
root@cyberpath:~# crunch 4 5 -o wordlist1.txt
Crunch will now generate the following amount of data: 73573136 bytes
70 MB
0 GB
0 TB
0 PB
Crunch will now generate the following number of lines: 12338352 
```


**2- Create Wordlist with Just Numbers and Crack the Password with john!**

```bash
root@cyberpath:~# crunch 6 6 1234567890 -o wordlist2.txt
Crunch will now generate the following amount of data: 7000000 bytes
6 MB
0 GB
0 TB
0 PB
Crunch will now generate the following number of lines: 1000000 

crunch: 100% completed generating output

root@cyberpath:~# cat client_password.txt | hash-identifier
   #########################################################################
   #     __  __                     __           ______    _____           #
   #    /\ \/\ \                   /\ \         /\__  _\  /\  _ `\         #
   #    \ \ \_\ \     __      ____ \ \ \___     \/_/\ \/  \ \ \/\ \        #
   #     \ \  _  \  /'__`\   / ,__\ \ \  _ `\      \ \ \   \ \ \ \ \       #
   #      \ \ \ \ \/\ \_\ \_/\__, `\ \ \ \ \ \      \_\ \__ \ \ \_\ \      #
   #       \ \_\ \_\ \___ \_\/\____/  \ \_\ \_\     /\_____\ \ \____/      #
   #        \/_/\/_/\/__/\/_/\/___/    \/_/\/_/     \/_____/  \/___/  v1.2 #
   #                                                             By Zion3R #
   #                                                    www.Blackploit.com #
   #                                                   Root@Blackploit.com #
   #########################################################################
--------------------------------------------------
 HASH: 
Possible Hashs:
[+] MD5
[+] Domain Cached Credentials - MD4(MD4(($pass)).(strtolower($username)))

root@cyberpath:~# john --format=raw-md5 --wordlist=wordlist2.txt client_password.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-MD5 [MD5 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
364266           (?)
1g 0:00:00:00 DONE (2021-09-20 14:25) 33.33g/s 8448Kp/s 8448Kc/s 8448KC/s 364167..364540
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```

**3- Unutulan Şifreyi Kıralım**

```bash
root@cyberpath:~# crunch 7 7 -t @@@202^ -o wordlist3.txt
Crunch will now generate the following amount of data: 140608 bytes
0 MB
0 GB
0 TB
0 PB
Crunch will now generate the following number of lines: 17576 

crunch: 100% completed generating output

root@cyberpath:~# cat forgotten_password.txt | hash-identifier 
   #########################################################################
   #     __  __                     __           ______    _____           #
   #    /\ \/\ \                   /\ \         /\__  _\  /\  _ `\         #
   #    \ \ \_\ \     __      ____ \ \ \___     \/_/\ \/  \ \ \/\ \        #
   #     \ \  _  \  /'__`\   / ,__\ \ \  _ `\      \ \ \   \ \ \ \ \       #
   #      \ \ \ \ \/\ \_\ \_/\__, `\ \ \ \ \ \      \_\ \__ \ \ \_\ \      #
   #       \ \_\ \_\ \___ \_\/\____/  \ \_\ \_\     /\_____\ \ \____/      #
   #        \/_/\/_/\/__/\/_/\/___/    \/_/\/_/     \/_____/  \/___/  v1.2 #
   #                                                             By Zion3R #
   #                                                    www.Blackploit.com #
   #                                                   Root@Blackploit.com #
   #########################################################################
--------------------------------------------------
 HASH: 
Possible Hashs:
[+] MD5
[+] Domain Cached Credentials - MD4(MD4(($pass)).(strtolower($username)))


root@cyberpath:~# john --format=raw-md5 --wordlist=wordlist3.txt forgotten_password.txt 
Using default input encoding: UTF-8
Loaded 1 password hash (Raw-MD5 [MD5 256/256 AVX2 8x3])
Warning: no OpenMP support for this hash type, consider --fork=2
Press 'q' or Ctrl-C to abort, almost any other key for status
llg202!          (?)
1g 0:00:00:00 DONE (2021-09-20 14:33) 100.0g/s 806400p/s 806400c/s 806400C/s ljk2020..lyd2020
Use the "--show --format=Raw-MD5" options to display all of the cracked passwords reliably
Session completed
```