# Scan Networks With Nmap

**Flags**:
- 10.0.0.40'ta bulunan serverları tarayın ve açık poty numarasını yanıt olarak gönderin:
  - **21**
- 10.0.0.7 ve 10.0.0.15'te bulunan serverları tarayın, toplam açık port sayısını yanıt olarak gönderin:
  - **2**
- 10.0.0.1/24 alt ağını tarayın ve çalışan server sayısını yanıt olarak gönderin:
  - **6**
- 10.0.0.1'de bulunan açık TCP portlarını bulun. Toplam açık port numarasını cevap olarak gönderin.
  - **5**
- 10.0.0.1'de bulunan açık UDP bağlantı noktalarını bulun. Toplam açık port sayısını cevap olarak gönderin.
  - **1**


## Açıklama:

Arkadaşınız Dwayne Medrano, iç ağ sızma testi için sizi işe aldı. Ağ Sızma Testinde yapmamız gereken ilk şey ağı taramak. Günümüzde insanlar RustScan gibi modern alternatifleri deniyorlar ama bu süreçte biz old-school Nmap ile başlayacağız.

**0 - Nmap'e Giriş:**

Nmap ("Ağ Eşleyici"), ağ keşfi ve güvenlik denetimi için ücretsiz ve açık kaynaklı (lisans) bir yardımcı programdır. Birçok sistem ve ağ yöneticisi, ağ envanteri, hizmet yükseltme programlarını yönetme ve ana bilgisayar veya hizmet çalışma süresini izleme gibi görevler için de yararlı bulur.
Nmap yürütülebilir, Nmap paketi gelişmiş bir GUI ve sonuç görüntüleyici (Zenmap), esnek bir veri aktarımı, yeniden yönlendirme ve hata ayıklama aracı (Ncat), tarama sonuçlarını karşılaştırmak için bir yardımcı program (Ndiff) ve bir paket oluşturma ve yanıt analiz aracı içerir ( Nping).


Syntax
`nmap <scan types> <options> <target>`

Syntax tekniklerini aşağıdaki gibi öğrenebilirsiniz:

```
attacker@cyberpath:~$ nmap --help

SCAN TECHNIQUES:
  -sS/sT/sA/sW/sM: TCP SYN/Connect()/ACK/Window/Maimon scans
  -sU: UDP Scan
  -sN/sF/sX: TCP Null, FIN, and Xmas scans
  --scanflags <flags>: Customize TCP scan flags
  -sI <zombie host[:probeport]>: Idle scan
  -sY/sZ: SCTP INIT/COOKIE-ECHO scans
  -sO: IP protocol scan
  -b <FTP relay host>: FTP bounce scan
```

Nmap ile port tarama yapmak için üç tarama tipini öğreneceğiz. Bunlar:
- TCP Connect Scans (-sT)
- SYN "Half-open" Scans (-sS)
- UDP Scans (-sU)


**1 - Nmap ile Hostları Taramak:**

- Tek bir hostu taramak: Öncelikle tek bir hostu tarayalım. 10.0.0.40'daki hostu tarayın. Syntax: `$ nmap <ip> `
`attacker@cyberpath:~$ nmap 10.0.0.40`
```
attacker@cyberpath:~$ nmap 10.0.0.40
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-14 07:19 UTC
Nmap scan report for scan-networks-with-nmap_ftp_server1_1.scan-networks-with-nmap_internal_network (10.0.0.40)
Host is up (0.00021s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
21/tcp open  ftp

Nmap done: 1 IP address (1 host up) scanned in 0.10 seconds
```

- Birden fazla hostu taramak: Nmap ile tek aramada birden fazla hostu tarayabiliriz. 10.0.0.7 ve 10.0.0.15'deki hostları tarayalım. Syntax: `$ nmap <ip> <ip>`
`attacker@cyberpath:~$ nmap 10.0.0.7 10.0.0.15`

```
attacker@cyberpath:~$ nmap 10.0.0.7 10.0.0.15
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-14 07:20 UTC
Nmap scan report for scan-networks-with-nmap_php_1.scan-networks-with-nmap_internal_network (10.0.0.7)
Host is up (0.00025s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap scan report for scan-networks-with-nmap_nginx_server_1.scan-networks-with-nmap_internal_network (10.0.0.15)
Host is up (0.00027s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http

Nmap done: 2 IP addresses (2 hosts up) scanned in 0.12 seconds
```


- Tüm subneti taramak: `$ nmap 10.0.0.1/24` komutu ile tüm subneti tarayabilirsiniz.

```
attacker@cyberpath:~$ nmap 10.0.0.1/24
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-14 07:24 UTC
Nmap scan report for 10.0.0.1
Host is up (0.00039s latency).
Not shown: 994 closed ports
PORT     STATE SERVICE
22/tcp   open  ssh
80/tcp   open  http
90/tcp   open  dnsix
111/tcp  open  rpcbind
3306/tcp open  mysql

Nmap scan report for cyberpath (10.0.0.3)
Host is up (0.00079s latency).
Not shown: 998 closed ports
PORT     STATE SERVICE
5901/tcp open  vnc-1
6901/tcp open  jetstream

Nmap scan report for scan-networks-with-nmap_php_1.scan-networks-with-nmap_internal_network (10.0.0.7)
Host is up (0.00077s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE
80/tcp open  http


Nmap scan report for scan-networks-with-nmap_mysql_1.scan-networks-with-nmap_internal_network (10.0.0.20)
Host is up (0.00053s latency).
Not shown: 999 closed ports
PORT     STATE SERVICE
3306/tcp open  mysql
```

**2 - Açık TCP Portlarını Bulmak**

10.0.0.1'deki açık TCP portlarını bulalım.

```
attacker@cyberpath:~$ nmap -sT 10.0.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-10 19:47 UTC
Nmap scan report for 10.0.0.1
Host is up (0.000098s latency).
Not shown: 995 closed ports
PORT    STATE SERVICE
21/tcp  open  ftp
22/tcp  open  ssh
80/tcp  open  http
90/tcp  open  dnsix
111/tcp open  rpcbind

Nmap done: 1 IP address (1 host up) scanned in 3.13 seconds
```

**3 - Açık UDP Portlarını Bulmak**

10.0.0.1'deki açık UDP portlarını bulalım.
```
attacker@cyberpath:~$ sudo nmap -sU 10.0.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-10 20:23 UTC
Stats: 0:11:38 elapsed; 0 hosts completed (1 up), 1 undergoing UDP Scan
UDP Scan Timing: About 66.39% done; ETC: 20:40 (0:05:50 remaining)
Stats: 0:17:33 elapsed; 0 hosts completed (1 up), 1 undergoing UDP Scan
UDP Scan Timing: About 99.97% done; ETC: 20:40 (0:00:00 remaining)
Nmap scan report for 10.0.0.1
Host is up (0.00014s latency).
Not shown: 999 closed ports
PORT    STATE SERVICE
111/udp open  rpcbind
MAC Address: 02:42:7C:3C:74:8D (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 1090.76 seconds
```