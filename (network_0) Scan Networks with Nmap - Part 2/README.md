# Scan Networks With Nmap


**Flags**:
- 10.0.0.1'e servis taraması yapın. Cevap olarak OpenSSH hizmet detaylarını gönderin.
  - **OpenSSH 7.7p1 Debian 3 (protocol 2.0)**
- 10.0.0.1'e aggresive scan taraması yapın. Bulduğunuz toplam ssh-hostkey sayısını cevap olarak gönderin.
  - **3**
- Nmap ile subnetteki tüm hostlara service ve vuln scan yapın. Cevap olarak zaafiyetli uygulama ismi ve versiyonunu gönderin.
  - **vsftpd 2.3.4**
- Nmap ile subnetteki tüm hostlara service ve vuln scan yapın. Cevap olarak zaafiyetli FTP sunucusunun CVE numarasını gönderin
  - **CVE-2011-2523**

## Açıklama:

Arkadaşınız Dwayne Medrano, iç ağ sızma testi için sizi işe aldı. Ağ Sızma Testinde yapmamız gereken ilk şey ağı taramak. Bir önceki Nmap labında basit tarama yapmayı, açık portları bulmayı, bunları sınıflandırmayı öğrendik. Bu labda Servis Enum yapmayı, serverlarda açık bulmayı ve nmap'in diğer advanced özelliklerini öğreneceğiz


**4 - Nmap ile Servis Enum:**

Nmap'i kullanarak karşı tarafdaki serverda bulunan servisleri enum edebiliriz. Bunun için -sV switchini kullanılır.

```
attacker@cyberpath:~$ nmap -sV 10.0.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-10 20:22 UTC
Nmap scan report for 10.0.0.1
Host is up (0.000081s latency).
Not shown: 995 closed ports
PORT    STATE SERVICE VERSION
21/tcp  open  ftp     vsftpd 2.0.8 or later
22/tcp  open  ssh     OpenSSH 7.7p1 Debian 3 (protocol 2.0)
80/tcp  open  ftp     vsftpd 2.0.8 or later
90/tcp  open  ftp     vsftpd 2.0.8 or later
111/tcp open  rpcbind 2-4 (RPC #100000)
Service Info: Host: Welcome; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 14.47 seconds
```


**5 - NSE ve AGGRESİVE SCAN:**
- **NSE nedir?:** Nmap Komut Dosyası Motoru (NSE), Nmap'in en güçlü ve esnek özelliklerinden biridir. Kullanıcıların çok çeşitli ağ oluşturma görevlerini otomatikleştirmek için (Lua programlama dilini kullanarak) basit komut dosyaları yazmasına (ve paylaşmasına) olanak tanır.
- Nmap'te -AA switch ile ana bilgisayar hakkında her şeyi öğrenmek için Agresif tarama yapabilirsiniz.
```
attacker@cyberpath:~$ nmap -A 10.0.0.1
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-10 20:18 UTC
Nmap scan report for 10.0.0.1
Host is up (0.000067s latency).
Not shown: 995 closed ports
PORT    STATE SERVICE VERSION
21/tcp  open  ftp     vsftpd 2.0.8 or later
22/tcp  open  ssh     OpenSSH 7.7p1 Debian 3 (protocol 2.0)
| ssh-hostkey: 
|   2048 e9:73:48:11:37:2e:84:eb:f0:b2:0b:7c:c5:9b:26:cb (RSA)
|   256 8e:5f:e1:ca:99:b9:a8:2d:7c:5a:39:87:9f:20:0b:7c (ECDSA)
|_  256 a8:79:25:cd:c8:29:d2:a7:dd:84:24:5c:cb:74:cb:c7 (ED25519)
80/tcp  open  ftp
| fingerprint-strings: 
|   GenericLines, NULL: 
|_    220 Welcome Alpine ftp server https://hub.docker.com/r/delfer/alpine-ftp-server/
90/tcp  open  ftp     vsftpd 2.0.8 or later
111/tcp open  rpcbind 2-4 (RPC #100000)
| rpcinfo: 
|   program version    port/proto  service
|   100000  2,3,4        111/tcp   rpcbind
|   100000  2,3,4        111/udp   rpcbind
|   100000  3,4          111/tcp6  rpcbind
|   100000  3,4          111/udp6  rpcbind
|   100024  1          38335/tcp6  status
|   100024  1          43453/udp   status
|   100024  1          43499/tcp   status
|_  100024  1          60588/udp6  status
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port80-TCP:V=7.80%I=7%D=10/10%Time=61634A93%P=x86_64-pc-linux-gnu%r(NUL
SF:L,52,"220\x20Welcome\x20Alpine\x20ftp\x20server\x20https://hub\.docker\
SF:.com/r/delfer/alpine-ftp-server/\r\n")%r(GenericLines,52,"220\x20Welcom
SF:e\x20Alpine\x20ftp\x20server\x20https://hub\.docker\.com/r/delfer/alpin
SF:e-ftp-server/\r\n");
Service Info: Host: Welcome; OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 18.70 seconds
```

- NSE'de en çok kullanılan betiklerden biri vuln scan'dir. Pentester'lar genellikle ilk olarak servis taraması ile vuln taraması yapar ve servisler ile savunmasız hostları öğrenir. Alt ağ taramasından ağda 5 server olduğunu öğrendik:
10.0.0.20
<br>
10.0.0.7
<br>
10.0.0.40
<br>
10.0.0.76
<br>
10.0.0.23
<br>
- Mousepad'i açın, aşağıdaki tüm ip adreslerini yazın ve host.lst olarak kaydedin. Ardından, "sudo nmap -sV --script vuln -iL host.lst" ile tüm ana serverlara güvenlik açığı taraması yapın.
<br>

```
attacker@cyberpath:~$ cat host.lst 
10.0.0.20
10.0.0.7
10.0.0.40
10.0.0.76
10.0.0.23

attacker@cyberpath:~$ sudo nmap -sV --script vuln -iL host.lst 
Starting Nmap 7.80 ( https://nmap.org ) at 2021-10-14 07:28 UTC
Stats: 0:00:40 elapsed; 0 hosts completed (6 up), 6 undergoing Script Scan
NSE Timing: About 99.41% done; ETC: 07:28 (0:00:00 remaining)

Nmap scan report for scan-networks-with-nmap_ftp_server1_1.scan-networks-with-nmap_internal_network (10.0.0.40)
Host is up (0.000017s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
21/tcp open  ftp     vsftpd 2.3.4
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| ftp-vsftpd-backdoor: 
|   VULNERABLE:
|   vsFTPd version 2.3.4 backdoor
|     State: VULNERABLE (Exploitable)
|     IDs:  BID:48539  CVE:CVE-2011-2523
|       vsFTPd version 2.3.4 backdoor, this was reported on 2011-07-04.
|     Disclosure date: 2011-07-03
|     Exploit results:
|       Shell command: id
|       Results: uid=0(root) gid=0(root) groups=0(root),1(bin),2(daemon),3(sys),4(adm),6(disk),10(wheel),11(floppy),20(dialout),26(tape),27(video)
|     References:
|       https://www.securityfocus.com/bid/48539
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2011-2523
|       http://scarybeastsecurity.blogspot.com/2011/07/alert-vsftpd-download-backdoored.html
|_      https://github.com/rapid7/metasploit-framework/blob/master/modules/exploits/unix/ftp/vsftpd_234_backdoor.rb
|_sslv2-drown: 
MAC Address: 02:42:0A:00:00:28 (Unknown)
Service Info: OS: Unix

Nmap scan report for scan-networks-with-nmap_ssh_server_1.scan-networks-with-nmap_internal_network (10.0.0.76)
Host is up (0.000026s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 8.4p1 Debian 5 (protocol 2.0)
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| vulners: 
|   cpe:/a:openbsd:openssh:8.4p1: 
|     	MSF:ILITIES/GENTOO-LINUX-CVE-2021-28041/	4.6	https://vulners.com/metasploit/MSF:ILITIES/GENTOO-LINUX-CVE-2021-28041/	*EXPLOIT*
|     	CVE-2021-28041	4.6	https://vulners.com/cve/CVE-2021-28041
|     	CVE-2021-41617	4.4	https://vulners.com/cve/CVE-2021-41617
|     	MSF:ILITIES/OPENBSD-OPENSSH-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/OPENBSD-OPENSSH-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP9-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP9-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/F5-BIG-IP-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2020-14145/	*EXPLOIT*
|     	CVE-2020-14145	4.3	https://vulners.com/cve/CVE-2020-14145
|_    	CVE-2016-20012	4.3	https://vulners.com/cve/CVE-2016-20012
MAC Address: 02:42:0A:00:00:4C (Unknown)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Nmap scan report for scan-networks-with-nmap_ssh_server1_1.scan-networks-with-nmap_internal_network (10.0.0.23)
Host is up (0.000017s latency).
Not shown: 999 closed ports
PORT   STATE SERVICE VERSION
22/tcp open  ssh     OpenSSH 7.7p1 Debian 3 (protocol 2.0)
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| vulners: 
|   cpe:/a:openbsd:openssh:7.7p1: 
|     	MSF:ILITIES/UBUNTU-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/UBUNTU-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/SUSE-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/SUSE-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/SUSE-CVE-2019-25017/	5.8	https://vulners.com/metasploit/MSF:ILITIES/SUSE-CVE-2019-25017/	*EXPLOIT*
|     	MSF:ILITIES/REDHAT_LINUX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/REDHAT_LINUX-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/REDHAT-OPENSHIFT-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/REDHAT-OPENSHIFT-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/ORACLE-SOLARIS-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/ORACLE-SOLARIS-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/OPENBSD-OPENSSH-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/OPENBSD-OPENSSH-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/IBM-AIX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/IBM-AIX-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP3-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP3-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP2-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP2-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/GENTOO-LINUX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/GENTOO-LINUX-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/F5-BIG-IP-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/DEBIAN-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/DEBIAN-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/CENTOS_LINUX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/CENTOS_LINUX-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/AMAZON_LINUX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/AMAZON_LINUX-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/AMAZON-LINUX-AMI-2-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/AMAZON-LINUX-AMI-2-CVE-2019-6111/	*EXPLOIT*
|     	MSF:ILITIES/ALPINE-LINUX-CVE-2019-6111/	5.8	https://vulners.com/metasploit/MSF:ILITIES/ALPINE-LINUX-CVE-2019-6111/	*EXPLOIT*
|     	EXPLOITPACK:98FE96309F9524B8C84C508837551A19	5.8	https://vulners.com/exploitpack/EXPLOITPACK:98FE96309F9524B8C84C508837551A19	*EXPLOIT*
|     	EXPLOITPACK:5330EA02EBDE345BFC9D6DDDD97F9E97	5.8	https://vulners.com/exploitpack/EXPLOITPACK:5330EA02EBDE345BFC9D6DDDD97F9E97	*EXPLOIT*
|     	EDB-ID:46516	5.8	https://vulners.com/exploitdb/EDB-ID:46516	*EXPLOIT*
|     	CVE-2019-6111	5.8	https://vulners.com/cve/CVE-2019-6111
|     	1337DAY-ID-32328	5.8	https://vulners.com/zdt/1337DAY-ID-32328	*EXPLOIT*
|     	1337DAY-ID-32009	5.8	https://vulners.com/zdt/1337DAY-ID-32009	*EXPLOIT*
|     	SSH_ENUM	5.0	https://vulners.com/canvas/SSH_ENUM	*EXPLOIT*
|     	PACKETSTORM:150621	5.0	https://vulners.com/packetstorm/PACKETSTORM:150621	*EXPLOIT*
|     	MSF:AUXILIARY/SCANNER/SSH/SSH_ENUMUSERS	5.0	https://vulners.com/metasploit/MSF:AUXILIARY/SCANNER/SSH/SSH_ENUMUSERS	*EXPLOIT*
|     	EXPLOITPACK:F957D7E8A0CC1E23C3C649B764E13FB0	5.0	https://vulners.com/exploitpack/EXPLOITPACK:F957D7E8A0CC1E23C3C649B764E13FB0	*EXPLOIT*
|     	EXPLOITPACK:EBDBC5685E3276D648B4D14B75563283	5.0	https://vulners.com/exploitpack/EXPLOITPACK:EBDBC5685E3276D648B4D14B75563283	*EXPLOIT*
|     	EDB-ID:45939	5.0	https://vulners.com/exploitdb/EDB-ID:45939	*EXPLOIT*
|     	CVE-2018-15919	5.0	https://vulners.com/cve/CVE-2018-15919
|     	CVE-2018-15473	5.0	https://vulners.com/cve/CVE-2018-15473
|     	1337DAY-ID-31730	5.0	https://vulners.com/zdt/1337DAY-ID-31730	*EXPLOIT*
|     	EDB-ID:45233	4.6	https://vulners.com/exploitdb/EDB-ID:45233	*EXPLOIT*
|     	CVE-2021-41617	4.4	https://vulners.com/cve/CVE-2021-41617
|     	CVE-2019-16905	4.4	https://vulners.com/cve/CVE-2019-16905
|     	MSF:ILITIES/OPENBSD-OPENSSH-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/OPENBSD-OPENSSH-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP9-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP9-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP8-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/HUAWEI-EULEROS-2_0_SP5-CVE-2020-14145/	*EXPLOIT*
|     	MSF:ILITIES/F5-BIG-IP-CVE-2020-14145/	4.3	https://vulners.com/metasploit/MSF:ILITIES/F5-BIG-IP-CVE-2020-14145/	*EXPLOIT*
|     	CVE-2020-14145	4.3	https://vulners.com/cve/CVE-2020-14145
|     	CVE-2019-6110	4.0	https://vulners.com/cve/CVE-2019-6110
|     	CVE-2019-6109	4.0	https://vulners.com/cve/CVE-2019-6109
|     	CVE-2018-20685	2.6	https://vulners.com/cve/CVE-2018-20685
|     	PACKETSTORM:151227	0.0	https://vulners.com/packetstorm/PACKETSTORM:151227	*EXPLOIT*
|     	EDB-ID:46193	0.0	https://vulners.com/exploitdb/EDB-ID:46193	*EXPLOIT*
|_    	1337DAY-ID-30937	0.0	https://vulners.com/zdt/1337DAY-ID-30937	*EXPLOIT*
MAC Address: 02:42:0A:00:00:17 (Unknown)
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 6 IP addresses (6 hosts up) scanned in 81.37 seconds

```

<br>
<br>


**6 - Firewall'ı Atlatmak:**

- Bu ağda bizi engelleyen bir güvenlik duvarı yoktu. Nmap güvenlik duvarı ile karşılaştığında basit bypass için, -Pn anahtarı ile SYN-Scan ve ACK-Scan kullanılabilir.


**7 - Çıktılar ve Sonuçları Kaydetme:**

- Detaylı Çıktı:
`attacker@cyberpath:~$ nmap -v scanme.nmap.org`

- Çıktıyı Kaydetmek:
`attacker@cyberpath:~$ nmap -oN output.txt scanme.nmap.org`

**Yapılacaklar:**
- 80 portunda giriş sayfası yayın yapan bir web sunucusu var. Hydra laboratuvarında brute force'u öğrenip panele erişmeye çalışacağız.
- OpenSSH 7.7 ve VSFTPD 2.3.4 çalıştıran savunmasız sunucular için Metasploit laboratuvarında Metasploit saldırı yöntemlerini öğreneceğiz ve sunucuları ele geçirmeye çalışacağız.